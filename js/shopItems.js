var pancho;
var reviewCount;
var viewType = getUrlVars()["fromPage"];
function altFnc() {
    alert("Cart is empty. PLease add Items to check out.");
}
$(document).ready(function() {
    $('#Recommended').hide();
    localStorage.removeItem('currentyUsingItem');
    sessionStorage.removeItem('storeGroupinfo');
    $('.modal').modal();
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 1, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: true, // Close upon selecting a date,
        format: 'yyyy-mm-dd'
    });
    $('.timepicker').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0, // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: false, // Use AM/PM or 24-hour format
        donetext: 'OK', // text for done-button
        cleartext: 'Clear', // text for clear-button
        canceltext: 'Cancel', // Text for cancel-button
        autoclose: true, // automatic close timepicker
        ampmclickable: true, // make AM PM clickable
        aftershow: function() {} //Function for after opening timepicker
    });
    var shopId = getUrlVars()["shopId"];
    var myObj = {};
    myObj['shopId'] = shopId;
    var data = JSON.stringify(myObj);
    var reviewList = [];
    $.ajax({
            url: beUrl() + '/getreviewlist',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            data: data,
            success: function(response) {
                var r =  JSON.stringify(response);
                var res = JSON.parse(r);
                reviewCount = res.count;
                $.each(res.Data, function(i,v){
                    reviewList.push(v);

                });
                    var myNewobj = {};
                    myNewobj['reviewIdList'] = reviewList;
                var sendData = JSON.stringify(myNewobj);
                $.ajax({
                    url: beUrl() + '/reviewdetails',
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: 'application/json',
                    data: sendData,
                    success: function(response) {
                        var r =  JSON.stringify(response);
                        var res = JSON.parse(r);
                        var printx = '';
                        $.each(res.Data, function(k,l){
                            printx = printx + "<p style='padding:1em;'><span style= ' float:right; font-size:1em; background-color:#27ce1e; color:white; width:4em; '> "+l.rating+" <i class=material-icons style='text-align:center; vertical-align:middle; font-size:1em;'>stars</i></span> <span style='font-weight:800; font-size:0.9em; color:#786;'> "+l.custName+"</span></p><p class='as_cons' style='padding:0.8em; font-size:1em; font-weight:650; font-color:#1e1e1e;'>"+l.reviewText+"</p><hr style=''>";
                        });
                    $("#allReviews").append(printx);
                    },
                    error: function(response) {
                        console.log("reviewdetails is False");
                    }
                });

            },
            error: function(response) {
                console.log(response.Data);
            }
    });
    var printx = '<div class="col s6" style="padding-right:0px; padding-left:0px; cursor:pointer;"><a class="mod_btn" onclick="redirect()" style="color:white; cursor:pointer; background:black; padding:10px; float:left;">Shops<i class="material-icons left">store</i></a></div><div class="col s6" style="padding-right:0px; padding-left:0px; cursor:pointer;"><a class="mod_btn" onclick="cartcheck()" style="color:white; padding:10px; background:black; float:right;  border-right:1px solid white;">Check Out<i class="material-icons right">shopping_cart</i></a></div>';
    $('#item_card_btn').append(printx);
    var shopId = getUrlVars()["shopId"];
    var xUser = [];
    xUser.push(shopId);
    var myObj1 = {};
    myObj1['shops'] = xUser;
    var data = JSON.stringify(myObj1);
    $.ajax({
        url: beUrl() + '/getshopinfo',
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: 'application/json',
        data: data,
        success: function(response) {
            var r = JSON.stringify(response);
            var res = JSON.parse(r);
            var data = res.Data;
            pancho = data[0];
            var printPar = "";
            var shname = "";
            var ImgUrl = imgUrl() + "" + shopId + ".jpeg";
            if (pancho.imageUploaded == 1) {
                shname = "" + pancho.shopName + "";
               }
            else
            {
                shname = "" + pancho.shopName + "";
                var ImgUrl = feUrl() + "/images/noImage.gif";
            }
            var isMobile = {
              Android: function() {
                return navigator.userAgent.match(/Android/i);
              },
              BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
              },
              iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
              },
              Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
              },
              Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
              },
              any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
              }
            };
            var makeCall = "";
            if(pancho.shopPhoneNumber>0){
              if(isMobile.any()){
                makeCall = "<span  class='favImgidc' style='z-index:+50;margin-top:-1.2em;  margin-right:1em;  height:auto; width:1.25em; padding:1px; cursor:pointer; vertical-align:center; float:right; display:block;'><a href='tel:"+pancho.shopPhoneNumber+"' onclick='Materialize.toast('I am a toast', 4000)'><i class='material-icons'>call</i></a></span>";
              }
              else{
                makeCall = "<span  class='favImgidc' style='z-index:+50;margin-top:-1.2em;  margin-right:1em;  height:auto; width:1.25em; padding:1px; cursor:pointer; vertical-align:center; float:right; display:block;'><a href='javascript:displayPhoneDesktop("+pancho.shopPhoneNumber+")'><i class='material-icons'>call</i></a></span>";
              }
            }
             printPar = "<p class='as_con'><span style='color:#FFA751; font-weight:bold'> Shop Minimum Order:</span> &#8377;" + pancho.minOrderValue + makeCall+"</p><p class='as_con' style='padding-bottom:0px; margin-bottom:0px; margin-top:-1em'><span style='color:#FFA751; font-weight:bold'> Delivery  Charges:</span> &#8377; " + pancho.delCharges + " </p><p><span style='font-weight:bold; color:black; font-size:1.25em;'> About Shop: </span> <span class = 'as_con1' > " + pancho.shopDesc + " </span> </p><p class='as_con ratRev'><span style='color:green; font-weight:bold'> Rating and Reviews:</span><span id='availRatRev'><span id='availRev' style= 'font-size:1em; color:black; width:4em; margin-left:1em; margin-right:1em;'> <i class=material-icons style='text-align:center; vertical-align:middle; font-size:1.2em;'>stars</i> " + pancho.rating + "  </span> | <a class='modal-trigger' href='#reviewModal'> See " +reviewCount+ " Reviews </a></span> </p> ";
            $('#shopimg').attr("src", ImgUrl);
            $('#shopname').append(shname);
            $('#shopTitleName').append(shname);
            $('#shopdescx').append(printPar);
            if( pancho.rating <= 0.01)
                {
                    $(".ratRev").hide();
                }
            $('#medLocation').append(pancho.address);
            if (pancho.isOpen == 1) {
                var myObj = {};
                myObj['shopId'] = shopId;
                var data = JSON.stringify(myObj);
                if(localStorage.getItem("delVal")){
                  $.ajax({
                      url: beUrl() + '/itemlist',
                      type: 'POST',
                      dataType: 'json',
                      processData: false,
                      contentType: 'application/json',
                      data: data,
                      success: function(response) {
                          var storeItem = JSON.stringify(response);
                          var storeItemData = JSON.parse(storeItem);
                          var totalCount = storeItemData.totalCount;
                          if (totalCount > 0) {
                              var itemData = storeItemData.data;
                              displayItems(itemData, shopId);
                          } else {
                              var $toastContent2 = $('<span id=noItems>Shop is empty</span>').add($('<button class="btn-flat toast-action" onclick="redirect()">View others</button>'));
                              Materialize.toast($toastContent2);
                          }
                      }
                  });
                  $.ajax({
                      url: beUrl() + '/shopaddoninfo',
                      type: 'POST',
                      dataType: 'json',
                      processData: false,
                      contentType: 'application/json',
                      data: data,
                      success: function(response) {
                          var data = (JSON.stringify(response));
                          localStorage.setItem("addOnInfo", data);
                      }
                  });
                  $.ajax({
                      url: beUrl() + '/viewshoptime',
                      type: 'POST',
                      dataType: 'json',
                      processData: false,
                      contentType: 'application/json',
                      data: data,
                      success: function(response) {
                          var data = JSON.parse((JSON.stringify(response)));
                          if (data.totalCount > 0) {
                              var print = "<div class='input-field'><select id='selectingSubscriptionTime'></select><label>Select Timing</label></div>";
                              $('#selectGroupIf').append(print);
                              $.each(data.Data, function(i, v) {
                                  if (v.isActive == 1) {
                                      var xPrint = "<option value='" + v.time + "' id='testIfPoss'>" + v.name + "</option>";
                                      $('#selectingSubscriptionTime').append(xPrint);
                                  }
                              })
                              $('select').material_select();
                          } else {
                              var print = "<input type='text' class='timepicker' id='selectingSubscriptionTime' placeholder='Select Time'><label for='selectingSubscriptionTime' data-error='Kindly pass the time'></label>";
                              $('#selectGroupIf').append(print);
                              $('.timepicker').pickatime({
                                  default: 'now', // Set default time: 'now', '1:30AM', '16:30'
                                  fromnow: 0, // set default time to * milliseconds from now (using with default = 'now')
                                  twelvehour: false, // Use AM/PM or 24-hour format
                                  donetext: 'OK', // text for done-button
                                  cleartext: 'Clear', // text for clear-button
                                  canceltext: 'Cancel', // Text for cancel-button
                                  autoclose: true, // automatic close timepicker
                                  ampmclickable: true, // make AM PM clickable
                                  aftershow: function() {} //Function for after opening timepicker
                              });
                          }
                      }
                  });
                  $.ajax({
                      url: beUrl() + '/shopsubsinfo',
                      type: 'POST',
                      dataType: 'json',
                      processData: false,
                      contentType: 'application/json',
                      data: data,
                      success: function(response) {
                          var data = (JSON.stringify(response));
                          localStorage.setItem("subsInfo", data);
                      }
                  });
                }
                else{
                  var $toastContent2 = $('<span id=noItems>Shop is not deliverable</span>').add($('<button class="btn-flat toast-action" onclick="redirect()">View others</button>'));
                  Materialize.toast($toastContent2)
                }
            } else {
                var $toastContent2 = $('<span id=noItems>Shop is closed</span>').add($('<button class="btn-flat toast-action" onclick="redirect()">View others</button>'));
                Materialize.toast($toastContent2);
            }
        }
    });
    if (localStorage.getItem("cartCount")) {
        var printx = '<div class="col s6"><a class="mod_btn" onclick="redirect()" style="color:white; padding:10px; background:black; float:left; cursor:pointer;">Shops<i class="material-icons left">store</i></a></div><div class="col s6>"<a class="mod_btn" onclick="cartcheck()" style="color:white; padding:10px; background:black; float:right;  border-right:1px solid white;">Check Out<i class="material-icons right">shopping_cart</i></a></div>';
        var shops = '<span onclick="redirect()" style="text-align:center; padding:5px;  display:block;"><i class="material-icons md-36" style="vertical-align:middle;">arrow_back</i></span>';
        var cartC = localStorage.getItem('cartCount');
        var search = '<span onclick=javascript:cartcheck() style="text-align:center; padding:5px;  display:block; color:white; font-size:1.5em; font-family:Arima Madurai; border-left:1px solid white;"><i class="material-icons md-36 right">shopping_cart</i>Cart</span>';
        $('#item_card_btn').html(printx);
        $('#cart_nav').html(search);
    }
    var database, idb_request;
    idb_request = window.indexedDB.open("cartInfo", 1);
    idb_request.addEventListener("error", function(event) {
        alert("Could not open Indexed DB due to error: " + this.errorCode);
    });
    idb_request.addEventListener("upgradeneeded", function(event) {
        var storage = this.result.createObjectStore("data1", {
            keyPath: "subItemId"
        });
        storage.createIndex("by_subItemId", "subItemId", {
            unique: true
        });
    });
    var database1, idb_reques1t;
    idb_request1 = window.indexedDB.open("cartShopInfo", 1);
    idb_request1.addEventListener("error", function(event) {
        alert("Could not open Indexed DB due to error: " + this.errorCode);
    });
    idb_request1.addEventListener("upgradeneeded", function(event) {
        var storage = this.result.createObjectStore("data1", {
            keyPath: "shopId"
        });
        storage.createIndex("by_shopId", "shopId", {
            unique: true
        });
    });
    var database2, idb_request2;
    idb_request2 = window.indexedDB.open("itemDesc", 1);
    idb_request2.addEventListener("error", function(event) {
        alert("Could not open Indexed DB due to error: " + this.errorCode);
    });
    idb_request2.addEventListener("upgradeneeded", function(event) {
        var storage = this.result.createObjectStore("data1", {
            keyPath: "subItemId"
        });
        storage.createIndex("by_subItemId", "subItemId", {
            unique: true
        });
    });

    $(".addSubscription").click(function() {
        var subItemId = Number(localStorage.getItem("currentyUsingItem"));
        var subsId = $("#selectingSubscriptionId").val();
        if (subsId) {
            var date = document.getElementById("selectingSubscriptionDate").value;
            if (date) {
                var time = document.getElementById("selectingSubscriptionTime").value;
                if (time) {
                    var myObj = {}
                    myObj['subId'] = subsId;
                    if (document.getElementById('testIfPoss')) {
                        myObj['startDate'] = (date + " " + time);
                    } else {
                        myObj['startDate'] = (date + " " + time + ":00");
                    }

                    var database2, idb_request2;
                    idb_request2 = window.indexedDB.open("itemDesc", 1);
                    idb_request2.addEventListener("success", function(event) {
                        database2 = this.result;
                        var storage2 = database2.transaction("data1", "readwrite").objectStore("data1");
                        var index2 = storage2.index("by_subItemId");
                        var request2 = index2.openCursor(IDBKeyRange.only(subItemId));
                        request2.onsuccess = function() {
                            var cursor2 = request2.result;
                            if (cursor2) {
                                storage2.put({
                                    subItemId: subItemId,
                                    quantId: cursor2.value.quantId,
                                    addOnInfo: cursor2.value.addOnInfo,
                                    subsInfo: myObj
                                });
                            }
                        }
                    });
                } else {
                    alert("Please select a time");
                }
            } else {
                alert("Please select a date");
            }
        }

        idb_request = window.indexedDB.open("cartInfo", 1);
        idb_request.addEventListener("success", function(event) {
            database = this.result;
            var storage = database.transaction("data1", "readwrite").objectStore("data1");
            var index = storage.index("by_subItemId");
            var request = index.openCursor(IDBKeyRange.only(subItemId));
            request.onsuccess = function() {
                var cursor = request.result;
                if (cursor) {
                    var shopId = cursor.value.shopId;
                    if (localStorage.getItem("cartCount")) {
                        var cartCount = localStorage.getItem("cartCount");
                        cartCount = Number(cartCount) + 1;
                        localStorage.setItem("cartCount", cartCount);
                        var shopList = localStorage.getItem("shopList")
                        var temp = new Array();
                        temp = shopList.split(",");
                        var x = -1;
                        for (i = 0; i < temp.length; i++) {
                            if (temp[i] == shopId) {
                                x = i;
                            }
                        }
                        if (x == -1) {
                            shopList = shopList + "," + shopId;
                        }
                        localStorage.setItem("shopList", shopList);
                        updateCartValue();
                    } else {
                        localStorage.setItem("cartCount", 1);
                        localStorage.setItem("shopList", shopId);
                        updateCartValue();
                    }
                    storage.put({
                        subItemId: subItemId,
                        mItemId: cursor.value.mainItemId,
                        shopId: shopId,
                        quantity: 1,
                        addOnInfo: cursor.value.addOnInfo,
                        subsInfo: cursor.value.subsInfo,
                        quantInfo: cursor.value.quantInfo
                    });
                }
            }
        })

        if (document.getElementById("add_btnImg" + subItemId)) {
            document.getElementById("add_btnImg" + subItemId).innerHTML = "";
            document.getElementById("add_btnImg" + subItemId).style.backgroundColor = "#ffffff";
        }
        document.getElementById("add_btn" + subItemId).innerHTML = "";
        document.getElementById("add_btn" + subItemId).style.backgroundColor = "#ffffff";
        var lPrint = "<span id='addbtn'><span onclick='deuctItem(" + subItemId + ")' style='cursor:pointer;'><a  class='waves-effect waves-teal' ><i class='material-icons left' id='remove'  >remove</i></a></span><span id='quanValueImg" + subItemId + "'>1</span><a href='javascript:addItemCount(" + subItemId + ")'  class='waves-effect waves-teal'><i class='material-icons right'>add</i></a></span>";
        $('#add_btnImg' + subItemId).append(lPrint);
        lPrint = "<span id='addbtn' onclick=deuctItem(" + subItemId + ")'><span style='cursor:pointer;' class='waves-effect waves-teal'><a><i class='material-icons left' id='remove'>remove</i></a></span><span id='quanValue" + subItemId + "' class='waves-effect waves-teal' onclick='addItemCount(" + subItemId + ")'>1</span><a  class='' ><i class='material-icons right'>add</i></a></span>";
        $('#add_btn' + subItemId).append(lPrint);
        var database1, idb_request1;
        idb_request1 = window.indexedDB.open("cartShopInfo", 1);
        idb_request1.addEventListener("success", function(event) {
            database1 = this.result;
            var storage1 = database1.transaction("data1", "readwrite").objectStore("data1");
            var index1 = storage1.index("by_shopId");
            var request1 = index1.openCursor(IDBKeyRange.only(shopId));
            request1.onsuccess = function() {
                var cursor1 = request1.result;
                if (cursor1) {
                    var xItemCount = cursor1.value.itemCount;
                    var itemList = cursor1.value.itemList;
                    if (xItemCount > 0) {
                        xItemCount = xItemCount + 1;
                        itemList = itemList + "," + subItemId;
                    } else {
                        xItemCount = xItemCount + 1;
                        itemList = subItemId;
                    }
                    storage1.put({
                        shopId: shopId,
                        itemCount: xItemCount,
                        itemList: itemList
                    });
                } else {
                    storage1.add({
                        shopId: shopId,
                        itemCount: 1,
                        itemList: subItemId
                    });
                }
            }
        });
        $('#subscriptionChoose').modal('close');
    });

    $(".addAddOnInfo").click(function() {
        var usingObj = {};
        usingObj['addOnInfo'] = [];

        event.preventDefault();
        var subItemId = Number(localStorage.getItem("currentyUsingItem"));
        var database, idb_request;
        idb_request = window.indexedDB.open("cartInfo", 1);
        idb_request.addEventListener("success", function(event) {
            database = this.result;
            var storage = database.transaction("data1", "readwrite").objectStore("data1");
            var index = storage.index("by_subItemId");
            var request = index.openCursor(IDBKeyRange.only(subItemId));
            request.onsuccess = function() {
                var cursor = request.result;
                if (cursor) {
                    var addOnInfo = cursor.value.addOnInfo;
                    var temp = new Array();
                    temp = addOnInfo.split(",");
                    var x = -1;
                    for (i = 0; i < temp.length; i++) {
                        var addNameUse = "addOn" + temp[i];
                        var isSelected = $("#" + addNameUse).val();;
                        var temp1 = new Array();
                        temp1 = isSelected;
                        if (isSelected != null) {
                            if (temp1[0]) {
                                var j = 0;
                                var usingObjLatest = {};
                                usingObjLatest['itemId'] = [];
                                var jAddOnInfo = JSON.parse(localStorage.getItem('addOnInfo')).data;
                                var multiSelect = 0
                                $.each(jAddOnInfo, function(l, h) {
                                    if (h.groupId == Number(temp[i])) {
                                        multiSelect = h.isMultiSelect;
                                    }
                                })
                                if (multiSelect != 0) {
                                    while (temp1[j]) {
                                        var userObj = {};
                                        userObj['subAddId'] = Number(temp1[j]);
                                        usingObjLatest['itemId'].push(userObj);
                                        j++;
                                        x = 0;
                                    }
                                } else {
                                    var userObj = {};
                                    userObj['subAddId'] = Number(isSelected);
                                    usingObjLatest['itemId'].push(userObj);
                                    j++;
                                    x = 0;
                                }
                                usingObjLatest['addOnId'] = Number(temp[i]);
                                usingObjLatest['quantity'] = j
                                usingObj['addOnInfo'].push(usingObjLatest);

                            } else {
                                console.log(temp);
                            }
                        }

                    }
                    if (x == -1) {
                        usingObj = "";
                    }
                    var database2, idb_request2;
                    idb_request2 = window.indexedDB.open("itemDesc", 1);
                    idb_request2.addEventListener("success", function(event) {
                        database2 = this.result;
                        var storage2 = database2.transaction("data1", "readwrite").objectStore("data1");
                        var index2 = storage2.index("by_subItemId");
                        var request2 = index2.openCursor(IDBKeyRange.only(subItemId));
                        request2.onsuccess = function() {
                            var cursor2 = request2.result;
                            if (cursor2) {
                                storage2.put({
                                    subItemId: subItemId,
                                    quantId: cursor2.value.quantId,
                                    addOnInfo: usingObj,
                                    subsInfo: ""
                                });
                            }
                        }
                    });
                    var subsInfo = cursor.value.subsInfo;
                    if (subsInfo == "") {
                        if (localStorage.getItem("cartCount")) {
                            var cartCount = localStorage.getItem("cartCount");
                            cartCount = Number(cartCount) + 1;
                            localStorage.setItem("cartCount", cartCount);
                            var shopList = localStorage.getItem("shopList")
                            var temp = new Array();
                            temp = shopList.split(",");
                            var x = -1;
                            for (i = 0; i < temp.length; i++) {
                                if (temp[i] == shopId) {
                                    x = i;
                                }
                            }
                            if (x == -1) {
                                shopList = shopList + "," + shopId;
                            }
                            localStorage.setItem("shopList", shopList);
                            updateCartValue();
                        } else {
                            localStorage.setItem("cartCount", 1);
                            localStorage.setItem("shopList", shopId);
                            updateCartValue();
                        }
                        storage.put({
                            subItemId: subItemId,
                            mItemId: cursor.value.mainItemId,
                            shopId: shopId,
                            quantity: 1,
                            addOnInfo: cursor.value.addOnInfo,
                            subsInfo: cursor.value.subsInfo,
                            quantInfo: cursor.value.quantInfo
                        });
                        if (document.getElementById("add_btnImg" + subItemId)) {
                            document.getElementById("add_btnImg" + subItemId).innerHTML = "";
                            document.getElementById("add_btnImg" + subItemId).style.backgroundColor = "#ffffff";
                        }
                        document.getElementById("add_btn" + subItemId).innerHTML = "";
                        document.getElementById("add_btn" + subItemId).style.backgroundColor = "#ffffff";
                        var lPrint = "<span id='addbtn'><span style='cursor:pointer;'><a href='javascript:deuctItem(" + subItemId + ")' class=' waves-effect waves-teal'  ><i class='material-icons left' id='remove'  >remove</i></a></span><span id='quanValueImg" + subItemId + "'>1</span><a href='javascript:addItemCount(" + subItemId + ")' class='  waves-effect waves-teal'  ><i class='material-icons right'>add</i></a></span>";
                        $('#add_btnImg' + subItemId).append(lPrint);
                        lPrint = "<span id='addbtn'><span style='cursor:pointer;'><a href='javascript:deuctItem(" + subItemId + ")' class='waves-effect waves-teal' ><i class='material-icons left' id='remove'  >remove</i></a></span><span id='quanValue" + subItemId + "'>1</span><a href='javascript:addItemCount(" + subItemId + ")' class='waves-effect waves-teal' ><i class='material-icons right'>add</i></a></span>";
                        $('#add_btn' + subItemId).append(lPrint);
                        var database1, idb_request1;
                        idb_request1 = window.indexedDB.open("cartShopInfo", 1);
                        idb_request1.addEventListener("success", function(event) {
                            database1 = this.result;
                            var storage1 = database1.transaction("data1", "readwrite").objectStore("data1");
                            var index1 = storage1.index("by_shopId");
                            var request1 = index1.openCursor(IDBKeyRange.only(shopId));
                            request1.onsuccess = function() {
                                var cursor1 = request1.result;
                                if (cursor1) {
                                    var xItemCount = cursor1.value.itemCount;
                                    var itemList = cursor1.value.itemList;
                                    if (xItemCount > 0) {
                                        xItemCount = xItemCount + 1;
                                        itemList = itemList + "," + subItemId;
                                    } else {
                                        xItemCount = xItemCount + 1;
                                        itemList = subItemId;
                                    }
                                    storage1.put({
                                        shopId: shopId,
                                        itemCount: xItemCount,
                                        itemList: itemList
                                    });
                                } else {
                                    storage1.add({
                                        shopId: shopId,
                                        itemCount: 1,
                                        itemList: subItemId
                                    });
                                }
                            }
                        });
                        $('#addOnInfoChoose').modal('close');
                    } else {
                        var subsTemp = new Array();
                        subsTemp = subsInfo.split(",");
                        document.getElementById("selectingSubscriptionId").innerHTML = "";
                        var lPrint = "<option value='' disabled selected>Choose option</option>";
                        $('#selectingSubscriptionId').append(lPrint);
                        for (i = 0; i < subsTemp.length; i++) {
                            var subsData = JSON.parse(localStorage.getItem("subsInfo")).data;
                            $.each(subsData, function(p, v) {
                                if ((v.subscriptionId == subsTemp[i]) && (v.isActive == 1)) {
                                    var lPrint = "<option value=" + v.subscriptionId + ">" + v.noOfDays + " Days @  &#8377 " + v.suscriptionPrice + "Rs</option>"
                                    $('#selectingSubscriptionId').append(lPrint);
                                }
                            });
                        }
                        $('select').material_select();
                        $('#addOnInfoChoose').modal('close');
                        $('#subscriptionChoose').modal('open');
                    }

                }
            }

        });

    });




    /*-------------------This section shows The parallax content------------------------*/

});

function displayItems(itemData, shopId) {
    var database, idb_request;
    idb_request = window.indexedDB.open("cartInfo", 1);
    idb_request.addEventListener("success", function(event) {
        database = this.result;
        var storage = database.transaction("data1", "readwrite").objectStore("data1");
        var index = storage.index("by_subItemId");
        $.each(itemData, function(i, v) {
            var mainItemId = v.mainItemId;
            var isActive = v.isActive;
            if (isActive == 1) {
                var mainItemName = v.Name;
                var subItems = v.subItems;
                var mMainItemId = "m" + mainItemId;
                var lMainItemId = "l"+mainItemId;
                var print = "<li id="+lMainItemId+"><div class='collapsible-header z-depth-3 headerDd textOverLap' style='font-size:1.25em; '><i class='tiny material-icons right'>format_line_spacing</i>" + mainItemName + "</div><div class=collapsible-body style='padding:0px'><span id=" + mMainItemId + "></span></div></li>";
                $('#itemList').append(print);
                var displayCount = 0;
                $.each(subItems, function(k, l) {
                    if (l.isActive == 1) {
                        if((viewType=="subs")&&(l.subsInfo!="")){
                          displayCount = displayCount + 1;
                        }
                        else if(viewType!="subs"){
                          displayCount = displayCount + 1;
                        }
                        var subItemName = l.Name;
                        var subItemId = l.subItemId;
                        var subs, addOn, mrp;
                        if (l.subsInfo == "") {
                            subs = ""
                        } else {
                            subs = ""
                        }
                        if (l.addOnInfo == "") {
                            addOn = ""
                        } else {
                            addOn = ""
                        }
                        var test = 0;
                        var request = index.openCursor(IDBKeyRange.only(subItemId));
                        var addOnInfo = l.addOnInfo;
                        var subsInfo = l.subsInfo;
                        var quantInfo = l.quantInfo;
                        var spclCat = l.spclCat;
                        var spclCatImg = "";
                        if (spclCat != "none") {
                            if (spclCat != "") {
                                spclCatImg = '<span style="padding-right:0.4em;z-index:12;"> <img src="images/' + spclCat + '.png" style="height:0.9em; width:0.9em;"></span>';
                            }
                        }
                        request.onsuccess = function() {
                            var cursor = request.result;
                            if (cursor) {
                                test = cursor.value.quantity;
                                storage.put({
                                    subItemId: subItemId,
                                    mItemId: mainItemId,
                                    shopId: shopId,
                                    quantity: test,
                                    addOnInfo: l.addOnInfo,
                                    subsInfo: l.subsInfo,
                                    quantInfo: l.quantInfo
                                });
                                if((viewType=="subs")&&(l.subsInfo!="")){
                                  displayCount = displayCount + 1;
                                  if (test == 0) {
                                      if (mrp != 0) {
                                          var xPrint = "<div class='card white' style='height:5em;'><div class='card-content' style='color:#1e1e1e; margin-bottom:-0.5em;'><span class='card-title textOverLap' style='margin-top:-1em;font-size:1.2em;'>" + subItemName + "</span><span style='font-size:0.85em;'>" + spclCatImg + " Starting from &#8377 " + mrp + " " + subs + " " + addOn + "</span><span class='para_p' onclick='addToCart(" + subItemId + ")'  id='add_btn" + subItemId + "' name='add_btn" + subItemId + "' style='background-color:#ffffff'><a href='javascript:addToCart(" + subItemId + ")' class='para_an waves-effect waves-teal'>Add</a></span></div></div>"
                                      }
                                  } else {
                                      if (mrp != 0) {
                                          var xPrint = "<div class='card white' style='height:5em;'><div class='card-content' style='color:#1e1e1e; margin-bottom:-0.5em;'><span class='card-title textOverLap' style='margin-top:-1em;font-size:1.2em;'>" + subItemName + "</span><span style='font-size:0.85em;'>" + spclCatImg + " Starting From  &#8377 " + mrp + " " + subs + " " + addOn + "</span><span class='para_p' id='add_btn" + subItemId + "' name='add_btn" + subItemId + "' style='background-color:#000;'><span id='addbtn'><span style='cursor:pointer; color:white;'><a href='javascript:deuctItem(" + subItemId + ")' class='waves-effect waves-teal'><i class='material-icons left' id='remove'>remove</i></a></span><span  id='quanValue" + subItemId + "'>" + test + "</span><span><a href='javascript:addItemCount(" + subItemId + ")' class='waves-effect waves-teal'><i class='material-icons right'>add</i></a> </span></p></div></div>"
                                      }
                                  }
                                  if (l.imgUrl != "") {
                                      $('#Recommended').show();
                                      var imgName = subItemId + ".jpeg";
                                      if (test == 0) {
                                          if (mrp != 0) {
                                              var jPrint = "<div class='col s6 m3'><div class='row z-depth-5'><div class='col s12' style='padding:0; margin:0;'><img src=" + imgItemUrl() + imgName + " style='width:100%; height:100px;'> </div><div class='col s12' style='margin:0; padding:0'><p style='text-align:center;' class='shopCard_name'>" + subItemName + "<p class='shopCard_cat2' style='font-size:0.8em; margin-top:-1.8em;'>" + spclCatImg + " Starting from &#8377 " + mrp + " " + subs + " " + addOn + "</p><span style='align:center;'><div class='para_p1' id='add_btnImg" + subItemId + "' style='background-color:#ffffff'><a href='javascript:addToCart(" + subItemId + ")' class='para_an waves-effect waves-teal' style='margin-bottom:0px;'><p  style='margin-bottom:0px; margin-top:0px;' name='add_btnImg" + subItemId + "'>Add</p></a></div ></span></div></div></div>";
                                          }
                                      } else {
                                          if (mrp != 0) {
                                              var jPrint = "<div class='col s6 m3'><div class='row z-depth-5'><div class='col s12' style='padding:0; margin:0;'><img src=" + imgItemUrl() + imgName + " style='width:100%; height:100px;'> </div><div class='col s12' style='margin:0; padding:0'><p style='text-align:center;' class='shopCard_name'>" + subItemName + "<p class='shopCard_cat2' style='font-size:0.8em; margin-top:-1.8em;'>" + spclCatImg + " Starting From  &#8377 " + mrp + " " + subs + " " + addOn + "</p><div class='para_p1'  id='add_btnImg" + subItemId + "'  name='add_btnImg" + subItemId + "' style='background-color:#ffffff;'><span id='addbtn'><span style='cursor:pointer;'><a href='javascript:deuctItem(" + subItemId + ")' class=' waves-effect waves-teal'><i class='material-icons left' id='remove' >remove</i></a></span><span id='quanValueImg" + subItemId + "'>" + test + "</span><a href='javascript:addItemCount(" + subItemId + ")' class=' waves-effect waves-teal'><i class='material-icons right'>add</i></a></span></div></div></div></div>";
                                          }
                                      }
                                      $('#featured').append(jPrint);
                                  }
                                  $('#' + mMainItemId).append(xPrint);
                                }
                                else if(viewType!="subs"){
                                  displayCount = displayCount + 1;
                                  if (test == 0) {
                                      if (mrp != 0) {
                                          var xPrint = "<div class='card white' style='height:5em;'><div class='card-content' style='color:#1e1e1e; margin-bottom:-0.5em;'><span class='card-title textOverLap' style='margin-top:-1em;font-size:1.2em;'>" + subItemName + "</span><span style='font-size:0.85em;'>" + spclCatImg + " Starting from &#8377 " + mrp + " " + subs + " " + addOn + "</span><span class='para_p' id='add_btn" + subItemId + "' name='add_btn" + subItemId + "' style='background-color:#ffffff'><a href='javascript:addToCart(" + subItemId + ")' class='para_an waves-effect waves-teal'>Add</a></span></div></div>"
                                      }
                                  } else {
                                      if (mrp != 0) {
                                          var xPrint = "<div class='card white' style='height:5em;'><div class='card-content' style='color:#1e1e1e; margin-bottom:-0.5em;'><span class='card-title textOverLap' style='margin-top:-1em;font-size:1.2em;'>" + subItemName + "</span><span style='font-size:0.85em;'>" + spclCatImg + " Starting From  &#8377 " + mrp + " " + subs + " " + addOn + "</span><span class='para_p' id='add_btn" + subItemId + "' name='add_btn" + subItemId + "' style='background-color:#ffffff;'><span id='addbtn'><span style='cursor:pointer; color:white;'><a href='javascript:deuctItem(" + subItemId + ")' class='waves-effect waves-teal'><i class='material-icons left' id='remove'>remove</i></a></span><span  id='quanValue" + subItemId + "'>" + test + "</span><span><a href='javascript:addItemCount(" + subItemId + ")' class='waves-effect waves-teal'><i class='material-icons right'>add</i></a> </span></p></div></div>"
                                      }
                                  }
                                  if (l.imgUrl != "") {
                                      $('#Recommended').show();
                                      var imgName = subItemId + ".jpeg";
                                      if (test == 0) {
                                          if (mrp != 0) {
                                              var jPrint = "<div class='col s6 m3'><div class='row z-depth-5'><div class='col s12' style='padding:0; margin:0;'><img src=" + imgItemUrl() + imgName + " style='width:100%; height:100px;'> </div><div class='col s12' style='margin:0; padding:0'><p style='text-align:center;' class='shopCard_name'>" + subItemName + "<p class='shopCard_cat2' style='font-size:0.8em; margin-top:-1.8em;'>" + spclCatImg + " Starting from &#8377 " + mrp + " " + subs + " " + addOn + "</p><span style='align:center;'><div class='para_p1' id='add_btnImg" + subItemId + "' style='background-color:#ffffff'><a href='javascript:addToCart(" + subItemId + ")' class='para_an waves-effect waves-teal' style='margin-bottom:0px;'><p  style='margin-bottom:0px; margin-top:0px;' name='add_btnImg" + subItemId + "'>Add</p></a></div ></span></div></div></div>";
                                          }
                                      } else {
                                          if (mrp != 0) {
                                              var jPrint = "<div class='col s6 m3'><div class='row z-depth-5'><div class='col s12' style='padding:0; margin:0;'><img src=" + imgItemUrl() + imgName + " style='width:100%; height:100px;'> </div><div class='col s12' style='margin:0; padding:0'><p style='text-align:center;' class='shopCard_name'>" + subItemName + "<p class='shopCard_cat2' style='font-size:0.8em; margin-top:-1.8em;'>" + spclCatImg + " Starting From  &#8377 " + mrp + " " + subs + " " + addOn + "</p><div class='para_p1'  id='add_btnImg" + subItemId + "'  name='add_btnImg" + subItemId + "' style='background-color:#ffffff;'><span id='addbtn'><span style='cursor:pointer;'><a href='javascript:deuctItem(" + subItemId + ")' class=' waves-effect waves-teal'><i class='material-icons left' id='remove' >remove</i></a></span><span id='quanValueImg" + subItemId + "'>" + test + "</span><a href='javascript:addItemCount(" + subItemId + ")' class=' waves-effect waves-teal'><i class='material-icons right'>add</i></a></span></div></div></div></div>";
                                          }
                                      }
                                      $('#featured').append(jPrint);
                                  }
                                  $('#' + mMainItemId).append(xPrint);
                                }
                            } else {
                                storage.add({
                                    subItemId: subItemId,
                                    mItemId: mainItemId,
                                    shopId: shopId,
                                    quantity: test,
                                    addOnInfo: l.addOnInfo,
                                    subsInfo: l.subsInfo,
                                    quantInfo: l.quantInfo
                                });
                                if((viewType=="subs")&&(l.subsInfo!="")){
                                  displayCount = displayCount + 1;
                                  if (mrp != 0) {
                                      var xPrint = "<div class='card white' style='height:5em;'><div class='card-content' style='color:#1e1e1e; margin-bottom:-0.5em;'><span class='card-title textOverLap' style='margin-top:-1em;font-size:1.2em;'>" + subItemName + "</span><span style='font-size:0.85em;'>" + spclCatImg + " Starting from &#8377 " + mrp + " " + subs + " " + addOn + "</span><span class='para_p' id='add_btn" + subItemId + "' name='add_btn" + subItemId + "' style='background-color:#ffffff'><a href='javascript:addToCart(" + subItemId + ")' class='para_an waves-effect waves-teal'>Add</a></span></div></div>"
                                      if (l.imgUrl != "") {
                                          $('#Recommended').show();
                                          var imgName = subItemId + ".jpeg";
                                          var jPrint = "<div class='col s6 m3'><div class='row z-depth-5'><div class='col s12' style='padding:0; margin:0;'><img src=" + imgItemUrl() + imgName + " style='width:100%; height:100px;'> </div><div class='col s12' style='margin:0; padding:0'><p style='text-align:center;' class='shopCard_name'>" + subItemName + "<p class='shopCard_cat2' style='font-size:0.8em; margin-top:-1.8em;'>" + spclCatImg + " Starting from &#8377 " + mrp + " " + subs + " " + addOn + "</p><div class='para_p1' id='add_btnImg" + subItemId + "' style='background-color:#ffffff'><a href='javascript:addToCart(" + subItemId + ")' class='para_an waves-effect waves-teal' style='margin-bottom:0px;'><p  style='margin-bottom:0px; margin-top:0px;' name='add_btnImg" + subItemId + "'>Add</p></a></div ></div></div></div>";
                                          $('#featured').append(jPrint);
                                      }
                                      $('#' + mMainItemId).append(xPrint);
                                  }
                                }
                                else if(viewType!="subs"){
                                  displayCount = displayCount + 1;
                                  if (mrp != 0) {
                                      var xPrint = "<div class='card white' style='height:5em;'><div class='card-content' style='color:#1e1e1e; margin-bottom:-0.5em;'><span class='card-title textOverLap' style='margin-top:-1em;font-size:1.2em;'>" + subItemName + "</span><span style='font-size:0.85em;'>" + spclCatImg + " Starting from &#8377 " + mrp + " " + subs + " " + addOn + "</span><span class='para_p' id='add_btn" + subItemId + "' name='add_btn" + subItemId + "' style='background-color:#ffffff'><a href='javascript:addToCart(" + subItemId + ")' class='para_an waves-effect waves-teal'>Add</a></span></div></div>"
                                      if (l.imgUrl != "") {
                                          $('#Recommended').show();
                                          var imgName = subItemId + ".jpeg";
                                          var jPrint = "<div class='col s6 m3'><div class='row z-depth-5'><div class='col s12' style='padding:0; margin:0;'><img src=" + imgItemUrl() + imgName + " style='width:100%; height:100px;'> </div><div class='col s12' style='margin:0; padding:0'><p style='text-align:center;' class='shopCard_name'>" + subItemName + "<p class='shopCard_cat2' style='font-size:0.8em; margin-top:-1.8em;'>" + spclCatImg + " Starting from &#8377 " + mrp + " " + subs + " " + addOn + "</p><div class='para_p1' id='add_btnImg" + subItemId + "' style='background-color:#ffffff'><a href='javascript:addToCart(" + subItemId + ")' class='para_an waves-effect waves-teal' style='margin-bottom:0px;'><p  style='margin-bottom:0px; margin-top:0px;' name='add_btnImg" + subItemId + "'>Add</p></a></div ></div></div></div>";
                                          $('#featured').append(jPrint);
                                      }
                                      $('#' + mMainItemId).append(xPrint);
                                  }
                                }
                            }
                        }
                        mrp = 0;
                       $.each(l.quantInfo, function(a, b) {
                            if (mrp == 0) {
                                mrp = b.value;
                            } else if (mrp > b.value) {
                                mrp = b.value;
                            }
                        });
                    }
                });
                if(displayCount==0){
                    $('#' + lMainItemId).hide();
                }
            }
        });
    });
}

function addItemCount(subItemId) {
    var database, idb_request;
    idb_request = window.indexedDB.open("cartInfo", 1);
    idb_request.addEventListener("success", function(event) {
        database = this.result;
        var storage = database.transaction("data1", "readwrite").objectStore("data1");
        var index = storage.index("by_subItemId");
        var request = index.openCursor(IDBKeyRange.only(subItemId));
        request.onsuccess = function() {
            var cursor = request.result;
            if (cursor) {
                var quantity = Number(cursor.value.quantity);
                quantity = quantity + 1;
                storage.put({
                    subItemId: subItemId,
                    mItemId: cursor.value.mainItemId,
                    shopId: cursor.value.shopId,
                    quantity: quantity,
                    addOnInfo: cursor.value.addOnInfo,
                    subsInfo: cursor.value.subsInfo,
                    quantInfo: cursor.value.quantInfo
                });
                if (document.getElementById("quanValueImg" + subItemId)) {
                    document.getElementById("quanValueImg" + subItemId).innerHTML = "";
                }
                document.getElementById("quanValue" + subItemId).innerHTML = "";
                $('#quanValueImg' + subItemId).append(quantity);
                $('#quanValue' + subItemId).append(quantity);
            }
        }
    });
}

function addToCart(subItemId) {
    localStorage.setItem("currentyUsingItem", subItemId);
    var database, idb_request;
    idb_request = window.indexedDB.open("cartInfo", 1);
    idb_request.addEventListener("success", function(event) {
        database = this.result;
        var storage = database.transaction("data1", "readwrite").objectStore("data1");
        var index = storage.index("by_subItemId");
        var request = index.openCursor(IDBKeyRange.only(subItemId));
        request.onsuccess = function() {
            var cursor = request.result;
            if (cursor) {
                var quantInfo = JSON.parse(JSON.stringify(cursor.value.quantInfo));
                var addOnInfo = cursor.value.addOnInfo;
                var subsInfo = cursor.value.subsInfo;
                var shopId = cursor.value.shopId;
                if ((quantInfo.length == 1) && (addOnInfo == "") && (subsInfo == "")) {
                    var database2, idb_request2;
                    idb_request2 = window.indexedDB.open("itemDesc", 1);
                    idb_request2.addEventListener("success", function(event) {
                        database2 = this.result;
                        var storage2 = database2.transaction("data1", "readwrite").objectStore("data1");
                        var index2 = storage2.index("by_subItemId");
                        var request2 = index2.openCursor(IDBKeyRange.only(subItemId));
                        request2.onsuccess = function() {
                            storage2.add({
                                subItemId: subItemId,
                                quantId: quantInfo[0].quantId,
                                addOnInfo: addOnInfo,
                                subsInfo: subsInfo
                            });
                        }
                    });
                    if (localStorage.getItem("cartCount")) {
                        var cartCount = localStorage.getItem("cartCount");
                        cartCount = Number(cartCount) + 1;
                        localStorage.setItem("cartCount", cartCount);
                        var shopList = localStorage.getItem("shopList")
                        var temp = new Array();
                        temp = shopList.split(",");
                        var x = -1;
                        for (i = 0; i < temp.length; i++) {
                            if (temp[i] == shopId) {
                                x = i;
                            }
                        }
                        if (x == -1) {
                            shopList = shopList + "," + shopId;
                        }
                        localStorage.setItem("shopList", shopList);
                        updateCartValue();
                    } else {
                        localStorage.setItem("cartCount", 1);
                        localStorage.setItem("shopList", shopId);
                        updateCartValue();
                    }
                    storage.put({
                        subItemId: subItemId,
                        mItemId: cursor.value.mainItemId,
                        shopId: shopId,
                        quantity: 1,
                        addOnInfo: cursor.value.addOnInfo,
                        subsInfo: cursor.value.subsInfo,
                        quantInfo: cursor.value.quantInfo
                    });
                    if (document.getElementById("add_btnImg" + subItemId)) {
                        document.getElementById("add_btnImg" + subItemId).innerHTML = "";
                        document.getElementById("add_btnImg" + subItemId).style.backgroundColor = "#ffffff";
                    }
                    document.getElementById("add_btn" + subItemId).innerHTML = "";
                    document.getElementById("add_btn" + subItemId).style.backgroundColor = "#ffffff";
                    var lPrint = "<span id='addbtn'><span style='cursor:pointer;'><a href='javascript:deuctItem(" + subItemId + ")'  class=' waves-effect waves-teal'><i class='material-icons left' id='remove'  >remove</i></a></span><span id='quanValueImg" + subItemId + "'>1</span><a href='javascript:addItemCount(" + subItemId + ")'  class='waves-effect waves-teal'><i class='material-icons right'>add</i></a></span>";
                    $('#add_btnImg' + subItemId).append(lPrint);
                    lPrint = "<span id='addbtn'><span style='cursor:pointer;'><a href='javascript:deuctItem(" + subItemId + ")'  class='waves-effect waves-teal'><i class='material-icons left' id='remove'>remove</i></a></span><span id='quanValue" + subItemId + "'>1</span><a href='javascript:addItemCount(" + subItemId + ")'  class='waves-effect waves-teal'><i class='material-icons right'>add</i></a></span>";
                    $('#add_btn' + subItemId).append(lPrint);
                    var database1, idb_request1;
                    idb_request1 = window.indexedDB.open("cartShopInfo", 1);
                    idb_request1.addEventListener("success", function(event) {
                        database1 = this.result;
                        var storage1 = database1.transaction("data1", "readwrite").objectStore("data1");
                        var index1 = storage1.index("by_shopId");
                        var request1 = index1.openCursor(IDBKeyRange.only(shopId));
                        request1.onsuccess = function() {
                            var cursor1 = request1.result;
                            if (cursor1) {
                                var xItemCount = cursor1.value.itemCount;
                                var itemList = cursor1.value.itemList;
                                if (xItemCount > 0) {
                                    xItemCount = xItemCount + 1;
                                    itemList = itemList + "," + subItemId;
                                } else {
                                    xItemCount = xItemCount + 1;
                                    itemList = subItemId;
                                }
                                storage1.put({
                                    shopId: shopId,
                                    itemCount: xItemCount,
                                    itemList: itemList
                                });
                            } else {
                                storage1.add({
                                    shopId: shopId,
                                    itemCount: 1,
                                    itemList: subItemId
                                });
                            }
                        }
                    });
                } else if (quantInfo.length > 1) {
                    document.getElementById("quanInfoItem").innerHTML = "";
                    $.each(quantInfo, function(i, v) {
                        var kPrint = "<tr><td style='text-align:center;'>" + v.quantity + "</td><td style='text-align:center;color:red;'><del>" + v.mrp + "</del></td><td style='text-align:center;color:#000;'>" + v.value + "</td><td style='text-align:center;'><a href=javascript:workWithQuantity(" + v.quantId + "," + subItemId + ") style='background-color:#f7f7f7;' class='btn-floating btn-small waves-effect waves-light'><i class='material-icons' style='color:#ffa751;'>check</i></a></td></tr>";
                        $('#quanInfoItem').append(kPrint);
                    });
                    var database2, idb_request2;
                    idb_request2 = window.indexedDB.open("itemDesc", 1);
                    idb_request2.addEventListener("success", function(event) {
                        database2 = this.result;
                        var storage2 = database2.transaction("data1", "readwrite").objectStore("data1");
                        var index2 = storage2.index("by_subItemId");
                        var request2 = index2.openCursor(IDBKeyRange.only(subItemId));
                        request2.onsuccess = function() {
                            storage2.add({
                                subItemId: subItemId,
                                quantId: 0,
                                addOnInfo: "",
                                subsInfo: ""
                            });
                        }
                    });
                    $('#quantInfoChoose').modal('open');
                } else if ((quantInfo.length == 1) && (addOnInfo != "")) {
                    var database2, idb_request2;
                    idb_request2 = window.indexedDB.open("itemDesc", 1);
                    idb_request2.addEventListener("success", function(event) {
                        database2 = this.result;
                        var storage2 = database2.transaction("data1", "readwrite").objectStore("data1");
                        var index2 = storage2.index("by_subItemId");
                        var request2 = index2.openCursor(IDBKeyRange.only(subItemId));
                        request2.onsuccess = function() {
                            storage2.add({
                                subItemId: subItemId,
                                quantId: quantInfo[0].quantId,
                                addOnInfo: "",
                                subsInfo: ""
                            });
                        }
                    });
                    var temp = new Array();
                    temp = addOnInfo.split(",");
                    var x = -1;
                    document.getElementById('selectingAddOnInfo').innerHTML = "";
                    for (i = 0; i < temp.length; i++) {
                        var addOnInfoUseData = JSON.parse(localStorage.getItem("addOnInfo")).data;

                        $.each(addOnInfoUseData, function(c, d) {

                            if ((d.groupId == temp[i]) && (d.isActiveFl == 1)) {
                                if (d.isMultiSelect == 1) {
                                    var addPrint = "<div class='input-field col s12'><select multiple style='margin-top:2em;' id='addOn" + d.groupId + "'><option value='' disabled selected>Choose option</option></select><label style='font-size:1.4em; color:#424242'>" + d.groupName + "@    &#8377 " + d.groupCharge + "</label></div>";
                                } else {
                                    var addPrint = "<div class='input-field col s12'><select style='margin-top:2em;' id='addOn" + d.groupId + "'><option value='' disabled selected>Choose option</option></select><label style='font-size:1.4em; color:#424242'>" + d.groupName + "@    &#8377 " + d.groupCharge + "</label></div>";
                                }
                                $('#selectingAddOnInfo').append(addPrint);
                                var addItemInfo = d.itemInfo;
                                $.each(addItemInfo, function(o, p) {
                                    if (p.isActiveFl == 1) {
                                        var itemPrint = "<option value=" + p.addOnItemId + ">" + p.addOnItemName + "</option>";
                                        $("#addOn" + d.groupId).append(itemPrint);
                                    }
                                });
                                $('select').material_select();
                                localStorage.setItem("currentyUsingItem", subItemId)
                            }
                        });
                        $('#addOnInfoChoose').modal('open');
                    }

                } else if ((quantInfo.length == 1) && (subsInfo != "")) {
                    var database2, idb_request2;
                    idb_request2 = window.indexedDB.open("itemDesc", 1);
                    idb_request2.addEventListener("success", function(event) {
                        database2 = this.result;
                        var storage2 = database2.transaction("data1", "readwrite").objectStore("data1");
                        var index2 = storage2.index("by_subItemId");
                        var request2 = index2.openCursor(IDBKeyRange.only(subItemId));
                        request2.onsuccess = function() {
                            storage2.add({
                                subItemId: subItemId,
                                quantId: quantInfo[0].quantId,
                                addOnInfo: "",
                                subsInfo: ""
                            });
                        }
                    });
                    var subsTemp = new Array();
                    subsTemp = subsInfo.split(",");
                    document.getElementById("selectingSubscriptionId").innerHTML = "";
                    var lPrint = "<option value='' disabled selected>Choose option</option>";
                    $('#selectingSubscriptionId').append(lPrint);
                    for (i = 0; i < subsTemp.length; i++) {
                        var subsData = JSON.parse(localStorage.getItem("subsInfo")).data;
                        $.each(subsData, function(p, v) {
                            if ((v.subscriptionId == subsTemp[i]) && (v.isActive == 1)) {
                                var lPrint = "<option value=" + v.subscriptionId + ">" + v.noOfDays + " Days @  &#8377 " + v.suscriptionPrice + "Rs</option>"
                                $('#selectingSubscriptionId').append(lPrint);
                            }
                        });
                    }
                    $('select').material_select();
                    $('#subscriptionChoose').modal('open');
                } else {

                }
            }
        }
    })
}

function workWithQuantity(quantId, subItemId) {
    var database2, idb_request2;
    idb_request2 = window.indexedDB.open("itemDesc", 1);
    idb_request2.addEventListener("success", function(event) {
        database2 = this.result;
        var storage2 = database2.transaction("data1", "readwrite").objectStore("data1");
        var index2 = storage2.index("by_subItemId");
        var request2 = index2.openCursor(IDBKeyRange.only(subItemId));
        request2.onsuccess = function() {
            storage2.put({
                subItemId: subItemId,
                quantId: quantId,
                addOnInfo: "",
                subsInfo: ""
            });
        }
    });
    var database, idb_request;
    idb_request = window.indexedDB.open("cartInfo", 1);
    idb_request.addEventListener("success", function(event) {
        database = this.result;
        var storage = database.transaction("data1", "readwrite").objectStore("data1");
        var index = storage.index("by_subItemId");
        var request = index.openCursor(IDBKeyRange.only(subItemId));
        request.onsuccess = function() {
            var cursor = request.result;
            if (cursor) {
                var addOnInfo = cursor.value.addOnInfo;
                var subsInfo = cursor.value.subsInfo;
                var shopId = cursor.value.shopId;
                if ((addOnInfo == "") && (subsInfo == "")) {
                    storage.put({
                        subItemId: subItemId,
                        mItemId: cursor.value.mainItemId,
                        shopId: cursor.value.shopId,
                        quantity: 1,
                        addOnInfo: cursor.value.addOnInfo,
                        subsInfo: cursor.value.subsInfo,
                        quantInfo: cursor.value.quantInfo
                    });
                    if (localStorage.getItem("cartCount")) {
                        var cartCount = localStorage.getItem("cartCount");
                        cartCount = Number(cartCount) + 1;
                        localStorage.setItem("cartCount", cartCount);
                        var shopList = localStorage.getItem("shopList")
                        var temp = new Array();
                        temp = shopList.split(",");
                        var x = -1;
                        for (i = 0; i < temp.length; i++) {
                            if (temp[i] == shopId) {
                                x = i;
                            }
                        }
                        if (x == -1) {
                            shopList = shopList + "," + shopId;
                        }
                        localStorage.setItem("shopList", shopList);
                        updateCartValue();
                    } else {
                        localStorage.setItem("cartCount", 1);
                        localStorage.setItem("shopList", shopId);
                        updateCartValue();
                    }
                    storage.put({
                        subItemId: subItemId,
                        mItemId: cursor.value.mainItemId,
                        shopId: shopId,
                        quantity: 1,
                        addOnInfo: cursor.value.addOnInfo,
                        subsInfo: cursor.value.subsInfo,
                        quantInfo: cursor.value.quantInfo
                    });
                    if (document.getElementById("add_btnImg" + subItemId)) {
                        document.getElementById("add_btnImg" + subItemId).innerHTML = "";
                        document.getElementById("add_btnImg" + subItemId).style.backgroundColor = "#ffffff";
                    }
                    document.getElementById("add_btn" + subItemId).innerHTML = "";
                    document.getElementById("add_btn" + subItemId).style.backgroundColor = "#ffffff";
                    var lPrint = "<span id='addbtn'><span style='cursor:pointer;'><a href='javascript:deuctItem(" + subItemId + ")' class='waves-effect waves-teal'><i class='material-icons left' id='remove'>remove</i></a></span><span id='quanValueImg" + subItemId + "'>1</span><a href='javascript:addItemCount(" + subItemId + ")' class='waves-effect waves-teal'><i class='material-icons right'>add</i></a></span>";
                    $('#add_btnImg' + subItemId).append(lPrint);
                    lPrint = "<span id='addbtn'><span style='cursor:pointer;'><a href='javascript:deuctItem(" + subItemId + ")'  class='waves-effect waves-teal'><i class='material-icons left' id='remove' >remove</i></a></span><span id='quanValue" + subItemId + "'>1</span><a href='javascript:addItemCount(" + subItemId + ")'  class=' waves-effect waves-teal'><i class='material-icons right'>add</i></a></span>";
                    $('#add_btn' + subItemId).append(lPrint);
                    var database1, idb_request1;
                    idb_request1 = window.indexedDB.open("cartShopInfo", 1);
                    idb_request1.addEventListener("success", function(event) {
                        database1 = this.result;
                        var storage1 = database1.transaction("data1", "readwrite").objectStore("data1");
                        var index1 = storage1.index("by_shopId");
                        var request1 = index1.openCursor(IDBKeyRange.only(shopId));
                        request1.onsuccess = function() {
                            var cursor1 = request1.result;
                            if (cursor1) {
                                var xItemCount = cursor1.value.itemCount;
                                var itemList = cursor1.value.itemList;
                                if (xItemCount > 0) {
                                    xItemCount = xItemCount + 1;
                                    itemList = itemList + "," + subItemId;
                                } else {
                                    xItemCount = xItemCount + 1;
                                    itemList = subItemId;
                                }
                                storage1.put({
                                    shopId: shopId,
                                    itemCount: xItemCount,
                                    itemList: itemList
                                });
                            } else {
                                storage1.add({
                                    shopId: shopId,
                                    itemCount: 1,
                                    itemList: subItemId
                                });
                            }
                        }
                    });
                    $('#quantInfoChoose').modal('close');
                } else if (addOnInfo != "") {
                    $('#quantInfoChoose').modal('close');
                    var temp = new Array();
                    temp = addOnInfo.split(",");
                    var x = -1;
                    document.getElementById('selectingAddOnInfo').innerHTML = "";
                    for (i = 0; i < temp.length; i++) {
                        var addOnInfoUseData = JSON.parse(localStorage.getItem("addOnInfo")).data;

                        $.each(addOnInfoUseData, function(c, d) {

                            if ((d.groupId == temp[i]) && (d.isActiveFl == 1)) {
                                if (d.isMultiSelect == 1) {
                                    var addPrint = "<div class='input-field col s12'><select multiple style='margin-top:2em;' id='addOn" + d.groupId + "'><option value='' disabled selected>Choose option</option></select><label style='font-size:1.4em; color:#424242'>" + d.groupName + "@    &#8377 " + d.groupCharge + "</label></div>";
                                } else {
                                    var addPrint = "<div class='input-field col s12'><select style='margin-top:2em;' id='addOn" + d.groupId + "'><option value='' disabled selected>Choose option</option></select><label style='font-size:1.4em; color:#424242'>" + d.groupName + "@    &#8377 " + d.groupCharge + "</label></div>";
                                }
                                $('#selectingAddOnInfo').append(addPrint);
                                var addItemInfo = d.itemInfo;
                                $.each(addItemInfo, function(o, p) {
                                    if (p.isActiveFl == 1) {
                                        var itemPrint = "<option value=" + p.addOnItemId + ">" + p.addOnItemName + "</option>";
                                        $("#addOn" + d.groupId).append(itemPrint);
                                    }
                                });
                                $('select').material_select();
                                localStorage.setItem("currentyUsingItem", subItemId)
                            }
                        });
                        $('#addOnInfoChoose').modal('open');
                    }
                } else if (subsInfo != "") {
                    var subsTemp = new Array();
                    subsTemp = subsInfo.split(",");
                    document.getElementById("selectingSubscriptionId").innerHTML = "";
                    var lPrint = "<option value='' disabled selected>Choose option</option>";
                    $('#selectingSubscriptionId').append(lPrint);
                    for (i = 0; i < subsTemp.length; i++) {
                        var subsData = JSON.parse(localStorage.getItem("subsInfo")).data;
                        $.each(subsData, function(p, v) {
                            if ((v.subscriptionId == subsTemp[i]) && (v.isActive == 1)) {
                                var lPrint = "<option value=" + v.subscriptionId + ">" + v.noOfDays + " Days @  &#8377 " + v.suscriptionPrice + "Rs</option>"
                                $('#selectingSubscriptionId').append(lPrint);
                            }
                        });
                    }
                    $('select').material_select();
                    $('#quantInfoChoose').modal('close');
                    $('#subscriptionChoose').modal('open');
                }
            }
        }
    });
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function mart() {
    window.location.replace(feUrl() + "/cart.html");
}

function redirect() {
    window.location.replace(feUrl() + "/shops.html");
}

function deuctItem(subItemId) {
    var database, idb_request;
    idb_request = window.indexedDB.open("cartInfo", 1);
    idb_request.addEventListener("success", function(event) {
        database = this.result;
        var storage = database.transaction("data1", "readwrite").objectStore("data1");
        var index = storage.index("by_subItemId");
        var request = index.openCursor(IDBKeyRange.only(subItemId));
        request.onsuccess = function() {
            var cursor = request.result;
            if (cursor) {
                var quantity = Number(cursor.value.quantity);
                if (quantity > 1) {
                    quantity = quantity - 1;
                    storage.put({
                        subItemId: subItemId,
                        mItemId: cursor.value.mainItemId,
                        shopId: cursor.value.shopId,
                        quantity: quantity,
                        addOnInfo: cursor.value.addOnInfo,
                        subsInfo: cursor.value.subsInfo,
                        quantInfo: cursor.value.quantInfo
                    });
                    if (document.getElementById("quanValueImg" + subItemId)) {
                        document.getElementById("quanValueImg" + subItemId).innerHTML = "";
                    }
                    document.getElementById("quanValue" + subItemId).innerHTML = "";
                    $('#quanValueImg' + subItemId).append(quantity);
                    $('#quanValue' + subItemId).append(quantity);
                } else {
                    quantity = quantity - 1;
                    storage.put({
                        subItemId: subItemId,
                        mItemId: cursor.value.mainItemId,
                        shopId: cursor.value.shopId,
                        quantity: quantity,
                        addOnInfo: cursor.value.addOnInfo,
                        subsInfo: cursor.value.subsInfo,
                        quantInfo: cursor.value.quantInfo
                    });
                    if (document.getElementById("quanValueImg" + subItemId)) {
                        document.getElementById("quanValueImg" + subItemId).innerHTML = "";
                    }
                    document.getElementById("quanValue" + subItemId).innerHTML = "";
                    $('#quanValueImg' + subItemId).append(quantity);
                    $('#quanValue' + subItemId).append(quantity);
                    var shopId = cursor.value.shopId;
                    var database2, idb_request2;
                    idb_request2 = window.indexedDB.open("itemDesc", 1);
                    idb_request2.addEventListener("success", function(event) {
                        database2 = this.result;
                        var storage2 = database2.transaction("data1", "readwrite").objectStore("data1");
                        var index2 = storage2.index("by_subItemId");
                        var request2 = index2.openCursor(IDBKeyRange.only(subItemId));
                        request2.onsuccess = function() {
                            var cursor2 = request2.result;
                            if (cursor2) {
                                storage2.put({
                                    subItemId: subItemId,
                                    quantId: cursor2.value.quantId,
                                    addOnInfo: "",
                                    subsInfo: ""
                                });
                            }
                        }
                    });
                    var database1, idb_request1;
                    idb_request1 = window.indexedDB.open("cartShopInfo", 1);
                    idb_request1.addEventListener("success", function(event) {
                        database1 = this.result;
                        var storage1 = database1.transaction("data1", "readwrite").objectStore("data1");
                        var index1 = storage1.index("by_shopId");
                        var request1 = index1.openCursor(IDBKeyRange.only(shopId));
                        request1.onsuccess = function() {
                            var cursor1 = request1.result;
                            if (cursor1) {
                                var xItemCount = Number(cursor1.value.itemCount);
                                var itemList = cursor1.value.itemList;
                                if (xItemCount > 1) {
                                    var temp = new Array();
                                    temp = itemList.split(",");
                                    var x = -1;
                                    for (i = 0; i < xItemCount; i++) {
                                        if (temp[i] == subItemId) {
                                            x = i;
                                        }
                                    }
                                    if (x > -1) {
                                        temp.splice(x, 1);
                                    }
                                    itemList = temp.join(",");
                                    xItemCount = xItemCount - 1;
                                    storage1.put({
                                        shopId: shopId,
                                        itemCount: xItemCount,
                                        itemList: itemList
                                    });
                                } else {
                                    xItemCount = xItemCount - 1;
                                    storage1.put({
                                        shopId: shopId,
                                        itemCount: xItemCount
                                    });
                                }
                            }
                        }

                    });
                    var cartCount = Number(localStorage.getItem('cartCount'));
                    cartCount = cartCount - 1;
                    localStorage.setItem('cartCount', cartCount);
                    if (document.getElementById("add_btnImg" + subItemId)) {
                        document.getElementById("add_btnImg" + subItemId).innerHTML = "";
                        document.getElementById("add_btnImg" + subItemId).style.backgroundColor = "#ffffff";
                    }
                    document.getElementById("add_btn" + subItemId).innerHTML = "";
                    document.getElementById("add_btn" + subItemId).style.backgroundColor = "#ffffff";
                    var lPrint = "<a href='javascript:addToCart(" + subItemId + ")' class='para_an waves-effect waves-teal'>Add</a>";
                    $('#add_btnImg' + subItemId).append(lPrint);
                    $('#add_btn' + subItemId).append(lPrint);
                    updateCartValue();
                }
            }
        }

    });
}

function cartcheck() {
    if (!localStorage.getItem('cartCount') || localStorage.getItem('cartCount') <= 0) {

        Materialize.toast('Cart is empty', 1000, 'rounded');

    } else {
        window.location.replace('cart.html');
    }
}


function skipThisPage(){

        var usingObj = {};
        usingObj['addOnInfo'] = [];

        event.preventDefault();
        var subItemId = Number(localStorage.getItem("currentyUsingItem"));
        var database, idb_request;
        idb_request = window.indexedDB.open("cartInfo", 1);
        idb_request.addEventListener("success", function(event) {
            database = this.result;
            var storage = database.transaction("data1", "readwrite").objectStore("data1");
            var index = storage.index("by_subItemId");
            var request = index.openCursor(IDBKeyRange.only(subItemId));
            request.onsuccess = function() {
                var cursor = request.result;
                if (cursor) {
                    var addOnInfo = cursor.value.addOnInfo;
                    var temp = new Array();
                    temp = addOnInfo.split(",");
                    var x = -1;
                    for (i = 0; i < temp.length; i++) {
                        var addNameUse = "addOn" + temp[i];
                        var isSelected = $("#" + addNameUse).val();;
                        var temp1 = new Array();
                        temp1 = isSelected;
                    }
                    if (x == -1) {
                        usingObj = "";
                    }
                    var database2, idb_request2;
                    idb_request2 = window.indexedDB.open("itemDesc", 1);
                    idb_request2.addEventListener("success", function(event) {
                        database2 = this.result;
                        var storage2 = database2.transaction("data1", "readwrite").objectStore("data1");
                        var index2 = storage2.index("by_subItemId");
                        var request2 = index2.openCursor(IDBKeyRange.only(subItemId));
                        request2.onsuccess = function() {
                            var cursor2 = request2.result;
                            if (cursor2) {
                                storage2.put({
                                    subItemId: subItemId,
                                    quantId: cursor2.value.quantId,
                                    addOnInfo: usingObj,
                                    subsInfo: ""
                                });
                            }
                        }
                    });
                    var subsInfo = cursor.value.subsInfo;
                    if (subsInfo == "") {
                        if (localStorage.getItem("cartCount")) {
                            var cartCount = localStorage.getItem("cartCount");
                            cartCount = Number(cartCount) + 1;
                            localStorage.setItem("cartCount", cartCount);
                            var shopList = localStorage.getItem("shopList")
                            var temp = new Array();
                            temp = shopList.split(",");
                            var x = -1;
                            for (i = 0; i < temp.length; i++) {
                                if (temp[i] == shopId) {
                                    x = i;
                                }
                            }
                            if (x == -1) {
                                shopList = shopList + "," + shopId;
                            }
                            localStorage.setItem("shopList", shopList);
                            updateCartValue();
                        } else {
                            localStorage.setItem("cartCount", 1);
                            localStorage.setItem("shopList", shopId);
                            updateCartValue();
                        }
                        storage.put({
                            subItemId: subItemId,
                            mItemId: cursor.value.mainItemId,
                            shopId: shopId,
                            quantity: 1,
                            addOnInfo: cursor.value.addOnInfo,
                            subsInfo: cursor.value.subsInfo,
                            quantInfo: cursor.value.quantInfo
                        });
                        if (document.getElementById("add_btnImg" + subItemId)) {
                            document.getElementById("add_btnImg" + subItemId).innerHTML = "";
                            document.getElementById("add_btnImg" + subItemId).style.backgroundColor = "#ffffff";
                        }
                        document.getElementById("add_btn" + subItemId).innerHTML = "";
                        document.getElementById("add_btn" + subItemId).style.backgroundColor = "#ffffff";
                        var lPrint = "<span id='addbtn'><span style='cursor:pointer;'><a href='javascript:deuctItem(" + subItemId + ")' class=' waves-effect waves-teal'  ><i class='material-icons left' id='remove'  >remove</i></a></span><span id='quanValueImg" + subItemId + "'>1</span><a href='javascript:addItemCount(" + subItemId + ")' class='  waves-effect waves-teal'  ><i class='material-icons right'>add</i></a></span>";
                        $('#add_btnImg' + subItemId).append(lPrint);
                        lPrint = "<span id='addbtn'><span style='cursor:pointer;'><a href='javascript:deuctItem(" + subItemId + ")' class='waves-effect waves-teal' ><i class='material-icons left' id='remove'  >remove</i></a></span><span id='quanValue" + subItemId + "'>1</span><a href='javascript:addItemCount(" + subItemId + ")' class='waves-effect waves-teal' ><i class='material-icons right'>add</i></a></span>";
                        $('#add_btn' + subItemId).append(lPrint);
                        var database1, idb_request1;
                        idb_request1 = window.indexedDB.open("cartShopInfo", 1);
                        idb_request1.addEventListener("success", function(event) {
                            database1 = this.result;
                            var storage1 = database1.transaction("data1", "readwrite").objectStore("data1");
                            var index1 = storage1.index("by_shopId");
                            var request1 = index1.openCursor(IDBKeyRange.only(shopId));
                            request1.onsuccess = function() {
                                var cursor1 = request1.result;
                                if (cursor1) {
                                    var xItemCount = cursor1.value.itemCount;
                                    var itemList = cursor1.value.itemList;
                                    if (xItemCount > 0) {
                                        xItemCount = xItemCount + 1;
                                        itemList = itemList + "," + subItemId;
                                    } else {
                                        xItemCount = xItemCount + 1;
                                        itemList = subItemId;
                                    }
                                    storage1.put({
                                        shopId: shopId,
                                        itemCount: xItemCount,
                                        itemList: itemList
                                    });
                                } else {
                                    storage1.add({
                                        shopId: shopId,
                                        itemCount: 1,
                                        itemList: subItemId
                                    });
                                }
                            }
                        });
                        $('#addOnInfoChoose').modal('close');
                    } else {
                        var subsTemp = new Array();
                        subsTemp = subsInfo.split(",");
                        document.getElementById("selectingSubscriptionId").innerHTML = "";
                        var lPrint = "<option value='' disabled selected>Choose option</option>";
                        $('#selectingSubscriptionId').append(lPrint);
                        for (i = 0; i < subsTemp.length; i++) {
                            var subsData = JSON.parse(localStorage.getItem("subsInfo")).data;
                            $.each(subsData, function(p, v) {
                                if ((v.subscriptionId == subsTemp[i]) && (v.isActive == 1)) {
                                    var lPrint = "<option value=" + v.subscriptionId + ">" + v.noOfDays + " Days @  &#8377 " + v.suscriptionPrice + "Rs</option>"
                                    $('#selectingSubscriptionId').append(lPrint);
                                }
                            });
                        }
                        $('select').material_select();
                        $('#addOnInfoChoose').modal('close');
                        $('#subscriptionChoose').modal('open');
                    }

                }
            }

        });

    }
function subsModalClose(){
    $('#subscriptionChoose').modal('close');
}

function displayPhoneDesktop(shopPhoneNumber) {
  var $toastContent = $('<span>Call shop at '+shopPhoneNumber+'</span>').add($('<button class="btn-flat toast-action" onclick="doneCall()">Done</button>'));
  Materialize.toast($toastContent);
}

function doneCall() {
  Materialize.Toast.removeAll();
}
