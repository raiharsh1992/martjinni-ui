$(document).ready(function() {
    localStorage.removeItem('shopCartItemDetails');
    localStorage.removeItem('shopListItem');
    var database, idb_request;
    idb_request = window.indexedDB.open("cartShopInfo", 1);
    idb_request.addEventListener("success", function(event) {
        database = this.result;
        var storage = database.transaction("data1", "readwrite").objectStore("data1");
        var index = storage.index("by_shopId");
        var request = index.openCursor();
        request.onsuccess = function() {
            var cursor = request.result;
            if (cursor) {
                var itemCount = cursor.value.itemCount;
                if (itemCount > 0) {
                    var shopObj = {};
                    var shopId = cursor.value.shopId;
                    var usingShop = "shopCartVal" + shopId;
                    localStorage.removeItem(usingShop);
                    shopObj['shopId'] = cursor.value.shopId;
                    shopObj['itemList'] = [];
                    if (itemCount > 1) {
                        var itemList = cursor.value.itemList;
                        var temp = new Array();
                        temp = itemList.split(",");
                        console.log(temp);
                        var userJson = 0
                        while (userJson < itemCount) {
                            calculateItem(Number(temp[userJson]), shopId);
                            userJson++;
                        }
                    } else if (itemCount == 1) {
                        var subItemId = Number(cursor.value.itemList);
                        calculateItem(subItemId, shopId);
                    }

                }
                cursor.continue();
            }
        }
    });
});

function merger(subItemId, shopId) {
    var usingShop = "shopCartVal" + shopId;
    if (localStorage.getItem(usingShop)) {
        var myObj = JSON.parse(localStorage.getItem(usingShop));
        var shopCatName = "shopData" + subItemId;
        myObj['itemList'].push(JSON.parse(localStorage.getItem(shopCatName)));
        var jsonObject = JSON.stringify(myObj);
        localStorage.setItem(usingShop, jsonObject);
    } else {
        var myObj = {}
        myObj['shopId'] = Number(shopId);
        myObj['itemList'] = [];
        var shopCatName = "shopData" + subItemId;
        myObj['itemList'].push(JSON.parse(localStorage.getItem(shopCatName)));
        var jsonObject = JSON.stringify(myObj);
        localStorage.setItem(usingShop, jsonObject);
        if (localStorage.getItem("shopListItem")) {
            var shopListItem = localStorage.getItem("shopListItem") + "," + shopId;
            localStorage.setItem("shopListItem", shopListItem);
        } else {
            localStorage.setItem("shopListItem", shopId);
        }

    }
}

function calculateItem(subItemId, shopId) {
    var itemObj = {};
    var database2, idb_request2;
    idb_request2 = window.indexedDB.open("itemDesc", 1);
    idb_request2.addEventListener("success", function(event) {
        database2 = this.result;
        var storage2 = database2.transaction("data1", "readwrite").objectStore("data1");
        var index2 = storage2.index("by_subItemId");
        var request2 = index2.openCursor(IDBKeyRange.only(subItemId));
        request2.onsuccess = function() {
            var cursor2 = request2.result;
            if (cursor2) {
                itemObj['quantId'] = cursor2.value.quantId;
                itemObj['subsInfo'] = cursor2.value.subsInfo;
                if (cursor2.value.addOnInfo == "") {
                    itemObj['addOnInfo'] = cursor2.value.addOnInfo;
                } else {
                    var addOnData = JSON.parse(JSON.stringify(cursor2.value.addOnInfo)).addOnInfo;
                    itemObj['addOnInfo'] = addOnData;
                }
                var database1, idb_request1;
                idb_request1 = window.indexedDB.open("cartInfo", 1);
                idb_request1.addEventListener("success", function(event) {
                    database1 = this.result;
                    var storage1 = database1.transaction("data1", "readwrite").objectStore("data1");
                    var index1 = storage1.index("by_subItemId");
                    var request1 = index1.openCursor(IDBKeyRange.only(subItemId));
                    request1.onsuccess = function() {
                        var cursor1 = request1.result;
                        if (cursor1) {
                            itemObj['units'] = cursor1.value.quantity;
                            itemObj['itemId'] = subItemId;
                            var shopCatName = "shopData" + subItemId;
                            localStorage.setItem(shopCatName, JSON.stringify(itemObj));
                            if (localStorage.getItem('shopCartItemDetails')) {
                                var shopCartDetails = localStorage.getItem('shopCartItemDetails') + "," + subItemId;
                                localStorage.setItem('shopCartItemDetails', shopCartDetails);
                            } else {
                                localStorage.setItem('shopCartItemDetails', subItemId);

                            }
                            merger(subItemId, shopId);
                        }
                    }
                });
            }
        }
    });
}

function createFinalOrder() {
    if ((localStorage.getItem("totalBilledAmount")) && (localStorage.getItem("createdOrderId")) && (localStorage.getItem('sessionInfo'))) {
        var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
        var sessionInfox = localStorage.getItem("sessionInfo");
        var dataObj = {};
        var orderId = Number(localStorage.getItem("createdOrderId"));
        dataObj['orderId'] = Number(localStorage.getItem("createdOrderId"));
        dataObj['amount'] = Number(localStorage.getItem("totalBilledAmount"));
        dataObj['userId'] = sessionInfo.userId;
        dataObj['typeId'] = sessionInfo.typeId;
        dataObj['userType'] = sessionInfo.userType;
        var head = sessionInfo.basicAuthenticate;
        var jsonObj = JSON.stringify(dataObj);
        $.ajax({
            url: beUrl() + '/payfororder',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            data: jsonObj,
            headers: {
                "basicauthenticate": "" + head
            },
            success: function(response) {
                var delVal = localStorage.getItem('delVal');
                var chosenAddId = localStorage.getItem('chosenAddId');
                localStorage.clear();
                localStorage.setItem('sessionInfo', sessionInfox);
                localStorage.setItem('chosenAddId', chosenAddId);
                localStorage.setItem('delVal', delVal);
                var DBDeleteRequest = window.indexedDB.deleteDatabase("cartInfo");
                DBDeleteRequest.onerror = function(event) {};
                DBDeleteRequest.onsuccess = function(event) {};
                var DBDeleteRequest = window.indexedDB.deleteDatabase("cartShopInfo");
                DBDeleteRequest.onerror = function(event) {};
                DBDeleteRequest.onsuccess = function(event) {};
                var DBDeleteRequest = window.indexedDB.deleteDatabase("itemDesc");
                DBDeleteRequest.onerror = function(event) {};
                DBDeleteRequest.onsuccess = function(event) {};
                window.location.replace(feUrl() + "/orderdetails.html?orderId=" + orderId);
            }
        });
    } else {
        window.location.reload();
    }
}
/*Prints all the stuff on the page*/
function showFinalPage() {
    if (localStorage.getItem('chosenAddId')) {
        var addId = localStorage.getItem('chosenAddId');
        var aAddId = 'a' + addId;
        if (sessionStorage.getItem(aAddId)) {
            var addressInfo = JSON.parse(sessionStorage.getItem(aAddId));
            var addName = addressInfo.addName;
            var addValue = addressInfo.addressLine1 + " " + addressInfo.addressLine2 + " " + addressInfo.city + " " + addressInfo.state + " " + addressInfo.country + " " + addressInfo.pincode;
            /*Prints the address*/
            var xPrint = "<h5>" + addName + "</h5></label><p>" + addValue + "</p>"
            $('#addressValues').append(xPrint);
            var shopListItem = localStorage.getItem("shopListItem");
            var array1 = new Array();
            array1 = shopListItem.split(",");
            var userJson = 0
            var finalOrderObject = {};
            finalOrderObject['shopItemList'] = []
            setTimeout(function() {
                while (userJson < array1.length) {
                    var shopNmae = "shopCartVal" + array1[userJson];
                    console.log(localStorage.getItem(shopNmae));
                    finalOrderObject['shopItemList'].push(JSON.parse(localStorage.getItem(shopNmae)));
                    userJson++;
                }
                var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
                finalOrderObject['typeId'] = sessionInfo.typeId;
                finalOrderObject['userId'] = sessionInfo.userId;
                finalOrderObject['custAddId'] = localStorage.getItem('chosenAddId');
                var jsonFinalOrderObject = JSON.stringify(finalOrderObject);
                console.log(jsonFinalOrderObject);
                var head = sessionInfo.basicAuthenticate;
                $.ajax({
                    url: beUrl() + '/createorder',
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: 'application/json',
                    data: jsonFinalOrderObject,
                    headers: {
                        "basicauthenticate": "" + head
                    },
                    success: function(response) {
                        var shopData = JSON.stringify(response);
                        var shopInfo = JSON.parse(shopData);
                        var masterOrderId = shopInfo.orderId;
                        var successInfo = shopInfo.successShop;
                        var totalBillPrint = "<p id=totalBillId>Total Bill : &#8377; " + shopInfo.totalBilledAmount + "</p><p>Total GST Paid : \u20B9 " + shopInfo.totalGstPaid + "</p>";
                        $("#totalSumValue").append(totalBillPrint);
                        console.log(shopInfo);
                        $.each(successInfo, function(i, v) {
                            var sShopId = "s" + v.shopId;
                            var xPrint = " <div class=card style='border:2px solid #ffa751; border-radius:5%;'> <div class=card-content><span class=card-title>" + v.shopName + "</span> <p>Delivery Charge : \u20B9 " + v.deliveryCharges + " <br>Total GST : \u20B9 " + v.shopGst + " <br> <p id='totalBill" + v.shopId + "'>Total Bill Amount : \u20B9" + v.shopTotal + " <ul class='collapsible' data-collapsible='accordion'> <li> <div class='collapsible-header'><i class='material-icons'>format_line_spacing</i>Order Details</div> <div class='collapsible-body'><span id=" + sShopId + "></span></div> </li> </ul> <div class='card-action' style='text-align:right;color:white;'><a href=/items.html?shopId=" + v.shopId + " class='btn' style='color:white;'>Edit Cart Items</a></div> <div class='row'> <div class='col s7 m5 l4'> <p style='font-style:0.8em; color:#494949; '>Have a promocode? </p> <input type='text' id='" + v.shopId + "promoValue' placeholder='Enter PromoCode' style='border:1px solid black;'> </div><div class='col s2 m1'><a id='applyBtn"+v.shopId+"' style=' vertical-align: middle; margin-top:2em; background-color:#fff; color:black; border:1px solid #ffa751;' class='btn' onclick='applyPromo(" + masterOrderId + ", " + v.orderStoreId + "," + v.shopId + ")'>Apply</a></div></div> </div></div>";
                            $('#itemDetails').append(xPrint);
                            $('.collapsible').collapsible();
                            $.each(v.successItem, function(o, p) {
                                if (p.subsDays == null) {
                                    var jPrint = "<p>" + p.itemName + "<br>" + p.addOnName + "<br>Total Item bill  : \u20B9 " + p.itemCost + "<br>Total Gst Paid : \u20B9 " + p.itemGst + "</p><br><hr>";
                                } else {
                                    var jPrint = "<p>" + p.itemName + "<br>" + p.addOnName + "<br>Total Item bill \u20B9 :" + p.itemCost + "<br>Total Gst Paid:" + p.itemGst + "<br>Subscription Info:<br>Starting On:" + p.subsStartDate + "<br>Total Days ₹ : " + p.subsDays + " </p><br><hr>";
                                }
                                $('#' + sShopId).append(jPrint);
                            });
                        });
                        localStorage.setItem('totalBilledAmount', shopInfo.totalBilledAmount);
                        localStorage.setItem('createdOrderId', shopInfo.orderId);
                    }
                });
            }, 500);

        } else {
            var sessionInfo = localStorage.getItem("sessionInfo");
            var sessionDummy = JSON.parse(sessionInfo);
            var myObj = {};
            myObj['userId'] = sessionDummy.userId;
            var data = JSON.stringify(myObj);
            var head = sessionDummy.basicAuthenticate;
            $.ajax({
                url: beUrl() + '/custaddresslist',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                headers: {
                    "basicauthenticate": "" + head
                },
                data: data,
                success: function(response) {
                    var pocket = localStorage.getItem("pocket");
                    pocket = JSON.parse(pocket);
                    var poc = pocket.pocketName;
                    var pocketName = JSON.stringify(pocket);
                    var address = JSON.stringify(response);
                    var addressUse = JSON.parse(address);
                    var address = addressUse.address;
                    if (addressUse.addressCount > 0) {
                        $.each(address, function(i, v) {
                            var storeId = "a" + v.addressId;
                            var delStoreValue = JSON.stringify(v);
                            sessionStorage.setItem(storeId, delStoreValue)
                        });
                    }
                }
            });
            location.reload();
        }
    } else {
        location.reload();
    }
}
/*
function displayTotalSum() {

}

function display(subItemId,shopId){

}

function calculateTotal(subItemId,shopId) {

}
*/
function proceedWithOrderCreation() {
    $('#addressModal').modal('close');
    if ((localStorage.getItem('newAddress')) || document.getElementById('newAddress')) {
        if (localStorage.getItem('cartCount')) {
            $('#newAddressModal').modal('open');
        } else {
            window.location.replace(feUrl() + "/shops.html");
        }
    } else {
        if ((localStorage.getItem('delVal'))) {
            var delVal =JSON.parse(localStorage.getItem('delVal'));
            var shopList = localStorage.getItem('shopList');
            var cartStoreId = localStorage.getItem('cartStoreId');
            var myObj ={};
            myObj['lat'] = delVal.lattitude;
            myObj['lng'] = delVal.longitude;
            myObj['shopList'] = shopList.split(',').map(Number);
            console.log(myObj);
            $.ajax({
                url: beUrl() + '/validatedelivery',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                data: JSON.stringify(myObj),
                success: function(response) {
                    showFinalPage();
                    console.log(response);
                },
                error: function(response){
                    console.log(response);
                    localStorage.removeItem('chosenAddId');
                    Materialize.toast('Selected address not deliverable by store, Please update', 4000);
                            setTimeout(function() {
                                $('#addressModal').modal('open');
                                console.log('here1');
                            }, 500);
                }
            })
        } else {
            if (localStorage.getItem('cartStoreId')) {
                $('#newAddressModal').modal('open');
            } else {
                window.location.replace(feUrl() + "/shops.html");
            }
        }
    }
}

function dec2hex(dec) {
    return ('0' + dec.toString(16)).substr(-2)
}

// generateId :: Integer -> String
function generateId(len) {
    var arr = new Uint8Array((len || 40) / 2)
    window.crypto.getRandomValues(arr)
    return Array.from(arr, dec2hex).join('')
}

function applyPromo(masterId, storeId, shopId) {
    var promo = document.getElementById(shopId + "promoValue");
    var promoValue = document.getElementById(shopId + "promoValue").value;
    var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
    var myObj = {};
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userId'] = sessionInfo.userId;
    myObj['userType'] = sessionInfo.userType;
    myObj['promoCode'] = promoValue;
    myObj['orderMasterId'] = masterId;
    myObj['orderStoreId'] = storeId;

    var data = JSON.stringify(myObj);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({
        url: beUrl() + '/applypromo',
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: 'application/json',
        headers: {
            "basicauthenticate": "" + head
        },
        data: data,
        success: function(response) {
            var res = JSON.stringify(response)
            var r = JSON.parse(res);
            var newStoreAmount = r.newStoreAmount;
            var oldStoreAmount = r.oldStoreAmount;
            var newTotal = r.newTotal;
            var originTotal = r.originTotal;
            var printx = "Total Bill : &#8377; " + newTotal + " <del>  &#8377;" + originTotal + "</del><p>";
            console.log("TRue part being executed");
            $("#totalBillId").html(printx);
            document.getElementById(shopId + "promoValue").disabled = true;
            $("#applyBtn"+shopId).removeClass('red btn');
            $("#applyBtn"+shopId).addClass('btn-floating green');
            printx = '<i class="material-icons" style="color:white; font-size:2em;">done_outline</i>';
            $("#applyBtn"+shopId).html(printx);
            printx = "Total Bill : &#8377; " + newStoreAmount + " <del>  &#8377;" + oldStoreAmount + "</del><p>";
            $("#totalBill"+shopId).html(printx);

        },
        error: function(response) {
            console.log("Not called");
            console.log("False part being executed");
        }
    })
}
