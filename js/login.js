$(document).ready(function(){
  $('.modal').modal({
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5,
  })
  $('#loginOnly').modal('open');
  $("#login_form").submit(function(event){
    event.preventDefault();
    $("#loginButton").click();
  })
$("#loginButton").click(function(){
  event.preventDefault();
  var userName = document.getElementById('username').value;
  var password = document.getElementById('password').value;
  var loginObj = {};
  loginObj['userName']=userName;
  loginObj['password']=password;
  loginObj['mode']="client";
  var loginData = JSON.stringify(loginObj);
  $.ajax({
    url : beUrl()+'/login',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:loginData,
    success: function(response) {
      var sessionInfo = JSON.stringify(response);
      localStorage.setItem('sessionInfo',sessionInfo);
      Materialize.toast('Successfully logged in', 4000);
      setTimeout(function(){
        if(localStorage.getItem('cartStoreId')){
          var shopId = localStorage.getItem('cartStoreId');
          window.location.replace(feUrl()+"/items.html?shopId="+shopId);
        }
        else if(localStorage.getItem('delVal')){
          window.location.replace(feUrl()+"/shops.html");
        }
        else{
          window.location.replace(feUrl()+"/index.html");
        }
      }, 1000);
    },
    error: function (response) {
      var responseJson = JSON.stringify(response);
      var responseJsonR = JSON.parse(responseJson);
      var statusCode = responseJsonR.responseJSON;
      var data = JSON.stringify(statusCode);
      var xData = JSON.parse(data);
      var message = xData.Data;
      Materialize.toast(message, 4000);
    }
  });
});
})
