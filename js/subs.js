$(document).ready(function() {
    if (window.localStorage) {
        if (localStorage.getItem("sessionInfo")) {
            var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
            var myObj = {};
            myObj['userId'] = sessionInfo.userId;
            myObj['userType'] = sessionInfo.userType;
            myObj['typeId'] = sessionInfo.typeId;
            var data = JSON.stringify(myObj);
            var head = sessionInfo.basicAuthenticate;
            $.ajax({
                url: beUrl() + '/viesublist',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                headers: {
                    "basicauthenticate": "" + head
                },
                data: data,
                success: function(response) {
                    var jsonData = JSON.stringify(response);
                    var dataUse = JSON.parse(jsonData);
                    var data = dataUse.Data;
                    $.each(data, function(i, v) {
                        var delShp = 0;
                        var ts;
                        var sl;
                        var delShp;
                        var prgbar;

                        var intransit = "images/intransit.png";
                        var delivered = "images/delivered.png";
                        if(v.itemStatus=="NEW"){
                          delShp = 0;
                        }
                        else if(v.itemStatus=="WORKING"){
                           ts = v.subsDays;
                           sl = v.subsDaysLeft;
                           intransit = "images/intransitcolor.png";
                           delShp = ts - sl;
                           prgbar = 100;
                          if (sl != 0) {
                              prgbar = ((ts - sl) / ts) * 100;
                          }
                        }
                        else{
                            prgbar = 100;
                            delShp = v.shopCount;
                            intransit = "images/intransitcolor.png";
                            delivered = "images/deliveredcolor.png";
                        }
                        var printd = '<div class="container"><div class="row z-depth-2" style="padding: 1em;"><div class="col s12"><p style="font-size:0.8em; color:#282828;">Item info:' + v.itemName + '<span style="float:right">Created On: ' + v.subsStartDate + '</span></p><div class="row"><div class="col s6" style="text-align:center; padding-left: 1em; margin: 0;"><div class="row" style="vertical-align:middle; width: 100%; padding-bottom:0px; position:relative;"><div style="padding:0px; vertical-align:middle; width: 100%; position: absolute;"><img src="images/createordercolor.png" style="width: 24px; z-index: 99;  height: 24px; float: left "/></div><div style="padding:0px; vertical-align:middle; width: 100%; position: absolute;"><span class="progress" style="z-index: -1;"><span class="determinate" style="width: 120%; background-color:#ffa751;"></span></span></div><div style="padding:0px; vertical-align:middle; float:right; clear:right; width: 100%; position: absolute;"><img src="' + intransit + '"id="intransit" style="width: 24px; z-index: 99;  height: 24px; float: right"/></div></div><div style="color:green; padding:0px;">' + v.subsDays + '</div><div style="color:green; padding:0px;">Working</div></div><div class="col s6" style="text-align:center; padding: 0; margin: 0;"><div class="row" style="vertical-align:middle; width: 100%; padding-bottom:0px; position:relative;"><div style="padding:0px; vertical-align:middle; width: 100%; position: absolute;"><span class="progress"><span class="determinate" style="width: ' + prgbar + '%;  background-color:#ffa751;"></span></span></div><div  style="padding:0px; vertical-align:middle; width: 100%; position: absolute;"><img src="' + delivered + '" id="deliverred" style="width: 24px; z-index: 99;  height: 24px; float: right"/></div></div><div style="color:green; padding:0px;">' + delShp + '</div><div style="color:green; padding:0px;">Delivered</div></div></div><p style="font-weight:900;">Total Amount : &#8377; ' + v.cost + '</p><p style="font-weight:900;"><span style="float:right"><a href=subsdetails.html?subsId=' + v.subscriptIonId + '>View Details</a></span></p></div></div></div>';
                        $('#test3').append(printd);
                    });
                },
                error: function(response) {
                    Materialize.toast('Not authorized to view the page', 4000)
                    setTimeout(function() {
                        window.location.replace(feUrl() + "/index.html");
                    }, 500);
                }
            });
        } else {
            Materialize.toast('Not authorized to view the page', 4000)
            setTimeout(function() {
                window.location.replace(feUrl() + "/index.html");
            }, 500);
        }
    }
});
