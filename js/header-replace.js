$(document).ready(function() {
  $("#menuSideNav").sideNav({
  closeOnClick: true,
    draggable: true
})
    if (window.localStorage) {
        if (localStorage.getItem("sessionInfo")) {
            var log = '<li><a href="orders.html">Orders<i class="material-icons left">shopping_basket</i></a></li><li><a href="subs.html">Subscription<i class="material-icons left">access_alarms</i></a></li><li><a href="javascript:redirectHome()"><i class="material-icons left">edit_location</i>Change Address</a></li> <li><a href="restore.html"><i class="material-icons left">account_circle</i>Change Password</a></li><li><a href="logout.html"><i class="material-icons left">face</i>Logout</a></li><li><a href="contact.html"><i class="material-icons">perm_contact_calendar</i>Contact Us</a></li>';
        } else {
            var log = '<li><a href="javascript:redirectHome()"><i class="material-icons left">edit_location</i>Change Address</a></li><li><a href="login.html"><i class="material-icons left">face</i>Login</a></li><li><a href="signup.html"><i class="material-icons left">account_circle</i>SignUp</a></li><li><a href="contact.html"><i class="material-icons">perm_contact_calendar</i>Contact Us</a></li>';
        }
        $('#profileContent').append(log);
        var shop = '<li><a href="shops.html"><i class="material-icons left">store</i>Shops</a></li>';
        xlog = shop + "<li><a href=/about.html><i class='material-icons left'>perm_device_information</i>About</a></li>";
        var cartC = localStorage.getItem("cartCount");
        if ((localStorage.getItem("cartCount")) >= 1) {
            var cart = ' <li><a href="cart.html"><i class="material-icons left">add_shopping_cart</i>Cart<span class="badge" data-badge-caption="items" id="cartInfor">' + cartC + '</span> </a></li>';
            $('#test').append(cart);
            var cart = ' <li><a href="cart.html"><i class="material-icons left">add_shopping_cart</i>Cart<span class="badge" data-badge-caption="items" id="cartInforS">' + cartC + '</span> </a></li>';
            $('#test2').append(cart);
        }
        log = xlog + "" + log;
        xlog = xlog + '<li><a class="dropdown-button" id="hello2" href="#!" data-activates="profileContent"><i class="material-icons right">person_outline</i></a></li>';
        $('#test').append(xlog);
        $('#test2').append(log);
        $("#hello2").dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: false, // Displays dropdown below the button
            alignment: 'left', // Displays dropdown with edge aligned to the left of button
            stopPropagation: false // Stops event propagation
        });
    }
    $('head').append('<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Courgette|Pattaya|Chela+One|Passion+One|Arima+Madurai|Josefin+Slab:700|Mogra|Limelight|Rye"rel="stylesheet">');
    $('head').append('<link rel="shortcut icon" href="images/mjsq.png">');
    $('head').append("<!-- Global site tag (gtag.js) - Google Analytics --><script async src='https://www.googletagmanager.com/gtag/js?id=UA-114684493-1'></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-114684493-1');</script>");
});

function redirectHome() {
    var $toastContent = $('<span>Proceeding will clear cart</span>').add($('<button class="btn-flat toast-action" onclick="gotHome()">Proceed</button>'));
    Materialize.toast($toastContent);
}

function gotHome() {
    window.location.replace(feUrl() + "/index.html");
}

function updateCartValue() {
    if (document.getElementById('cartInfor')) {
        document.getElementById('cartInfor').innerHTML = "";
        document.getElementById('cartInforS').innerHTML = "";
        var cartCount = localStorage.getItem("cartCount");
        if (cartCount > 0) {
            $('#cartInfor').append(cartCount);
            $('#cartInforS').append(cartCount);
        } else {
            $('#cartInfor').append(0);
            $('#cartInforS').append(0);
        }
    } else {
        var cartC = localStorage.getItem("cartCount");
        var cart = ' <a href="cart.html"><i class="material-icons left">add_shopping_cart</i>Cart<span class="badge" data-badge-caption="items" id="cartInforS">' + cartC + '</span> </a>';
        $('#mobcart').append(cart);
        console.log('hello');
        var cart = ' <li><a href="cart.html"><i class="material-icons left">add_shopping_cart</i>Cart<span class="badge" data-badge-caption="items" id="cartInfor">' + cartC + '</span> </a></li>';
        $('#test').append(cart);
        var cart = ' <li><a href="cart.html"><i class="material-icons left">add_shopping_cart</i>Cart<span class="badge" data-badge-caption="items" id="cartInforS">' + cartC + '</span> </a></li>';
        $('#test2').append(cart);

    }
}
