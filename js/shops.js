var noImage = feUrl() + "/images/noimage.gif";
var arr = new Array();
var favlist;
$(document).ready(function() {
    localStorage.removeItem('catini');
    localStorage.removeItem('catrem');
    localStorage.removeItem('favini');
    localStorage.removeItem('favrem');
    localStorage.removeItem("currentActiveFilter");
    $('.collapsible').collapsible({
        accordion: true,
    });
    if (localStorage.getItem("sessionInfo")) {
        var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
        var typeId = sessionInfo.typeId;
        var userId = sessionInfo.userId;
        var userType = sessionInfo.userType;
        var head = sessionInfo.basicAuthenticate;
        var myObj = {};
        myObj['typeId'] = typeId;
        myObj['userId'] = userId;
        myObj['userType'] = userType;
        var data = JSON.stringify(myObj);
        $.ajax({
            url: beUrl() + '/viewfav',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            headers: {
                "basicauthenticate": "" + head
            },
            data: data,
            success: function(response) {
                var shopData = JSON.stringify(response);
                var shopInfo = JSON.parse(shopData);
                favlist = shopInfo.favList;
                arr = favlist.split(",");
                localStorage.setItem('favlist', arr);
            },
            error: function(response) {
                var shopData = JSON.stringify(response);
                var shopInfo = JSON.parse(shopData);
                var Data2 = shopInfo.Data;
                Materialize.toast('Something went wrong', 4000);
            }
        });
    }


    $('select').material_select();

    $('.modal').modal();

    if (window.localStorage) {
        if ((localStorage.getItem("delVal")) && (localStorage.getItem("searchLocal"))) {
            var searchValue = localStorage.getItem("searchLocal");
            $("#shopTitleName").append(localStorage.getItem("searchLocal"));
            var url = feUrl() + '/shoplist';
            var delInfo = JSON.parse(localStorage.getItem("delVal"));
            var myObj = {};
            myObj['lattitude'] = delInfo.lattitude;
            myObj['longitude'] = delInfo.longitude;
            var data = JSON.stringify(myObj);
            $.ajax({
                url: beUrl() + '/shoplist',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                data: data,
                success: function(response) {
                    var data = JSON.stringify(response);
                    var jsonData = JSON.parse(data);
                    var tabs = jsonData.tabs;
                    var mainTab = '<ul class="tabs">';
                    var tabData = '';
                    var g = 0;
                    $.each(tabs, function(i, v) {
                        if (g == 0) {
                            var ftab = JSON.stringify(v);
                            localStorage.setItem("fTab", v);
                            localStorage.setItem("currentActiveTab", v);
                            g++;
                        }

                        var k = '<li class="tab col s3"><a href="#' + v + '">' + v + '</a></li>';
                        mainTab = mainTab + k;
                        var m = '<div id="' + v + '" class="col s12"><div class="row"><div class="col s0 m3 l3 xl3"><div class="row" style="position:fixed; width:23%;"><div class="fixed"><div class="hide-on-med-and-down" style="width:100%; " id="filter' + v + 'd"><div class="card grey z-depth-5" id="headul" style="min-height:20em; max-height:35em; color:orange"><div class="card-content white-text" style=" overflow:auto;"><span class="card-title" style="text-align:center">Choose Category</span><ul id="filterDropdown' + v + 'd"></ul></div></div></div></div></div></div><div class="col s12 m9 l9 xl9"><div id="' + v + 'Info"></div><ul id="filterDropdown' + v + 'm" class="side-nav"></ul><div class="row"><div class="navbar1"><div class="col s7"><a class="button-collapse" id="collap' + v + '" href="#" data-activates="filterDropdown' + v + 'm" ><i class="material-icons left md-25">filter_list</i><span style="font-family:Pattaya, sans-serif; font-size:1em">Categories</a></div><div class="col s5"><span class="mobcart" style="margin-left:-0.5"></span></div></div></div></div></div></div>';
                        tabData = tabData + m;

                    })
                    mainTab = mainTab + "</ul>";
                    $("#mainTab").append(mainTab);
                    $("#tabData").append(tabData);



                    var cartC = localStorage.getItem("cartCount");
                    if (cartC == 0 || cartC == null) {
                        var cart = ' <a href="cart.html" style="margin-left:-1em"><i class="material-icons left">add_shopping_cart</i><span class="new badge red" data-badge-caption="" id="cartInforS" style="z-index:-2"> 0 </span> </a>';
                    } else {
                        var cart = ' <a href="cart.html" style="margin-left:-1em"><i class="material-icons left">add_shopping_cart</i><span class="new badge red" data-badge-caption="" id="cartInforS" style="z-index:-2; margin-right:-1em;">' + cartC + '</span> </a>';
                    }
                    $('.mobcart').append(cart);

                    $.each(jsonData, function(i, v) {
                        if (i != "tabs") { /*Condition if any shop is open or closed*/
                            if (v.count > 0) {
                                var open = v.shopList.open;
                                var close = v.shopList.close;
                                var allInitial = JSON.stringify(open.concat(close));
                                var name = i + "ini";
                                var name2 = i + "rem";
                                localStorage.setItem(name, allInitial);
                                localStorage.setItem(name2, "[]")
                                var mjAvailable = v.filterData.mjAvailable;
                                $.each(mjAvailable, function(k, z) {
                                    var printx = '<ul class="collapsible" data-collapsible="accordion" style="padding:3px;"><li style="padding:3px;"><div class="collapsible-header filterView" style="color:black; border:1px solid black; font-size:1.35em;  font-weight:900;">' + z.mjName + '</div><div class="collapsible-body" style="background-color:white"><ul id=mjF' + z.mjId + '' + i + '></ul></div></li></ul>';

                                    $("#filterDropdown" + i + "m").append(printx);
                                    $('.collapsible').collapsible();

                                    var printx = '<ul class="collapsible" data-collapsible="accordion"><li><div class="collapsible-header" style="color:#262626; display:block; width:100%; font-family:Arima Ma.durai; font-family:Arima Madurai; font-weight:900;">' + z.mjName + '</div><div class="collapsible-body" style="background-color:white"><ul id=mjK' + z + '></ul></div></li></ul>';
                                    $("#filterDropdown" + i + "d").append(printx);
                                    $('.collapsible').collapsible();
                                    $('#collap' + i).sideNav({
                                        closeOnClick: true,
                                    });
                                });
                                //This Prints the View Fav button
                                var arrStr = arr;
                                var printy = "<span class='view_fav_class' style='text-align:center; display:block;  padding: 3px; border:1px groove red; '><img src='images/hf.png' style='height:1em; width:auto;'><a href=javascript:displayFilter('" + i + "','fav'," + arrStr + ", 10) style='padding:5px; cursor:pointer; color:black;'>View Favs</a></span><p onclick='location.reload();' class='view_fav_class' style='text-align:center; display:block;  padding: 3px; border:1px groove red; '>Clear Filter</p>"
                                $("#filterDropdown" + i + "d").append(printy);
                                $("#filterDropdown" + i + "m").append(printy);
                                var catCount = 0;
                                var catAval = v.filterData.catAvailable;
                                $.each(catAval, function(o, p) {
                                    var catMj = p.catMJ;
                                    var catName = p.catName;
                                    var catId = p.catId;
                                    var shopList = JSON.stringify(p.shopList);
                                    var catUserName = 'cat' + catId;
                                    if (!document.getElementById(catUserName)) {
                                        catCount++;
                                        var catInfo = JSON.stringify(v)
                                        var catp = catId + "cat";
                                        var printx = '<li id="cat' + catId + '" style="word-wrap: break-word;"><a href=javascript:displayFilter("' + i + '","cat",' + shopList + ',10) style="color:black; width:100%; word-wrap: break-word; ">' + catName + '</a></li>';
                                        var idMain = "#mj" + catMj;
                                        $("#mjF" + catMj + "").append(printx);
                                        $("#mjK" + catMj + "").append(printx);
                                        $('.collapsible').collapsible();
                                    }
                                });
                                //End of the filetr
                            } else {
                                console.log('0 open ' + i);
                            }

                        }
                    })
                    var fTab = localStorage.getItem('fTab');
                    displayShops(fTab, 11);
                    $('ul.tabs').tabs({
                        onShow: function(tab) {
                            var activeTab = $(".active").attr('href');
                            console.log(activeTab);
                            var shopType = activeTab.substr(1);
                            localStorage.setItem("currentActiveTab", shopType);
                            displayShops(shopType, 10);
                        }
                    });

                }
            });
        } else {
            console.log('Nothing here');
            window.location.replace('index.html');
        }

    }
    if (!localStorage.getItem("sessionInfo")) {
        $(".view_fav_class").hide();
    }
});
$(window).scroll(function() {
    if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
        var shopType = localStorage.getItem("currentActiveTab");
        if (localStorage.getItem("currentActiveFilter")) {
            var fileterName = localStorage.getItem("currentActiveFilter");
            var arrFilA = JSON.parse(localStorage.getItem(fileterName + "rem"));
            displayFilter(shopType, fileterName, arrFilA, 3);
        }
        displayShops(shopType, 3);
    }
});

function displayFilter(shopType, fileterName, arrFil, shopNum) {
    localStorage.setItem("currentActiveFilter", fileterName);
    $("#" + shopType + "Info").html('');
    var arrFilS = JSON.stringify(arrFil);
    localStorage.setItem(fileterName + "ini", arrFilS);
    localStorage.setItem(fileterName + "rem", '[]');
    var shopini = JSON.parse(localStorage.getItem(fileterName + "ini"));
    var shopsrem = JSON.parse(localStorage.getItem(fileterName + "rem"));
    if (shopini == null) {
        shopini = [];
    }
    if (shopsrem == null) {
        shopsrem = [];
    }
    if (shopini.length > 0) {
        if (shopini.length < shopNum) {
            shopNum = shopini.length;
            console.log("shop num if  in start length < shopNum " + shopNum);
        }

        var d = [];
        for (var i = 0; i < shopNum; ++i) {
            d[i] = shopini[i];
        }
        shopini = $(shopini).not(d).get();
        var finalshoprem = shopsrem.concat(d);
        var shopinistr2 = JSON.stringify(shopini);
        var finalshopremstr2 = JSON.stringify(finalshoprem);
        console.log("new shop rem" + finalshopremstr2);
        localStorage.setItem(fileterName + "ini", shopinistr2);
        console.log("new shop ini" + shopinistr2);
        localStorage.setItem(fileterName + "rem", finalshopremstr2);
        var myObj = {};
        myObj['shops'] = d;
        var data = JSON.stringify(myObj);
        var sessionInfo = '';
        var typeId = '';
        var userId = '';
        var userType = '';
        $.ajax({
            url: beUrl() + '/getshopinfo',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            data: data,
            success: function(response) {
                var r = JSON.stringify(response);
                var res = JSON.parse(r);
                var data = res.Data;
                $.each(data, function(i, v) {
                    if (v.imageUploaded == 1) {
                        imagUrl = imgUrl() + "" + v.shopId + ".jpeg";
                    }
                    else {
                        imagUrl = noImage;
                    }
                    var favImg;
                    var favStatus;
                    if (!localStorage.getItem("sessionInfo")) {
                        favImg = "images/ho.png";
                        favStatus = 2;
                        typeId = 0;
                        userId = 0;
                        userType = 0;
                    } else {
                        console.log("Session Info is here")
                        var shopIdStr = String(v.shopId);
                        var isin = arr.includes(shopIdStr);
                        var isin = arr.includes(shopIdStr);
                        sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
                        typeId = sessionInfo.typeId;
                        userId = sessionInfo.userId;
                        userType = "'" + sessionInfo.userType + "'";
                        if (isin == false) {
                            favImg = "images/ho.png";
                            favStatus = 0;
                        } else if (isin == true) {
                            favImg = "images/hf.png";
                            favStatus = 1;
                        }
                    }
                    var shopStat = 'images/open.png';
                    if (v.isOpen == 0) {
                        shopStat = "images/closed.png";
                    }
                    var print = '<div class="col s12 m6 l6"  onclick="toItem(' + v.shopId + ')" style="padding-left:0px; padding-right:0px" id="shp' + v.shopId + '"class="z-depth-2 hoverable" ><div class="shopCard z-depth-2 hoverable" style="margin-bottom:1em" ><div class="row"><div class="col s6" style="padding:3px;"><div class="shopCard_img" style="background-image:url(' + imagUrl + ')"></div></div><div class="col s6" style="padding:1px;"><div class="shopCard_content"><span class="shopCard_name">' + v.shopName + '</span><span class="shopCard_address">' + v.address + '</span><span class="shopCard_cat">' + v.shopDesc + '</span><span class="shopCard_delChar">Del charge: <span class="shopCard_money">&#8377;' + v.delCharges + '</span><span><img src="' + favImg + '" class="favImgidc" id="favImgid" style="margin-top:-1.2em;  margin-right:1em;  height:1.3em; width:auto; padding:1px; cursor:pointer; vertical-align:center; float:right; display:inline-block;" onclick="toggleFav(' + favStatus + ',' + typeId + ',' + userId + ',' + String(userType) + ',' + v.shopId + ')"></span></span><p class="shopCard_min">Del Free After: <span class="shopCard_money">&#8377;' + v.minOrderValue + '</span></p><img src="' + shopStat + '" class="openClose"></div></div></div></div></div>';
                    var print = '<div class="col s12 m6 l6" style="padding-left:0px; padding-right:0px" id="shp' + v.shopId + '"class="z-depth-2 hoverable" ><div class="shopCard z-depth-2 hoverable" style="margin-bottom:1em"><div class="row"><div class="col s6" style="padding:3px;"><div class="shopCard_img" style="background-image:url(' + imagUrl + ')"></div></div><div class="col s6" style="padding:1px;"><div class="shopCard_content"><span class="shopCard_name">' + v.shopName + '</span><span class="shopCard_address">' + v.address + '</span><span class="shopCard_cat">' + v.shopDesc + '</span><span class="shopCard_delChar">Del charge: <span class="shopCard_money">&#8377;' + v.delCharges + '</span><span><img src="' + favImg + '" class="favImgidc" id="favImgid" style="margin-top:-1.2em;  margin-right:1em;  height:1.3em; width:auto; padding:1px; cursor:pointer; vertical-align:center; float:right; display:inline-block;" onclick="toggleFav(' + favStatus + ',' + typeId + ',' + userId + ',' + String(userType) + ',' + v.shopId + ')"></span></span><p class="shopCard_min">Del Free After: <span class="shopCard_money">&#8377;' + v.minOrderValue + '</span></p><img src="' + shopStat + '" class="openClose"></div></div></div></div></div>';
                    $(".loading_text").hide("slow");
                    $("#" + shopType + "Info").append(print);
                })
            }
        });
    } else {
        console.log("Nothing to Display Now")
    }
}

function displayShops(shopType, shopNum) {
    var shopini = JSON.parse(localStorage.getItem(shopType + "ini"));
    var shopsrem = JSON.parse(localStorage.getItem(shopType + "rem"));
    if (shopini == null) {
        shopini = [];
    }
    if (shopsrem == null) {
        shopsrem = [];
    }
    if (shopini.length > 0) {
        if (shopini.length < shopNum) {
            shopNum = shopini.length;
        }

        var d = [];
        for (var i = 0; i < shopNum; ++i) {
            d[i] = shopini[i];
        }
        shopini = $(shopini).not(d).get();
        var finalshoprem = shopsrem.concat(d);
        var shopinistr2 = JSON.stringify(shopini);
        var finalshopremstr2 = JSON.stringify(finalshoprem);
        localStorage.setItem(shopType + "ini", shopinistr2);
        localStorage.setItem(shopType + "rem", finalshopremstr2);
        var myObj = {};
        myObj['shops'] = d;
        var data = JSON.stringify(myObj);
        var sessionInfo = '';
        var typeId = '';
        var userId = '';
        var userType = '';
        $.ajax({
            url: beUrl() + '/getshopinfo',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            data: data,
            success: function(response) {
                var r = JSON.stringify(response);
                var res = JSON.parse(r);
                var data = res.Data;
                $.each(data, function(i, v) {
                    if (v.imageUploaded == 1) {
                        imagUrl = imgUrl() + "" + v.shopId + ".jpeg";
                    } else {
                        imagUrl = noImage;
                    }
                    var favImg;
                    var favStatus;
                    if (!localStorage.getItem("sessionInfo")) {
                        favImg = "images/ho.png";
                        favStatus = 2;
                        typeId = 0;
                        userId = 0;
                        userType = 0;
                    } else {
                        var shopIdStr = String(v.shopId);
                        var isin = arr.includes(shopIdStr);
                        var isin = arr.includes(shopIdStr);
                        sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
                        typeId = sessionInfo.typeId;
                        userId = sessionInfo.userId;
                        userType = "'" + sessionInfo.userType + "'";
                        if (isin == false) {
                            favImg = "images/ho.png";
                            favStatus = 0;
                        } else if (isin == true) {
                            favImg = "images/hf.png";
                            favStatus = 1;
                        }
                    }
                    var shopStat = 'images/open.png';
                    if (v.isOpen == 0) {
                        shopStat = "images/closed.png";
                    }
                    var print = '<div class="col s12 m6 l6" onclick="toItem(' + v.shopId + ')" style="padding-left:0px; padding-right:0px" id="shp' + v.shopId + '"class="z-depth-2 hoverable"><div class="shopCard z-depth-2 hoverable" style="margin-bottom:1em"><div class="row"><div class="col s6" style="padding:3px;"><div class="shopCard_img" style="background-image:url(' + imagUrl + ')"></div></div><div class="col s6" style="padding:1px;"><div class="shopCard_content"><span class="shopCard_name">' + v.shopName + '</span><span class="shopCard_address">' + v.address + '</span><span class="shopCard_cat">' + v.shopDesc + '</span><span class="shopCard_delChar">Del charge: <span class="shopCard_money">&#8377;' + v.delCharges + '</span><span><img src="' + favImg + '" class="favImgidc" id="favImgid" style="margin-top:-1.2em;  margin-right:1em;  height:1.3em; width:auto; padding:1px; cursor:pointer; vertical-align:center; float:right; display:inline-block;" onclick="toggleFav(' + favStatus + ',' + typeId + ',' + userId + ',' + String(userType) + ',' + v.shopId + ')"></span></span><p class="shopCard_min">Del Free After: <span class="shopCard_money">&#8377;' + v.minOrderValue + '</span></p><img src="' + shopStat + '" class="openClose"></div></div></div></div></div>';
                    $(".loading_textreg").hide("slow");
                    $("#" + shopType + "Info").append(print);
                })
            }
        });
    } else {
        console.log("Nothing to Display Now")
    }
}


function redirect() {
    window.location.replace(feUrl());
}

function viewfavs() {
    $('.carousel').hide("slow").fadeOut("slow");
    $('#hello').sideNav('hide');
    if (localStorage.getItem('favlist')) {
        var favlist = localStorage.getItem('favlist');
        var arr = favlist.split(',');
        if (localStorage.getItem('availableShopList')) {
            var shoplist = localStorage.getItem('availableShopList');
            var temp = new Array();
            temp = shoplist.split(",");
            var x = 0;
            for (i = 0; i < temp.length; i++) {
                var shopId = temp[i];
                var shopIdStr = String(shopId);
                var isin = arr.includes(shopIdStr);
                var shopInfo = JSON.parse(localStorage.getItem(shopId));
                if (shopInfo.isOpen == 1) {
                    if (isin == false) {
                        $("#shp" + shopId).hide("slow").fadeOut("slow");
                    }
                }

            }
        }
    } else {
        Materialize.toast('Kindly add favourite', 10000)
    }
}

function toggleFav(favStatus, typeId, userId, userType, shopId) {
    console.log(favStatus, typeId, userId, userType, shopId);
    if (!localStorage.getItem("sessionInfo")) {
        Materialize.toast('PLease Login to add as Favourite', 4000)
    } else {
        //If fav status ==1 or if shop is marked as favourite we will delete favourite
        if (favStatus == 1) {
            var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
            var head = sessionInfo.basicAuthenticate;
            var myObj = {};
            myObj['typeId'] = typeId;
            myObj['userId'] = userId;
            myObj['userType'] = userType;
            myObj['shopId'] = shopId;
            var data = JSON.stringify(myObj);
            $.ajax({
                url: beUrl() + '/delfav',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                headers: {
                    "basicauthenticate": "" + head
                },
                data: data,
                success: function(response) {
                    var shopData = JSON.stringify(response);
                    var shopInfo = JSON.parse(shopData);
                    console.log(shopInfo)
                    window.location.reload();
                },
                error: function(response) {
                    console.log(response);
                    Materialize.toast('Something went wrong', 4000)
                }


            });
        }
        //if fav status is ==0 mark shop as favourite
        else if (favStatus == 0) {
            var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
            var head = sessionInfo.basicAuthenticate;
            var myObj = {};
            myObj['typeId'] = typeId;
            myObj['userId'] = userId;
            myObj['userType'] = userType;
            myObj['shopId'] = shopId;
            var data = JSON.stringify(myObj);
            $.ajax({
                url: beUrl() + '/addtofav',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                headers: {
                    "basicauthenticate": "" + head
                },
                data: data,
                success: function(response) {
                    var shopData = JSON.stringify(response);
                    var shopInfo = JSON.parse(shopData);
                    console.log(shopInfo)
                    window.location.reload();
                },
                error: function(response) {
                    var cap = JSON.stringify(response);
                    var cap2 = JSON.parse(cap);
                    var res = cap.Data;
                    console.log(res);
                    Materialize.toast('Something went wrong', 4000)
                }
            });
        }
    }
}

function toItem(shopId) {
    console.log("Function to redirect");
    window.location.replace('items.html?shopId=' + shopId);

}
