$(document).ready(function() {
    localStorage.removeItem("newAddress");
    $("#newAddressForm").submit(function(event) {
        event.preventDefault();
        $("#createNewAddress").click();
    });
    $("#createNewAddress").click(function() {
        insertNewAddress();
        })
    $('.modal').modal({
        dismissible: false, // Modal can be dismissed by clicking outside of the modal
        opacity: .5,
    })
    $("#cancel_btn_icon").click(function(){
      window.location.replace('shops.html');
    });
    if (localStorage.getItem("cartCount")) {
        if (localStorage.getItem("sessionInfo")) {
            if (localStorage.getItem("chosenAddId")) {
                addressHandler();
            } else {
                addressHandler();
            }
        } else {
            $('#loginSignup').modal('open');
        }
    } else {
        window.location.replace(feUrl() + "/shops.html")
    }
    $("#signUp1").submit(function(event) {
        event.preventDefault();
        $("#signup1").click();
    });
    $("#signup1").click(function() {
        //$("#signup1").addClass('disabled');
        var userNameId = document.getElementById("signupUsername")
        var userNameClass = userNameId.className;
        var userPassId = document.getElementById("signuppassword");
        var userPassClass = userPassId.className;
        if ((userNameClass == "validate valid") && (userPassClass == "validate valid")) {
            var signUpObj = {}
            signUpObj['userNameLogin'] = userNameId.value;
            signUpObj['password'] = userPassId.value;
            var signUpJson = JSON.stringify(signUpObj);
            sessionStorage.setItem('signUpJson', signUpJson)
            if (sessionStorage.getItem('signUpJson')) {
                Materialize.toast('Almost there', 4000);
                $('#signUpOnly1').modal('close');
                $('#signUpOnly2').modal('open');
            }
        } else {
            Materialize.toast('See the error message', 4000);
        }
    });
    $("#signUp3").submit(function(event) {
        event.preventDefault();
        $("#signup3").click();
    });
    $("#signup3").click(function() {
        event.preventDefault();
        //$("#signup3").addClass('disabled');
        var otpId = document.getElementById('custOTP');
        var otpclass = otpId.className;
        if (otpclass == "validate valid") {
            var signUpObjectFinal = sessionStorage.getItem('signUpJson');
            var jsonParser = JSON.parse(signUpObjectFinal);
            var finalObject = {};
            finalObject['otp'] = otpId.value;
            finalObject['userNameLogin'] = jsonParser.userNameLogin;
            finalObject['password'] = jsonParser.password;
            finalObject['phoneNumber'] = jsonParser.phoneNumber;
            finalObject['custName'] = jsonParser.custName;
            finalObject['emailId'] = jsonParser.emailId;
            finalObject['userType'] = "CUST";
            var finalJsonObject = JSON.stringify(finalObject);
            $.ajax({
                url: beUrl() + '/createuser',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                data: finalJsonObject,
                success: function(response) {
                    Materialize.toast('Please wait for magic', 4000);
                    var sessionInfo = JSON.stringify(response);
                    localStorage.setItem('sessionInfo', sessionInfo);;
                    setTimeout(function() {
                        location.reload();
                    }, 1000);
                },
                error: function(response) {
                    var responseJson = JSON.stringify(response);
                    var responseJsonR = JSON.parse(responseJson);
                    var statusCode = responseJsonR.responseJSON;
                    var data = JSON.stringify(statusCode);
                    var xData = JSON.parse(data);
                    var message = xData.Data;
                    Materialize.toast(message, 4000);
                }
            });
        } else {
            Materialize.toast('Please pass OTP', 4000);
        }
    })
    $("#signUp2").submit(function(event) {
        event.preventDefault();
        $("#signup2").click();
    });
    $("#signup2").click(function() {
        event.preventDefault();
        //$("#signup2").addClass('disabled');
        var custNameId = document.getElementById('signupCustname');
        var custEmailId = document.getElementById('userEmail');
        var custPhoneId = document.getElementById('userPhone');
        var custNameClass = custNameId.className;
        var custEmailClass = custEmailId.className;
        var custPhoneClass = custPhoneId.className;
        if ((custNameClass == "validate valid") && (custEmailClass == "validate valid") && (custPhoneClass == "validate valid")) {
            if ((document.getElementById("check").checked) == true) {
                var loginObject = sessionStorage.getItem('signUpJson');
                var loginPars = JSON.parse(loginObject);
                sessionStorage.removeItem('signUpJson');
                var signUpObject = {};
                signUpObject['userType'] = "CUST";
                signUpObject['userNameLogin'] = loginPars.userNameLogin;
                signUpObject['password'] = loginPars.password;
                signUpObject['phoneNumber'] = custPhoneId.value;
                signUpObject['custName'] = custNameId.value;
                signUpObject['emailId'] = custEmailId.value;
                var jsonSignUpJson = JSON.stringify(signUpObject);
                sessionStorage.setItem('signUpJson', jsonSignUpJson);
                var otp = {};
                otp['phoneNumber'] = custPhoneId.value;
                otp['userType'] = "CUST";
                otp['userNeed'] = "SIGNUP";
                var jsonOtp = JSON.stringify(otp);
                $.ajax({
                    url: beUrl() + '/generateotp',
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: 'application/json',
                    data: jsonOtp,
                    success: function(response) {
                        Materialize.toast('Please see your phone for OTP', 4000);
                        $('#signUpOnly2').modal('close');
                        $('#signUpOnly3').modal('open');
                    },
                    error: function(response) {
                        var responseJson = JSON.stringify(response);
                        var responseJsonR = JSON.parse(responseJson);
                        var statusCode = responseJsonR.responseJSON;
                        var data = JSON.stringify(statusCode);
                        var xData = JSON.parse(data);
                        var message = xData.Data;
                        Materialize.toast(message, 4000);
                    }
                });
            } else {
                Materialize.toast('Kindly accept our T&C', 4000);
            }
        } else {
            Materialize.toast('See the error message', 4000);
        }
    })
    $("#loginbutton").submit(function(event) {
        event.preventDefault();
        $("#loginButton").click();
    });
    $("#loginButton").click(function() {
        event.preventDefault();
        //$("#loginButton").addClass('disabled');
        var userName = document.getElementById('username').value;
        var password = document.getElementById('password').value;
        var loginObj = {};
        loginObj['userName'] = userName;
        loginObj['password'] = password;
        loginObj['mode'] = "client";
        var loginData = JSON.stringify(loginObj);
        $.ajax({
            url: beUrl() + '/login',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            data: loginData,
            success: function(response) {
                var sessionInfo = JSON.stringify(response);
                localStorage.setItem('sessionInfo', sessionInfo);
                Materialize.toast('Successfully logged in', 4000);
                setTimeout(function() {
                    location.reload();
                }, 1000);
            },
            error: function(response) {
                var responseJson = JSON.stringify(response);
                var responseJsonR = JSON.parse(responseJson);
                var statusCode = responseJsonR.responseJSON;
                var data = JSON.stringify(statusCode);
                var xData = JSON.parse(data);
                var message = xData.Data;
                Materialize.toast(message, 4000);
            }
        });
    });
});

function loginModal() {
    $('#loginSignup').modal('close');
    $('#loginOnly').modal('open');
}

function signupModal() {
    $('#loginSignup').modal('close');
    $('#signUpOnly1').modal('open');
}


function userAvailability() {
    var userData = document.getElementById('signupUsername').value;
    myObj = {};
    myObj['userName'] = userData;
    myObj['mode'] = "client";
    var jsonData = JSON.stringify(myObj);
    $.ajax({
        url: beUrl() + '/validateuser',
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: 'application/json',
        data: jsonData,
        error: function(response) {
            $("#signupUsername").next("label").attr('data-error', ' userName already taken');
            $("#signupUsername").removeClass("valid");
            $("#signupUsername").addClass("invalid");
        },
        success: function(response) {
            $("#signupUsername").next("label").attr('data-success', 'The userName is available');
            $("#signupUsername").removeClass("invalid");
            $("#signupUsername").addClass("valid");
        }
    });
}

function phoneAvailablity() {
    var validatePhone = document.getElementById('userPhone');
    var length = validatePhone.value.length;
    if (length < 10) {
        $("#userPhone").next("label").attr('data-error', 'Invalid number');
        $("#userPhone").removeClass("valid");
        $("#userPhone").addClass("invalid");
    } else if (length > 10) {
        $("#userPhone").next("label").attr('data-error', 'Too big phone number');
        $("#userPhone").removeClass("valid");
        $("#userPhone").addClass("invalid");
    } else {
        var phoneNumber = validatePhone.value;
        var phoneObj = {};
        phoneObj['phoneNumber'] = phoneNumber;
        phoneObj['userType'] = "CUST";
        var jsonPhone = JSON.stringify(phoneObj);
        $.ajax({
            url: beUrl() + '/validphone',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            data: jsonPhone,
            error: function(response) {
                $("#userPhone").next("label").attr('data-error', 'The phoneNumber is already taken');
                $("#userPhone").removeClass("valid");
                $("#userPhone").addClass("invalid");
            },
            success: function(response) {
                $("#userPhone").next("label").attr('data-success', 'The phoneNumber is available');
                $("#userPhone").removeClass("invalid");
                $("#userPhone").addClass("valid");
            }
        });
    }
}

function passwordValidity() {
    var validatePass = document.getElementById('signuppassword');
    var length = validatePass.value.length;
    if (length < 8) {
        $("#signuppassword").next("label").attr('data-error', 'Too small password');
        $("#signuppassword").removeClass("valid");
        $("#signuppassword").addClass("invalid");
    } else if (length > 24) {
        $("#signuppassword").next("label").attr('data-error', 'Too big password');
        $("#signuppassword").removeClass("valid");
        $("#signuppassword").addClass("invalid");
    } else {
        $("#signuppassword").next("label").attr('data-success', 'The password is acceptable');
        $("#signuppassword").removeClass("invalid");
        $("#signuppassword").addClass("valid");
    }
}

function showAddressInfo() {
    localStorage.removeItem("chosenAddId");
    localStorage.removeItem("shopCartInfo");
    localStorage.removeItem("itemsCartInfo");
    localStorage.setItem("totalBillAmount", 0);
    localStorage.setItem("totalGst", 0);
    document.getElementById('addressValues').innerHTML = "";
    document.getElementById('itemDetails').innerHTML = "";
    document.getElementById('totalSumValue').innerHTML = "";
    var ele = document.getElementsByName("group1");
    for (var i = 0; i < ele.length; i++)
        ele[i].checked = false;
    addressHandler();
}

function addressHandler() {
    if (localStorage.getItem("chosenAddId")) {
        setTimeout(function() {
            showFinalPage();
        }, 500);


    } else {
        var sessionInfo = localStorage.getItem("sessionInfo");
        var sessionDummy = JSON.parse(sessionInfo);
        var myObj = {};
        myObj['userId'] = sessionDummy.userId;
        var data = JSON.stringify(myObj);
        var head = sessionDummy.basicAuthenticate;
        $.ajax({
            url: beUrl() + '/custaddresslist',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            headers: {
                "basicauthenticate": "" + head
            },
            data: data,
            success: function(response) {
                var pocket = localStorage.getItem("pocket");
                pocket = JSON.parse(pocket);
                var poc = pocket.pocketName;
                var pocketName = JSON.stringify(pocket);
                var address = JSON.stringify(response);
                var addressUse = JSON.parse(address);
                var address = addressUse.address;
                if (addressUse.addressCount > 0) {
                    $.each(address, function(i, v) {
                        var addShow = v.addressLine1;
                        addShow = addShow + " " + v.addressLine2;
                        addShow = addShow + " " + v.city;
                        addShow = addShow + " " + v.state;
                        addShow = addShow + " " + v.country;
                        addShow = addShow + "-" + v.pincode;
                        var addName = v.addName;
                        var addId = v.addressId;
                        var chosenAddId = localStorage.getItem('chosenAddId');
                        var print = "<p><input name=group1 class=group1 type=radio id=" + addId + " onclick=handleClick(" + addId + ") /><label for=" + addId + " style='float:left'><h5 class='subhead'>" + addName + "</h5></label><p class='content'>" + addShow + "</p></p><hr>";
                        if (!document.getElementById(addId)) {
                            $('#addressList').append(print);
                        }
                        var addObj = {};
                        addObj['lattitude'] = v.addLat;
                        addObj['longitude'] = v.addLng;
                        addObj['addressLine1'] = v.addressLine1;
                        addObj['addressLine2'] = v.addressLine2;
                        addObj['city'] = v.city;
                        addObj['state'] = v.state;
                        addObj['country'] = v.country;
                        addObj['pincode'] = v.pincode;
                        addObj['addName'] = v.addName;
                        var addDelVal = JSON.stringify(addObj);
                        var aAddId = 'a' + addId;
                        sessionStorage.setItem(aAddId, addDelVal);
                        /*if(localStorage.getItem("delVal")){
                            window.location.replace(feUrl()+"/shops.html");
                          }*/
                    })
                    var print = "<p><input name=group1 class=with-gap type=radio id=new onclick=removeClick() /><label for=new style='float:left'><h5 class='subhead'>Add new ADDRESS</h5></label></p><hr>";
                    if (!document.getElementById("new")) {
                        $('#addressList').append(print);
                    }
                } else {
                    var print = "<p id=newAddress><h5>PLEASE ADD A NEW ADDRESS</h5></p>"
                    $('#addressList').append(print);
                }
                $('#addressModal').modal('open');
            }
        })
    }
}

function removeClick() {
    localStorage.removeItem('chosenAddId');
    localStorage.removeItem('delVal');
    localStorage.setItem('newAddress', "TRUE");
}

function handleClick(addId) {
    localStorage.removeItem('newAddress');
    localStorage.setItem('chosenAddId', addId);
    var aAddId = 'a' + addId;
    var delVal = sessionStorage.getItem(aAddId);
    var delValParse = JSON.parse(delVal);
    var addObj = {};
    addObj['lattitude'] = delValParse.lattitude;
    addObj['longitude'] = delValParse.longitude;
    var delStoreVal = JSON.stringify(addObj)
    localStorage.setItem('delVal', delStoreVal);
}

function insertNewAddress() {
    var pocket = localStorage.getItem("pocket");
    var poc = JSON.parse(pocket);
    var addressValue = {};
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var delVal = JSON.parse(localStorage.getItem('delVal'));
    addressValue['userId'] = sessionInfo.userId;
    addressValue['lat'] = poc.lat;
    addressValue['lng'] = poc.lng;
    addressValue['addLine1'] = document.getElementById('addLine1').value;
    addressValue['addLine2'] = document.getElementById('addLine2').value;
    addressValue['city'] = poc.city;
    addressValue['state'] = poc.state;
    addressValue['country'] =poc.country;
    addressValue['pincode'] = poc.pincode;
    addressValue['addName'] = document.getElementById('addressName').value;
    var jsonAddress = JSON.stringify(addressValue);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({
        url: beUrl() + '/addaddress',
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: 'application/json',
        headers: {
            "basicauthenticate": "" + head
        },
        data: jsonAddress,
        success: function(response) {
            var addData = JSON.stringify(response);
            var v = JSON.parse(addData);
            var addId = v.addressId;
            var addObj = {};
            addObj['lattitude'] = poc.lat;
            addObj['longitude'] = poc.lng;
            addObj['addressLine1'] = addressValue['addLine1'];
            addObj['addressLine2'] = addressValue['addLine2'];
            addObj['city'] = poc.city;
            addObj['state'] = poc.state;
            addObj['country'] = poc.country;
            addObj['pincode'] = poc.pincode;
            addObj['addName'] =  addressValue['addName'];
            var addDelVal = JSON.stringify(addObj);
            var aAddId = 'a' + addId
            sessionStorage.setItem(aAddId, addDelVal);
            localStorage.setItem('chosenAddId', addId);
            localStorage.removeItem("newAddress");
            if (localStorage.getItem('chosenAddId')) {
                $('#newAddressModal').modal('close');
                showFinalPage();
            }
        },
        error: function(response) {
            Materialize.toast('Please pass address value...', 4000);
            console.log(response);

        }
    });
}
