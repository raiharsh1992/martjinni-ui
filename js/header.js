$(document).ready(function() {
    $("#menuSideNav").sideNav({
        closeOnClick: true,
        draggable: true
    });
    if(screen.width > 995){
      var fPrint = '<style>footer {position: fixed;left: 0;width: 100%;bottom: 0;background-color: black;color: #e0e0e0;}</style><footer style="text-align: center; font-size:0.7em;" id="footer1"><div class="container" style="text-align:center">&#169; 2018 Martjinni Pvt. Ltd.</div></footer>';
      $("body").append(fPrint);
    }
    if (window.localStorage) {
        if (localStorage.getItem("sessionInfo")) {
            var log = '<li><a href="orders.html" style="color:#1e1e1e">Orders<i class="material-icons left" style="color:#1e1e1e">shopping_basket</i></a></li><li><a href="subs.html" style="color:#1e1e1e">Subscription<i class="material-icons left" style="color:#1e1e1e">access_alarms</i></a></li><li><a href="javascript:redirectHome()" style="color:#1e1e1e"><i class="material-icons left" style="color:#1e1e1e">edit_location</i>Change Address</a></li> <li><a href="restore.html" style="color:#1e1e1e"><i class="material-icons left" style="color:#1e1e1e">account_circle</i>Change Password</a></li><li><a href="logout.html" style="color:#1e1e1e"><i class="material-icons left" style="color:#1e1e1e">face</i>Logout</a></li><li><a href="contact.html" style="color:#1e1e1e"><i class="material-icons" style="color:#1e1e1e">perm_contact_calendar</i>Contact Us</a></li>';
        } else {
            var log = '<li><a href="javascript:redirectHome()" style="color:#1e1e1e"><i class="material-icons left" style="color:#1e1e1e">edit_location</i>Change Address</a></li><li><a href="login.html" style="color:#1e1e1e"><i class="material-icons left" style="color:#1e1e1e">face</i>Login</a></li><li><a href="signup.html" style="color:#1e1e1e"><i class="material-icons left" style="color:#1e1e1e">account_circle</i>SignUp</a></li><li><a href="contact.html" style="color:#1e1e1e"><i class="material-icons" style="color:#1e1e1e">perm_contact_calendar</i>Contact Us</a></li>';
        }
        $('#profileContent').append(log);
        if(screen.width > 995){
          var shop = '<li><a href="shops.html" ><i class="material-icons left">store</i>Shops</a></li>';
          xlog = shop + "<li><a href=/about.html><i class='material-icons left'>perm_device_information</i>About</a></li>";
        }
        else{
          var shop = '<li><a href="shops.html" style="color:#1e1e1e"><i class="material-icons left" style="color:#1e1e1e">store</i>Shops</a></li>';
          xlog = shop + "<li><a href='/about.html' style='color:#1e1e1e;'><i class='material-icons left' style='color:#1e1e1e;'>perm_device_information</i>About</a></li>";
        }

        var cartC = localStorage.getItem("cartCount");
        if ((localStorage.getItem("cartCount")) >= 1) {
            var cart = ' <li><a href="cart.html"><i class="material-icons left">add_shopping_cart</i>Cart<span class="badge" data-badge-caption="items" id="cartInfor">' + cartC + '</span> </a></li>';
            $('#test').append(cart);
            var cart = ' <li><a href="cart.html" style="color:#1e1e1e"><i class="material-icons left" style="color:#1e1e1e">add_shopping_cart</i>Cart<span class="badge" data-badge-caption="items" id="cartInforS">' + cartC + '</span> </a></li>';
            $('#test2').append(cart);
        }
        log = xlog + "" + log;
        xlog = xlog + '<li><a class="dropdown-button" id="hello2" href="#!" data-activates="profileContent"><i class="material-icons right">person_outline</i></a></li>';
        $('#test').append(xlog);
        $('#test2').append(log);
        $("#hello2").dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: false, // Displays dropdown below the button
            alignment: 'left', // Displays dropdown with edge aligned to the left of button
            stopPropagation: false // Stops event propagation
        });
    }
    $('head').append('<link rel="shortcut icon" href="images/logoFinal.png">');
    $('head').append("<!-- Global site tag (gtag.js) - Google Analytics --><script async src='https://www.googletagmanager.com/gtag/js?id=UA-114684493-1'></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-114684493-1');</script>");
});

function redirectHome() {
    localStorage.removeItem("delVal");
    var $toastContent = $('<span>Proceeding will clear cart</span>').add($('<button class="btn-flat toast-action" onclick="gotHome()">Proceed</button>'));
    Materialize.toast($toastContent);
}

function gotHome() {
    window.location.replace(feUrl() + "/index.html");
}

function updateCartValue() {
    if (document.getElementById('cartInfor')) {
        document.getElementById('cartInfor').innerHTML = "";
        document.getElementById('cartInforS').innerHTML = "";
        var cartCount = localStorage.getItem("cartCount");
        if (cartCount > 0) {
            $('#cartInfor').append(cartCount);
            $('#cartInforS').append(cartCount);
        } else {
            $('#cartInfor').append(0);
            $('#cartInforS').append(0);
        }
    } else {
        var cartC = localStorage.getItem("cartCount");
        var cart = ' <a href="cart.html"><i class="material-icons left">add_shopping_cart</i>Cart<span class="badge" data-badge-caption="items" id="cartInforS">' + cartC + '</span> </a>';
        $('#mobcart').append(cart);
        console.log('hello');
        var cart = ' <li><a href="cart.html"><i class="material-icons left">add_shopping_cart</i>Cart<span class="badge" data-badge-caption="items" id="cartInfor">' + cartC + '</span> </a></li>';
        $('#test').append(cart);
        var cart = ' <li><a href="cart.html" style="color:#1e1e1e"><i class="material-icons left" style="color:#1e1e1e">add_shopping_cart</i>Cart<span class="badge" data-badge-caption="items" id="cartInforS">' + cartC + '</span> </a></li>';
        $('#test2').append(cart);

    }
}
