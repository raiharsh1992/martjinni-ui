$(document).ready(function(){
    $('.collapsible').collapsible();
    if(localStorage.getItem('sessionInfo')){
      var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
      var orderId = getUrlVars()["subsId"];
      var myObj = {};
      myObj['subsId']=Number(orderId);
      myObj['typeId']=sessionInfo.typeId;
      myObj['userId']=sessionInfo.userId;
      myObj['userType']=sessionInfo.userType;
      var jsonObj = JSON.stringify(myObj);
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/viesubdetails',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
          var jsonData = JSON.stringify(response);
          var dataUse = JSON.parse(jsonData);
          $('#orderId').append(dataUse.subsId);
          $('#createdOnDate').append(dataUse.startDate);
          $('#totalAmount').append(dataUse.itemName);
          $('#addressName').append(dataUse.shopName);
          $('#totalItems').append(dataUse.value);
          var status = dataUse.itemStatus;
          var handshake = "images/handshakecolor.png";
          var intransit = "images/intransitcolor.png";
          var delivered ="images/deliveredcolor.png";
          var pr1 = 0;
          var pr2 = 0;
          var pr3 = 0;
          var printy="";
          var canDis = "none;"
          if(status=="NEW"){
            var delShp = 0;
            var xPrint = '<div class="row" style="position:relative;"> <div class="col s6" style="text-align:center;"> <div class="row" style="vertical-align:middle; padding-bottom:0px;"> <div class="col s3" style="padding:0px; vertical-align:middle;"> <i class="material-icons" style="color:green; padding:0px;">check_circle</i> </div> <div class="col s6" style="padding:0px; vertical-align:middle;"> <span class="progress"><span class="determinate light-green accent-3" style="width: 0%"></span></span> </div> <div class="col s3" style="padding:0px; vertical-align:middle;"> <i class="material-icons" style="color:grey; padding:0px;">check_circle</i> </div> </div> <div style="color:green; padding:0px;">' + dataUse.daysLeft + '</div> <div style="color:green; padding:0px;">Days Lef</div> </div> <div class="col s6" style="text-align:center;"> <div class="row" style="vertical-align:middle; padding-bottom:0px;"> <div class="col s9" style="padding:0px; vertical-align:middle;"> <span class="progress"><span class="determinate light-green accent-3" style="width:0%;"></span></span> </div> <div class="col s3" style="padding:0px; vertical-align:middle;"> <i class="material-icons" style="color:grey; padding:0px;">check_circle</i> </div> </div> <div style="color:green; padding:0px;">' + delShp + '</div> <div style="color:green; padding:0px;">Days Delivered</div> </div> </div>';
          }
          else if(status=="WORKING"){
            var ts = dataUse.subsDays;
            var sl = dataUse.daysLeft;
            var delShp = ts - sl;
            var prgbar = 100;
            if (sl != 0) 
            {
                prgbar = ((ts - sl) / ts) * 100;
            }
            var xPrint = '<div class="row" style="position:relative;"> <div class="col s6" style="text-align:center;"> <div class="row" style="vertical-align:middle; padding-bottom:0px;"> <div class="col s3" style="padding:0px; vertical-align:middle;"> <i class="material-icons" style="color:green; padding:0px;">check_circle</i> </div> <div class="col s6" style="padding:0px; vertical-align:middle;"> <span class="progress"><span class="determinate light-green accent-3" style="width: 100%"></span></span> </div> <div class="col s3" style="padding:0px; vertical-align:middle;"> <i class="material-icons" style="color:green; padding:0px;">check_circle</i> </div> </div> <div style="color:green; padding:0px;">' + dataUse.daysLeft + '</div> <div style="color:green; padding:0px;">Days Left</div> </div> <div class="col s6" style="text-align:center;"> <div class="row" style="vertical-align:middle; padding-bottom:0px;"> <div class="col s9" style="padding:0px; vertical-align:middle;"> <span class="progress"><span class="determinate light-green accent-3" style="width:'+prgbar+'%;"></span></span> </div> <div class="col s3" style="padding:0px; vertical-align:middle;"> <i class="material-icons" style="color:grey; padding:0px;">check_circle</i> </div> </div> <div style="color:green; padding:0px;">' + delShp + '</div> <div style="color:green; padding:0px;">Days Delivered</div> </div> </div>';
          }
          else{
            var xPrint = '<div class="row" style="position:relative;"> <div class="col s6" style="text-align:center;"> <div class="row" style="vertical-align:middle; padding-bottom:0px;"> <div class="col s3" style="padding:0px; vertical-align:middle;"> <i class="material-icons" style="color:green; padding:0px;">check_circle</i> </div> <div class="col s6" style="padding:0px; vertical-align:middle;"> <span class="progress"><span class="determinate light-green accent-3" style="width: 100%"></span></span> </div> <div class="col s3" style="padding:0px; vertical-align:middle;"> <i class="material-icons" style="color:green; padding:0px;">check_circle</i> </div> </div> <div style="color:green; padding:0px;">' + dataUse.daysLeft + '</div> <div style="color:green; padding:0px;">Days Left</div> </div> <div class="col s6" style="text-align:center;"> <div class="row" style="vertical-align:middle; padding-bottom:0px;"> <div class="col s9" style="padding:0px; vertical-align:middle;"> <span class="progress"><span class="determinate light-green accent-3" style="width:100%;"></span></span> </div> <div class="col s3" style="padding:0px; vertical-align:middle;"> <i class="material-icons" style="color:green; padding:0px;">check_circle</i> </div> </div> <div style="color:green; padding:0px;">' + dataUse.subsDays + '</div> <div style="color:green; padding:0px;">Days Delivered</div> </div> </div>';
          }
          $('#timeLine').append(xPrint);
          var items1 = dataUse.itemTranHistory;
          $.each(items1, function(i,v){
            var lPrint = "<tr><td>"+v.dayCount+"</td><td>"+v.dayEta+"</td><td>"+v.dayStatus+"</td></tr>";
            $('#itemsBody1').append(lPrint);
          });
        },
        error: function(response){
          Materialize.toast('Not authorized to view the page', 4000)
          setTimeout(function(){
             window.location.replace(feUrl()+"/index.html");
          }, 500);
        }
      });
    }
    else{
      Materialize.toast('Not authorized to view the page', 4000)
      setTimeout(function(){
         window.location.replace(feUrl()+"x.html");
      }, 500);
    }
  });

  function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
    });
    return vars;
  }
