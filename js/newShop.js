var noImage = feUrl() + "/images/noImage.gif";
var fav, currentTab, modView;
var currentView = "";
var availList = {};
var origin = {};
var catData = {};
var view = "plain"
$(document).ready(function() {
    localStorage.removeItem("displayBuffer");
    if (localStorage.getItem("sessionInfo")) {
        var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
        var typeId = sessionInfo.typeId;
        var userId = sessionInfo.userId;
        var userType = sessionInfo.userType;
        var head = sessionInfo.basicAuthenticate;
        var myObj = {};
        myObj['typeId'] = typeId;
        myObj['userId'] = userId;
        myObj['userType'] = userType;
        var data = JSON.stringify(myObj);
        $.ajax({
            url: beUrl() + '/viewfav',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            headers: {
                "basicauthenticate": "" + head
            },
            data: data,
            success: function(response) {
                var shopData = JSON.stringify(response);
                var shopInfo = JSON.parse(shopData);
                var favlist = shopInfo.favList;
                fav = favlist;
            },
            error: function(response) {
                var shopData = JSON.stringify(response);
                var shopInfo = JSON.parse(shopData);
                var Data2 = shopInfo.Data;
                Materialize.toast('Something went wrong', 4000);
            }
        });
    }
    if (window.localStorage) {
        if ((localStorage.getItem("delVal")) && (localStorage.getItem("searchLocal")) && (localStorage.getItem("pocket"))) {
            var searchValue = localStorage.getItem("searchLocal");
            var finalUse = searchValue + '<i class="material-icons left" style="color:#ffa751">location_on</i>';
            $("#shopTitleName").append(searchValue);
            var delInfo = JSON.parse(localStorage.getItem("delVal"));
            var myObj = {};
            myObj['lattitude'] = delInfo.lattitude;
            myObj['longitude'] = delInfo.longitude;
            var data = JSON.stringify(myObj);
            $.ajax({
                url: beUrl() + '/shoplist',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                data: data,
                success: function(response) {
                    var data = JSON.stringify(response);
                    var jsonData = JSON.parse(data);
                    var tabs = jsonData.tabs;
                    var mainTab = '<ul class="tabs" style="overflow:hidden;">';
                    var tabData = '';
                    $.each(tabs, function(i, v) {
                        //Prints Tabs and filters
                        var currentData = jsonData[v];
                        mainTab = mainTab + '<li class="tab col s4"><a href="#' + v + '">' + v + '</a></li>';
                        var tabData = '<div id="' + v + '" class="col s12" style="margin:0; padding:0; "><div class="row"><div class="col s12 m12 l12 xl12" style="margin:0; padding:0;"><div id="' + v + 'Info"></div><ul id="filterDropdown' + v + 'm" class="side-nav"></ul><div class="row"><div class="navbar1 hide-on-med-and-up"><div class="col s7"><a class="button-collapse" id="collap' + v + '" href="#" data-activates="filterDropdown' + v + 'm"><i class="material-icons left md-25" style="color:#ffa751;">tune</i><span style=" font-size:1em">Categories</a></div><div class="col s5"><span class="mobcart" style="margin-left:-0.5"></span></div></div></div></div></div></div>';
                        $("#tabData").append(tabData);
                        var mjAvailable = currentData.filterData.mjAvailable;
                        //Prints the heading of the filter
                        $.each(mjAvailable, function(k, z) {
                            var printx = '<ul class="collapsible" data-collapsible="accordion"><li><div class="collapsible-header" style="color:#262626; display:block; width:100%;  font-weight:300;">' + z.mjName + '</div><div class="collapsible-body" style="background-color:white"><ul id=mjF' + z.mjId + '' + v + '></ul></div></li></ul>';
                            $("#filterDropdown" + v + "m").append(printx);
                            $('.collapsible').collapsible();
                            $('#collap' + v).sideNav({
                                closeOnClick: true,
                            });
                        });
                        var catAvailable = currentData.filterData.catAvailable;
                        //Prints the list of available categories in all filter
                        $.each(catAvailable, function(o, p) {
                            var catMj = p.catMJ;
                            var catName = p.catName;
                            var catId = p.catId;
                            var shopList = JSON.stringify(p.shopList);
                            var catUserName = 'cat' + catId;
                            catData[catId] = catName;
                            if (p.shopList != "") {
                                if (!document.getElementById(catUserName)) {;
                                    var catInfo = JSON.stringify(v)
                                    var catp = catId + "cat";
                                    var printx = ' <li id="cat' + catId + '' + v + '" style="word-wrap: break-word; "><input type="checkbox" name="vehicle1" value="Bike"> <a href=javascript:displayFilter("' + p.shopList + '") style="color:black; width:100%; word-wrap: break-word; ">' + catName + '</a></li>';
                                    var idMain = "#mj" + catMj;
                                    $("#mjF" + catMj + v + "").append(printx);
                                    $("#mjK" + catMj + v + "").append(printx);
                                    $('.collapsible').collapsible();
                                }
                            } else {

                            }
                        });
                        //Prints View Favourites and Clear Filter
                        var printy = "<span class='view_fav_class' style='text-align:center; display:block; margin:5px;'><a href=javascript:vieFav()  class='view_fav_class btn btn-large' style='padding:5px; cursor:pointer; color:black; font-weight:400; font-size:1em; width:100%; vertical-align: middle;'><i class='material-icons' style='color:#e50922; vertical-align: middle;'>favorite</i>View Favs</a></span><span class='view_fav_class' style='text-align:center; display:block; margin:5px; border:2px solid #ffa751; '><a href=javascript:window.location.reload()  class='view_fav_class btn btn-large ' style='padding:5px; background-color:#fffbfa; cursor:pointer; color:black; font-weight:400; font-size:1em; width:100%; vertical-align: middle;'>Clear Filter</a></span>";
                        $("#filterDropdown" + v + "d").append(printy);
                        $("#filterDropdown" + v + "m").append(printy);
                        var list = currentData.shopList.open;
                        list = list.concat(currentData.shopList.close);
                        availList[v] = {
                            "shops": list,
                            "content": list
                        };
                        localStorage.setItem(v, list);
                        if (i == 0) {
                            currentTab = v;
                            modView = []
                            if (screen.width < 900) {
                                appendToScreen(5);
                            } else {
                                appendToScreen(15);
                            }
                        }
                    });
                    mainTab = mainTab + "</ul>";
                    $("#mainTab").append(mainTab);
                    $('ul.tabs').tabs({
                        onShow: function(tab) {
                            var activeTab = $(".active").attr('href');
                            var shopType = activeTab.substr(1);
                            currentTab = shopType;
                            modView = "";
                            currentView = "";
                            view = "plain"
                            if (screen.width < 900) {
                                appendToScreen(5);
                            } else {
                                appendToScreen(15);
                            }
                        }
                    });
                    //Prints bottom navigation.
                    var cartC = localStorage.getItem("cartCount");
                    if (cartC == 0 || cartC == null) {
                        var cart = '<a href="cart.html" ><i class="material-icons left" style="padding:0; margin:0; color:#ffa751;">add_shopping_cart</i><span class="new badge" data-badge-caption="" id="cartInforS" style="z-index:-2; background-color:white; color:black; padding:0; margin:0;"> 0 </span> </a>';
                    } else {
                        var cart = '<a href="cart.html"><i class="material-icons left" style="padding:0; margin:0; color:#ffa751;">add_shopping_cart</i><span class="new badge" data-badge-caption="" id="cartInforS" style="z-index:-2; background-color:white; color:black; padding:0; margin:0;">' + cartC + '</span> </a>';
                    }
                    $('.mobcart').append(cart);
                }
            });
            var useDataCat = JSON.stringify(catData);
            sessionStorage.setItem("catData", catData);
        } else {
            window.location.replace('index.html');
        }
    } else {
        window.location.replace('index.html');
    }
});

function vieFav() {
    if (localStorage.getItem("sessionInfo")) {
        var xPrint = "";
        var justUse = new Array()
        if (view == "filter") {
            var useData = currentView.split(",")
            $.each(useData, function(k, j) {
                justUse.push(Number(j))
            })
            $.each(fav, function(i, v) {
                var l = justUse.indexOf(v)
                if (l >= 0) {
                    if (xPrint == "") {
                        xPrint = v;
                    } else {
                        xPrint = xPrint + "," + v;
                    }
                }
            })
        } else {
            var content = (localStorage.getItem(currentTab)).split(",");
            var justFun = [];
            $.each(content, function(k, l) {
                justFun.push(Number(l))
            })
            $.each(fav, function(i, v) {
                var l = justFun.indexOf(v);
                if (l >= 0) {
                    if (xPrint == "") {
                        xPrint = v;
                    } else {
                        xPrint = xPrint + "," + v;
                    }
                }
            })
        }
        xPrint = xPrint + ",";
        displayFilter(xPrint);
    } else {
        Materialize.toast('Kindly login', 4000);
    }
}

function displayFilter(shopList) {
    modView = shopList.split(",");
    currentView = shopList;
    view = "filter"
    $("#" + currentTab + "Info").html('');
    if (screen.width < 900) {
        appendToScreen(5);
    } else {
        appendToScreen(15);
    }
}

function appendToScreen(size) {
    if (view == "plain") {
        var list = availList[currentTab].shops;
        if (list.length > 0) {
            var i;
            var useArr = new Array();
            for (i = 0; i < size; i++) {
                useArr.push(availList[currentTab].shops.shift());
            }
            var myObj = {};
            var print = "";
            myObj['shops'] = useArr;
            var data = JSON.stringify(myObj);
            $.ajax({
                url: beUrl() + '/getshopinfo',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                data: data,
                success: function(response) {
                    var r = JSON.stringify(response);
                    var res = JSON.parse(r);
                    var data = res.Data;
                    $.each(data, function(i, v) {
                        if (v.imageUploaded == 1) {
                            imagUrl = imgUrl() + "" + v.shopId + ".jpeg";
                        } else {
                            imagUrl = noImage;
                        }
                        var favImg;
                        var favStatus;
                        if (!localStorage.getItem("sessionInfo")) {
                            favImg = "images/ho.png";
                            favStatus = 2;
                        } else {
                            if (fav.indexOf(v.shopId) >= 0) {
                                favImg = "images/hf.png";
                                favStatus = 1;
                            } else {
                                favImg = "images/ho.png";
                                favStatus = 0;
                            }
                        }
                        var catId = v.catId.split(",");
                        var catName = catData[catId[0]];
                        var makingCall = "";
                        var shopStat = 'images/open.png';
                        if (v.isOpen == 0) {
                            shopStat = "images/closed.png";
                            if (currentTab == "subs") {
                                print = print + '<div class="col s12 m6 l4" style="padding:5px;" id="shp' + v.shopId + '"class="z-depth-2 hoverable"><div class="shopCard z-depth-2 hoverable" style="margin-bottom:1em" ><div class="row"><div class="col s6" style="padding:3px;"><div class="shopCard_img" style="background-image:url(' + imagUrl + ');"><p style="margin:0; padding:0; font-size:0.8em; background-color:#27ce1e; color:white; width:4em;" id="rate' + v.shopId + '"><i class=material-icons style="text-align:center; vertical-align:middle; font-size:1.2em;1">stars</i><span style="padding-left:0.4em;">' + v.rating + '</span></p><span style="width:20px; padding:0; margin:0px;"></span></div></div><div class="col s6" style="padding:1px;"><div class="shopCard_content"><span class="shopCard_name">' + v.shopName + '</span><span class="shopCard_address">' + v.address + '</span><span class="shopCard_cat">' + catName + '</span><span><img src="' + favImg + '" class="favImgidc" id="favImgid" style="z-index:+50;margin-top:-1.2em;  margin-right:1em;  height:auto; width:1.25em; padding:1px; cursor:pointer; vertical-align:center; float:right; display:block;" onclick="toggleFav(' + favStatus + ',' + v.shopId + ')"></span></span><img src="' + shopStat + '" class="openClose">'+makingCall+'</div></div></div></div></div>';
                                $(".loading_textreg").hide("slow");
                            } else {
                                print = print + '<div class="col s12 m6 l4" style="padding:5px;" id="shp' + v.shopId + '"class="z-depth-2 hoverable"><div class="shopCard z-depth-2 hoverable" style="margin-bottom:1em" ><div class="row"><div class="col s6" style="padding:3px;"><div class="shopCard_img" style="background-image:url(' + imagUrl + ');"><p style="margin:0; padding:0; font-size:0.8em; background-color:#27ce1e; color:white; width:4em;" id="rate' + v.shopId + '"><i class=material-icons style="text-align:center; vertical-align:middle; font-size:1.2em;1">stars</i><span style="padding-left:0.4em;">' + v.rating + '</span></p><span style="width:20px; padding:0; margin:0px;"></span></div></div><div class="col s6" style="padding:1px;"><div class="shopCard_content"><span class="shopCard_name">' + v.shopName + '</span><span class="shopCard_address">' + v.address + '</span><span class="shopCard_cat">' + catName + '</span><span class="shopCard_delChar">Del charge: <span class="shopCard_money">&#8377;' + v.delCharges + '</span><span><img src="' + favImg + '" class="favImgidc" id="favImgid" style="z-index:+50;margin-top:-1.2em;  margin-right:1em;  height:auto; width:1.25em; padding:1px; cursor:pointer; vertical-align:center; float:right; display:block;" onclick="toggleFav(' + favStatus + ',' + v.shopId + ')"></span></span><p class="shopCard_min">Del Free After: <span class="shopCard_money">&#8377;' + v.minOrderValue + '</span></p><img src="' + shopStat + '" class="openClose">'+makingCall+'</div></div></div></div></div>';
                                $(".loading_textreg").hide("slow");
                            }
                        } else {
                            if (currentTab == "subs") {
                                print = print + '<div class="col s12 m6 l4" style="padding:5px;" id="shp' + v.shopId + '"class="z-depth-2 hoverable"><div class="shopCard z-depth-2 hoverable" style="margin-bottom:1em" ><div class="row"><a onclick = toItem(' + v.shopId + ')><div class="col s6" style="padding:3px;"><div class="shopCard_img" style="background-image:url(' + imagUrl + ');"><p style="margin:0; padding:0; font-size:0.8em; background-color:#27ce1e; color:white; width:4em;" id="rate' + v.shopId + '"><i class=material-icons style="text-align:center; vertical-align:middle; font-size:1.2em;1">stars</i><span style="padding-left:0.4em;">' + v.rating + '</span></p><span style="width:20px; padding:0; margin:0px;"></span></div></div></a><div class="col s6" style="padding:1px;"><a onclick = toItem(' + v.shopId + ')><div class="shopCard_content"><span class="shopCard_name">' + v.shopName + '</span><span class="shopCard_address">' + v.address + '</span><span class="shopCard_cat">' + catName + '</span></a><span><img src="' + favImg + '" class="favImgidc" id="favImgid" style="z-index:+50;margin-top:-1.2em;  margin-right:1em;  height:auto; width:1.25em; padding:1px; cursor:pointer; vertical-align:center; float:right; display:block;" onclick="toggleFav(' + favStatus + ',' + v.shopId + ')"></span><a onclick = toItem(' + v.shopId + ')></span><img src="' + shopStat + '" class="openClose"></a>'+makingCall+'</div></div></div></div></div>';
                                $(".loading_textreg").hide("slow");
                            } else {
                                print = print + '<div class="col s12 m6 l4" style="padding:5px;" id="shp' + v.shopId + '"class="z-depth-2 hoverable"><div class="shopCard z-depth-2 hoverable" style="margin-bottom:1em" ><div class="row"><a onclick = toItem(' + v.shopId + ')><div class="col s6" style="padding:3px;"><div class="shopCard_img" style="background-image:url(' + imagUrl + ');"><p style="margin:0; padding:0; font-size:0.8em; background-color:#27ce1e; color:white; width:4em;" id="rate' + v.shopId + '"><i class=material-icons style="text-align:center; vertical-align:middle; font-size:1.2em;1">stars</i><span style="padding-left:0.4em;">' + v.rating + '</span></p><span style="width:20px; padding:0; margin:0px;"></span></div></div></a><div class="col s6" style="padding:1px;"><a onclick = toItem(' + v.shopId + ')><div class="shopCard_content"><span class="shopCard_name">' + v.shopName + '</span><span class="shopCard_address">' + v.address + '</span><span class="shopCard_cat">' + catName + '</span><span class="shopCard_delChar">Del charge: <span class="shopCard_money">&#8377;' + v.delCharges + '</span></a><span><img src="' + favImg + '" class="favImgidc" id="favImgid" style="z-index:+50;margin-top:-1.2em;  margin-right:1em;  height:auto; width:1.25em; padding:1px; cursor:pointer; vertical-align:center; float:right; display:block;" onclick="toggleFav(' + favStatus + ',' + v.shopId + ')"></span><a onclick = toItem(' + v.shopId + ')></span><p class="shopCard_min">Del Free After: <span class="shopCard_money">&#8377;' + v.minOrderValue + '</span></p><img src="' + shopStat + '" class="openClose"></a>'+makingCall+'</div></div></div></div></div>';
                                $(".loading_textreg").hide("slow");
                            }
                        }

                        if (v.rating <= 3.5 && v.rating >= 2) {
                            $("#rate" + v.shopId).css('background-color', '#f48042');
                        } else if (v.rating < 2 && v.rating > 0) {
                            $("#rate" + v.shopId).css('background-color', '#f72020');
                        } else if (v.rating <= 0.00) {
                            $("#rate" + v.shopId).css('display', 'none');
                        }
                    });
                    print = print + '<div class="as_float hide-on-small-only" style="position:fixed; bottom:25px; right:10px; float:right"> <a class="btn waves-effect waves-light" style="background-color: #ffa751; font-size:0.8em;" onClick="filterClicked(' + currentTab + ')"><i class="material-icons left">tune</i> Filter</a></div>';

                    $("#" + currentTab + "Info").append(print);
                }
            });
        }
    } else {
        if (modView.length > 0) {
            var useArr = new Array();
            for (i = 0; i < size; i++) {
                useArr.push(modView.shift());
            }
            var myObj = {};
            myObj['shops'] = useArr;
            var data = JSON.stringify(myObj);
            var print = "";
            $.ajax({
                url: beUrl() + '/getshopinfo',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                data: data,
                success: function(response) {
                    var r = JSON.stringify(response);
                    var res = JSON.parse(r);
                    var data = res.Data;
                    $.each(data, function(i, v) {
                        if (v.imageUploaded == 1) {
                            imagUrl = imgUrl() + "" + v.shopId + ".jpeg";
                        } else {
                            imagUrl = noImage;
                        }
                        var favImg;
                        var favStatus;
                        if (!localStorage.getItem("sessionInfo")) {
                            favImg = "images/ho.png";
                            favStatus = 2;
                        } else {
                            if (fav.indexOf(v.shopId) >= 0) {
                                favImg = "images/hf.png";
                                favStatus = 1;
                            } else {
                                favImg = "images/ho.png";
                                favStatus = 0;
                            }
                        }
                        var makingCall = "";
                        var catId = v.catId.split(",");
                        var catName = catData[catId[0]];
                        var shopStat = 'images/open.png';
                        if (v.isOpen == 0) {
                            shopStat = "images/closed.png";
                            print = print + '<div class="col s12 m6 l4" style="padding-left:0px; padding-right:0px" id="shp' + v.shopId + '"class="z-depth-2 hoverable"><div class="shopCard z-depth-2 hoverable" style="margin-bottom:1em" ><div class="row"><div class="col s6" style="padding:3px;"><div class="shopCard_img" style="background-image:url(' + imagUrl + ')"></div></div><div class="col s6" style="padding:1px;"><div class="shopCard_content"><span class="shopCard_name">' + v.shopName + '</span><span class="shopCard_address">' + v.address + '</span><span class="shopCard_cat">' + catName + '</span><span class="shopCard_delChar">Del charge: <span class="shopCard_money">&#8377;' + v.delCharges + '</span><span><img src="' + favImg + '" class="favImgidc" id="favImgid" style="z-index:+50;margin-top:-1.2em;  margin-right:1em;  height:auto; width:1.25em; padding:1px; cursor:pointer; vertical-align:center; float:right; display:inline-block;" onclick="toggleFav(' + favStatus + ',' + v.shopId + ')"></span></span><p class="shopCard_min">Del Free After: <span class="shopCard_money">&#8377;' + v.minOrderValue + '</span></p><img src="' + shopStat + '" class="openClose">'+makingCall+'</div></div></div></div></div>';
                        } else {
                            print = print + '<div class="col s12 m6 l4" style="padding-left:0px; padding-right:0px" id="shp' + v.shopId + '"class="z-depth-2 hoverable"><div class="shopCard z-depth-2 hoverable" style="margin-bottom:1em" ><div class="row"><a onclick = toItem(' + v.shopId + ')><div class="col s6" style="padding:3px;"><div class="shopCard_img" style="background-image:url(' + imagUrl + ')"></div></div></a><div class="col s6" style="padding:1px;"><a onclick = toItem(' + v.shopId + ')><div class="shopCard_content"><span class="shopCard_name">' + v.shopName + '</span><span class="shopCard_address">' + v.address + '</span><span class="shopCard_cat">' + catName + '</span><span class="shopCard_delChar">Del charge: <span class="shopCard_money">&#8377;' + v.delCharges + '</span></a><span><img src="' + favImg + '" class="favImgidc" id="favImgid" style="z-index:+50;margin-top:-1.2em;  margin-right:1em;  height:auto; width:1.25em; padding:1px; cursor:pointer; vertical-align:center; float:right; display:inline-block;" onclick="toggleFav(' + favStatus + ',' + v.shopId + ')"></span><a onclick = toItem(' + v.shopId + ')></span><p class="shopCard_min">Del Free After: <span class="shopCard_money">&#8377;' + v.minOrderValue + '</span></p><img src="' + shopStat + '" class="openClose"></a>'+makingCall+'</div></div></div></div></div>';
                        }

                        $(".loading_textreg").hide("slow");
                    });

                    print = print + '<div class="as_float hide-on-small-only" style="position:fixed; bottom:25px; right:10px; float:right"> <a class="btn waves-effect waves-light" style="background-color: #ffa751; font-size:0.8em;" onClick="filterClicked(' + currentTab + ')"><i class="material-icons left">tune</i> Filter</a></div>' + '<div class="as_float hide-on-small-only" style="position:fixed; bottom:25px; left:10px; float:right"> <a class="btn waves-effect waves-light" style="background-color: #fff; border:2px solid #ffa751; font-size:0.8em; color:black;" onClick="window.location.reload()"><i class="material-icons left">cancel</i>Clear Filter</a></div>';
                    $("#" + currentTab + "Info").append(print);
                }
            });
        }
    }
}

function filterClicked(a) {
    $("#collap" + a.id).click();
}

$(window).scroll(function() {
    if ($(window).scrollTop() + $(window).height() > $(document).height() - 1500) {
        if (screen.width < 900) {
            appendToScreen(5);
        } else {
            appendToScreen(15);
        }
    }
});

function toggleFav(favStatus, shopId) {
    if (favStatus == 2) {
        Materialize.toast('Kindly login', 4000);
    } else {
        var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
        var container = {};
        container['typeId'] = sessionInfo.typeId;
        container['userType'] = sessionInfo.userType;
        container['userId'] = sessionInfo.userId;
        container['shopId'] = shopId;
        var head = sessionInfo.basicAuthenticate;
        var data = JSON.stringify(container);
        if (favStatus == 1) {
            $.ajax({
                url: beUrl() + '/delfav',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                headers: {
                    "basicauthenticate": "" + head
                },
                data: data,
                success: function(response) {
                    var shopData = JSON.stringify(response);
                    var shopInfo = JSON.parse(shopData);
                    console.log(shopInfo)
                    window.location.reload();
                },
                error: function(response) {
                    console.log(response);
                    Materialize.toast('Something went wrong', 4000)
                }
            });
        } else if (favStatus == 0) {
            $.ajax({
                url: beUrl() + '/addtofav',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                headers: {
                    "basicauthenticate": "" + head
                },
                data: data,
                success: function(response) {
                    var shopData = JSON.stringify(response);
                    var shopInfo = JSON.parse(shopData);
                    console.log(shopInfo)
                    window.location.reload();
                },
                error: function(response) {
                    var cap = JSON.stringify(response);
                    var cap2 = JSON.parse(cap);
                    var res = cap.Data;
                    console.log(res);
                    Materialize.toast('Something went wrong', 4000)
                }
            });
        }
    }
}

function toItem(shopId) {
    window.location.replace('items.html?shopId=' + shopId + '&fromPage=' + currentTab);
}
