$(document).ready(function() {
    $('.modal').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5,
    })
    sessionStorage.removeItem('selectedCust');
    if (!localStorage.getItem("sessionInfo")) {
        window.location.replace('index.html');
    }
    <!-----------------------------------------End of Cust list and selection------------>
    showbills();
});

function showbills() {
    var si = localStorage.getItem("sessionInfo");
    var sessionInfo = JSON.parse(si);
    var myObj = {};
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userId'] = sessionInfo.userId;
    myObj['userType'] = sessionInfo.userType;
    var head = sessionInfo.basicAuthenticate;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
        url: beUrl() + '/viewbilllist.',
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: 'application/json',
        headers: {
            "basicauthenticate": "" + head
        },
        data: jsonObj,
        success: function(response) {
            var res = JSON.stringify(response);
            var response = JSON.parse(res);
            var totalCount = response.totalCount;

            if (totalCount == 0) {
                var printx = "You don't have any bills Right Now";
                $("#stat0").append(printx);
                $("#stat1").append(printx);
                $("#stat2").append(printx);
            } else {
                var orderId;
                var data = response.Data;
                $.each(data, function(i, v) {
                    var shopName = v.shopName;
                    var totalBill = v.totalBill;
                    var billId = v.billId;
                    var isPaid = v.isPaid;
                    var custName = v.custName;
                    var orderCount = v.orderCount;
                    var totalBill = v.totalBill;
                    var orders = v.orders;
                    var printx = "<li><p class='markpaid' style='float:right'><a class='btn red' style='margin:1em;' onclick='paybill(" + billId + ")' >Mark Paid</a></p><div class='collapsible-header shopCard_name' style='font-weight:600; font-size:1em;'> " + custName + " <br> Bill Id:" + billId + " <br> Order Count : " + orderCount + " <br> Total Bill Amount " + totalBill + " <a style='float:right'href='#' class=''>Deatils</a></div><div class='collapsible-body' id='billId" + billId + "'></li>";
                    $("#stat" + isPaid).append(printx);
                    $.each(orders, function(i, v) {
                        orderId = v.orderId;
                        var orderStatus = v.orderStatus;
                        var totalItemCount = v.totalItemCount;
                        var items = v.items;
                        var totalAmount = v.totalAmount;
                        var orderDate = v.orderDate;
                        var printy = "<div class='z-depth-3' style='padding-top:1em; padding-bottom:1em;' id='orderId" + orderId + "'> orderId " + orderId + " <br>orderStatus " + orderStatus + " <br>totalItemCount " + totalItemCount + " <br>totalAmount " + totalAmount + " <br>orderDate " + orderDate + "<div id='#orderId" + orderId + "'></div></div>";
                        $('#billId' + billId).append(printy);
                        $.each(items, function(i, v) {
                            var printz = "<br>" + v.itemName;
                            $('#orderId' + orderId).append(printz);
                        });
                    });
                    console.log("#stat" + isPaid);
                    if (isPaid == 2) {
                        $('.markpaid').hide();
                    }

                });
            }
        },
        error: function(response) {
            var responseJson = JSON.stringify(response);
            var responseJsonR = JSON.parse(responseJson);
            var statusCode = responseJsonR.responseJSON;
            var data = JSON.stringify(statusCode);
            var xData = JSON.parse(data);
            var message = xData.Data;
            Materialize.toast(message, 4000);
        }
    });
}

function paybill(billId) {
    var si = localStorage.getItem("sessionInfo");
    var sessionInfo = JSON.parse(si);
    var myObj = {};
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userId'] = sessionInfo.userId;
    myObj['userType'] = sessionInfo.userType;
    myObj['billId'] = billId;
    var head = sessionInfo.basicAuthenticate;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
        url: beUrl() + '/paythebill.',
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: 'application/json',
        headers: {
            "basicauthenticate": "" + head
        },
        data: jsonObj,
        success: function(response) {
            var res = JSON.stringify(response);
            var response = JSON.parse(res);
            var message = response.Data;
            console.log(message);
            //location.reload();
        },
        error: function(response) {
            var responseJson = JSON.stringify(response);
            var responseJsonR = JSON.parse(responseJson);
            var statusCode = responseJsonR.responseJSON;
            var data = JSON.stringify(statusCode);
            var xData = JSON.parse(data);
            var message = xData.Data;
            Materialize.toast('Bill already Paid', 4000);
        }
    });
}
