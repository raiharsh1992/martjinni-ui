var noImage = feUrl() + "/images/noimage.gif";
$(document).ready(function() {

    $('.collapsible').collapsible();
   

    if (localStorage.getItem("sessionInfo")) {
        var arr = new Array();
        var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
        var typeId = sessionInfo.typeId;
        var userId = sessionInfo.userId;
        var userType = sessionInfo.userType;
        var head = sessionInfo.basicAuthenticate;
        var myObj = {};
        myObj['typeId'] = typeId;
        myObj['userId'] = userId;
        myObj['userType'] = userType;
        var data = JSON.stringify(myObj);
        $.ajax({
            url: beUrl() + '/viewfav',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: 'application/json',
            headers: {
                "basicauthenticate": "" + head
            },
            data: data,
            success: function(response) {
                var shopData = JSON.stringify(response);
                var shopInfo = JSON.parse(shopData);
                var favlist = shopInfo.favList;
                arr = favlist.split(",");
                console.log(arr);
                localStorage.setItem('favlist', arr);
                //window.location.reload();
            },
            error: function(response) {
                console.log('reporting from view fav');
                var shopData = JSON.stringify(response);
                var shopInfo = JSON.parse(shopData);
                var Data2 = shopInfo.Data;
                console.log(response);
                Materialize.toast('Something went wrong', 4000)
            }


        });
    }
    
    
    $('select').material_select();
    
    $('.modal').modal()
    
    if (window.localStorage) 
    {
        if ((localStorage.getItem("delVal")) && (localStorage.getItem("searchLocal"))) 
        {
            var searchValue = localStorage.getItem("searchLocal");
            var finalUse = searchValue + '<i class="material-icons right">location_on</i>';
             
            $("#shopTitleName").append(finalUse)
            var url = feUrl() + '/shoplist';
            var delVal = localStorage.getItem("delVal");
            var delInfo = JSON.parse(delVal);
            var myObj = {};
            myObj['lattitude'] = delInfo.lattitude;
            myObj['longitude'] = delInfo.longitude;
            var data = JSON.stringify(myObj);
            $.ajax({
                url: beUrl() + '/shoplist',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                data: data,
                success: function(response) 
                {
                    var data = JSON.stringify(response);
                    var jsonData = JSON.parse(data);
                    var tabs = jsonData.tabs;
                    var mainTab = '<ul class="tabs">';
                    var tabData = '';
                    var g=0;
                    $.each(tabs, function(i,v){
                        if(g==0)
                            {   var ftab = JSON.stringify(v);
                                localStorage.setItem("fTab",v );
                                g++;
                            }
                        
                        var k = '<li class="tab col s3"><a href="#'+v+'">'+v+'</a></li>';
                        mainTab = mainTab + k;
                        var m = '<div id="'+v+'" class="col s12"><div class="row"><div class="col s0 m3 l3 xl3"><div class="row" style="position:fixed; width:23%;"><div class="fixed"><div class="hide-on-med-and-down" style="width:100%; " id="filter'+v+'d"><div class="card grey z-depth-5" id="headul" style="min-height:20em; max-height:35em; color:orange"><div class="card-content white-text" style=" overflow:auto;"><span class="card-title" style="text-align:center">Choose Category</span><ul id="filterDropdown'+v+'d"></ul></div></div></div></div></div></div><div class="col s12 m9 l9 xl9"><div id="'+v+'Info"></div><ul id="filterDropdown'+v+'m" class="side-nav"></ul><div class="row"><div class="navbar1"><div class="col s7"><a class="button-collapse" id="collap'+v+'" href="#" data-activates="filterDropdown'+v+'m" ><i class="material-icons left md-25">filter_list</i><span style="font-family:Pattaya, sans-serif; font-size:1em">Categories</a></div><div class="col s5"><span class="mobcart" style="margin-left:-0.5"></span></div></div></div></div></div></div>';
                        tabData = tabData + m;
                        
                    })
                    mainTab = mainTab + "</ul>";
                    $("#mainTab").append(mainTab);
                    $("#tabData").append(tabData);
                   
                      
                    
                        var cartC = localStorage.getItem("cartCount");
                        if (cartC == 0 || cartC == null) {
                        var cart = ' <a href="cart.html" style="margin-left:-1em"><i class="material-icons left">add_shopping_cart</i><span class="new badge red" data-badge-caption="" id="cartInforS" style="z-index:-2"> 0 </span> </a>';
                    } 
                    else {
                        var cart = ' <a href="cart.html" style="margin-left:-1em"><i class="material-icons left">add_shopping_cart</i><span class="new badge red" data-badge-caption="" id="cartInforS" style="z-index:-2;margin-right:-1em;" >' + cartC + '</span> </a>';
                    }
                    $('.mobcart').append(cart);
                    
                    $.each(jsonData, function(i, v)
                    {   
                        if(i == "tabs")
                    {
                        console.log("Tab is skipped");
                    }
                           else
                           { /*Condition if any shop is open or closed*/
                               if(v.count >0)
                                   {
                                       var open = v.shopList.open;
                                       var close = v.shopList.close;
                                       var allInitial =JSON.stringify (open.concat(close));
                                       var name = i + "ini";
                                       var name2 = i + "rem";
                                       localStorage.setItem(name, allInitial);
                                       localStorage.setItem(name2, "[]")
                                       var mjAvailable = v.filterData.mjAvailable;
                                            $.each(mjAvailable, function(k, z) {
                                                var printx = '<ul class="collapsible" data-collapsible="accordion" style="padding:3px;"><li style="padding:3px;"><div class="collapsible-header filterView" style="color:black; border:1px solid black; font-size:1.35em;  font-weight:900;">' + z.mjName + '</div><div class="collapsible-body" style="background-color:white"><ul id=mjF' + z.mjId + '></ul></div></li></ul>';
                                                
                                                $("#filterDropdown"+i+"m").append(printx);
                                                $('.collapsible').collapsible();

                                                var printx = '<ul class="collapsible" data-collapsible="accordion"><li><div class="collapsible-header" style="color:#262626; display:block; width:100%; font-family:Arima Ma.durai; font-family:Arima Madurai; font-weight:900;">' + z.mjName + '</div><div class="collapsible-body" style="background-color:white"><ul id=mjK' + z.mjId + '></ul></div></li></ul>';
                                                $("#filterDropdown"+i+"d").append(printx);
                                                $('.collapsible').collapsible();
                                                $('#collap'+i).sideNav();
                                            });
                                       //This Prints the View Fav button
                                       var printy = "<span style='text-align:center; display:block;  padding: 3px; border:1px groove red; '><img src='images/hf.png' style='height:1em; width:auto;'><a href='javascript:viewfavs()' style='padding:5px; font-family:monospace; cursor:pointer; color:black;'>View Favs</a></span><p onclick='location.reload();' style='padding:5px; font-family:monospace; display:block; cursor:pointer;'>Clear Filter</p>"
                                       $("#filterDropdown"+i+"d").append(printy);
                                       var printy = "<span style='text-align:center; display:block; padding: 3px 5px; border:1px groove red;'><span><img src='images/hf.png' style='height:1em; width:auto;'><a href='javascript:viewfavs()' style='padding:5px;   cursor:pointer; color:black;'>View Favs</a></span></span><p onclick='location.reload();' style='padding:5px;  display:block; cursor:pointer; color:black;'>Clear Filter</p>"
                                       $("#filterDropdown"+i+"m").append(printy);
                                       var catCount = 0;
                                       var catAval = v.filterData.catAvailable;
                                       $.each(catAval, function(i, v) {
                                           var catMj = v.catMJ;
                                           var catName = v.catName;
                                           var catId = v.catId;
                                           var shopList = JSON.stringify(v.shopList);
                                           var catUserName = 'cat' + catId;
                                           if (!document.getElementById(catUserName)) {
                                               catCount++;
                                               var catInfo = JSON.stringify(v)
                                               var catp = catId + "cat";
                                               var printx = '<li id="cat' + catId + '" style="word-wrap: break-word;"><a href="javascript:applyFilter(' + shopList + ')" style="color:black; width:100%; word-wrap: break-word; ">' + catName + '</a></li>';
                                               var idMain = "#mj" + catMj;
                                               $("#mjF" + catMj + "").append(printx);
                                               $("#mjK" + catMj + "").append(printx);
                                               $('.collapsible').collapsible();
                                           }
                                       });
                                       //End of the filetr
                                   }
                               else{
                                   console.log('0 open '+i);
                               }
                               
                           }
                     })
                     var fTab = localStorage.getItem('fTab');
                    console.log(fTab);
                    displayShops(fTab, 11);
                      $('ul.tabs').tabs({ 
                            onShow: function(tab) 
                            { var activeTab = $(".active").attr('href'); 
                              console.log(activeTab);
                             var shopType = activeTab.substr(1);
                             console.log(shopType);
                             displayShops(shopType, 11);
                            } 
                        });
            
            }
            });
        }
    else
        {   console.log('Nothing here');
            window.location.replace('index.html');
        }
        
    }
});

function displayShops(shopType, shopNum)
{   console.log("display function called");
    var shopiniS = shopType+"ini";
    var shopsremS = shopType+"rem";
 console.log(shopType);
    var shopini = JSON.parse(localStorage.getItem(shopType+"ini"));
    var shopsrem = JSON.parse(localStorage.getItem(shopType+"rem"));
 console.log("shop rem in start "+shopsrem);
 console.log("shop ini in start "+shopini);
    if(shopini == null)
        {
          shopini = [];  
        }
       if(shopsrem == null)
        {
          shopsrem =[];  
        }
    if(shopini.length > 0)
        {   if(shopini.length < shopNum )
            {
                shopNum = shopini.length;
                console.log("shop num if  in start length < shopNum "+shopNum);
            }
            
            var d = [];
         console.log(shopNum);
            for(var i=0; i<shopNum; ++i)
                {
                    d [i] = shopini[i];
                    console.log("d at" +i+" = " +d [i] );
                }
         shopini =  $(shopini).not(d).get();
        var finalshoprem =  shopsrem.concat(d);
         var shopinistr2 = JSON.stringify(shopini);
         var finalshopremstr2 = JSON.stringify(finalshoprem);
         console.log("new shop rem" + finalshopremstr2);
         localStorage.setItem(shopType+"ini",shopinistr2);
         console.log("new shop ini" + shopinistr2);
         localStorage.setItem(shopType+"rem",finalshopremstr2);
         
         var myObj = {};
         myObj['shops'] = d;
         var data = JSON.stringify(myObj);
         console.log(data);
          $.ajax({
                url: beUrl() + '/getshopinfo',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                data: data,
                success: function(response) { 
                    var r = JSON.stringify(response);
                    var res = JSON.parse(r);
                    var data = res.Data;
                    $.each(data, function(i, v){
                        if (v.imageUploaded == 1) {
                                  imagUrl = imgUrl() + "" + v.shopId + ".jpeg";
                              } else {
                                  imagUrl = noImage;
                              }
                        var print = '<div class="col s12 m6 l6" style="padding-left:0px; padding-right:0px" id="shp'+v.shopId+'"class="z-depth-2 hoverable"><div class="shopCard z-depth-2 hoverable" style="margin-bottom:1em"><div class="row"><div class="col s6" style="padding:3px;"><div class="shopCard_img" style="background-image:url('+imagUrl+')"></div></div><div class="col s6" style="padding:1px;"><div class="shopCard_content"><span class="shopCard_name">'+ v.shopName + '</span><span class="shopCard_address">'+ v.address +'</span><span class="shopCard_cat">'+v.shopDesc+'</span><span class="shopCard_delChar">Del charge: <span class="shopCard_money">&#8377;' + v.delCharges + '</span><span><img src= class="favImgidc" id="favImgid" style="margin-top:-1.2em;  margin-right:1em;  height:1.3em; width:auto; padding:1px; cursor:pointer; vertical-align:center; float:right; display:inline-block;"></span></span><p class="shopCard_min">Del Free After: <span class="shopCard_money">&#8377;' + v.minOrderValue + '</span></p><img src="images/open.png" class="openClose"></div></div></div></div></div>';
                        console.log(print);
                        $("#"+shopType+"Info").append(print);
                    })
                }});
                }
  else{
        console.log("Nothing to Display Now")
    }
                }
   
function redirect() {
    window.location.replace(feUrl());
}

function applyFilter(catId) {
    $('.carousel').hide("slow").fadeOut("slow");
    $('#hello').sideNav('hide');
    if (localStorage.getItem("favlist")) {
        var arr = localStorage.getItem("favlist");
    } else {
        var arr = [];
    }
    if (catId == 0) {
        window.location.reload();
    } else {
        if (localStorage.getItem('availableShopList')) {
            var shoplist = localStorage.getItem('availableShopList');
            var temp = new Array();
            temp = shoplist.split(",");
            //document.getElementById('test3').innerHTML = "";
            document.getElementById('test4').innerHTML = "";
            $("#test3").html('').fadeOut("slow").fadeIn("slow");
            var x = 0;
            for (i = 0; i < temp.length; i++) {
                var shopId = temp[i];
                var shopInfo = JSON.parse(localStorage.getItem(shopId));
                var temp1 = new Array();
                temp1 = shopInfo.catId.split(",");
                if (shopInfo.isOpen == 1) {
                    for (j = 0; j < temp1.length; j++) {
                        if (temp1[j] == catId) {
                            x = 1;
                            var catUseName = catId + "cat";
                            var useName = JSON.parse(sessionStorage.getItem(catUseName)).catName;
                            var isOpen = shopInfo.isOpen;
                            var shopName = shopInfo.shopName;
                            var shopId = shopInfo.shopId;
                            var delCharges = shopInfo.delCharges;
                            var minOrder = shopInfo.minOrderValue;
                            var shopDesc = shopInfo.shopDesc;
                            var shopAddress = shopInfo.address;
                            var shopIdStr = String(shopId);
                            console.log(shopIdStr);
                            var isin = arr.includes(shopIdStr);
                            var favImg;
                            var favStatus;
                            if (!localStorage.getItem("sessionInfo")) {
                                favImg = "images/ho.png";
                                favStatus = 2;
                                typeId = 0;
                                userId = 0;
                                userType = 0;

                                console.log('Session Info not detected');
                            } else {
                                sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
                                typeId = sessionInfo.typeId;
                                userId = sessionInfo.userId;
                                userType = "'" + sessionInfo.userType + "'";
                                console.log(userType);
                                if (isin == false) {
                                    favImg = "images/ho.png";
                                    favStatus = 0;
                                } else if (isin == true) {
                                    favImg = "images/hf.png";
                                    favStatus = 1;
                                }
                            }

                            var imagUrl = imgUrl() + "" + shopId + ".jpeg";

                            if (shopInfo.imageUploaded == 1) {
                                imagUrl = imgUrl() + "" + shopId + ".jpeg";
                            } else {
                                imagUrl = noImage;
                            }
                            var print = '<div class="col s12 m6 l6" style="padding-left:0px; padding-right:0px" id="shp' + shopId + '" class="z-depth-2 hoverable"><a id="press_cart" href=/items.html?shopId=' + shopId + ' style="color:black;"><div class="shopCard z-depth-2 hoverable" style="margin-bottom:1em"><div class="row"><div class="col s6" style="padding:3px;"><div class="shopCard_img" style="background-image:url(' + imagUrl + ')"></div></div><div class="col s6" style="padding:1px;"><div class="shopCard_content"><span class="shopCard_name"> ' + shopName + ' </span><span class="shopCard_address">' + shopAddress + '</span><span class="shopCard_cat">' + useName + '</span><span class="shopCard_delChar">Del charge: <span class="shopCard_money">&#8377;' + delCharges + '</span><span></a><img src=' + favImg + ' class="favImgidc" id="favImgid" style="margin-top:-1.2em;  margin-right:1em;  height:1.3em; width:auto; padding:1px; cursor:pointer; vertical-align:center; float:right; display:inline-block; " onclick="toggleFav(' + favStatus + ',' + typeId + ',' + userId + ',' + String(userType) + ',' + shopId + ')"></span><a id="press_cart" href=/items.html?shopId=' + shopId + ' style="color:black;"></span><p class="shopCard_min">Del Free After: <span class="shopCard_money">&#8377;' + minOrder + '</span></p><img src="images/open.png" class="openClose"></div></div></div></a></div></div>';
                            $('#test3').append(print)
                        }
                    }
                }
            }
            if (x == 0) {
                Materialize.toast('No shops available for cateogary now', 4000)
            }
        } else {
            window.location.reload();
        }
    }
}

function viewfavs() {
    $('.carousel').hide("slow").fadeOut("slow");
    $('#hello').sideNav('hide');
    if (localStorage.getItem('favlist')) {
        var favlist = localStorage.getItem('favlist');
        var arr = favlist.split(',');
        if (localStorage.getItem('availableShopList')) {
            var shoplist = localStorage.getItem('availableShopList');
            var temp = new Array();
            temp = shoplist.split(",");
            var x = 0;
            for (i = 0; i < temp.length; i++) {
                var shopId = temp[i];
                var shopIdStr = String(shopId);
                var isin = arr.includes(shopIdStr);
                var shopInfo = JSON.parse(localStorage.getItem(shopId));
                if (shopInfo.isOpen == 1) {
                    if (isin == false) {
                        $("#shp" + shopId).hide("slow").fadeOut("slow");
                    }
                }

            }
        }
    } else {
        Materialize.toast('Kindly add favourite', 10000)
    }
}

function toggleFav(favStatus, typeId, userId, userType, shopId) {
    console.log(favStatus, typeId, userId, userType, shopId);
    if (!localStorage.getItem("sessionInfo")) {
        Materialize.toast('PLease Login to add as Favourite', 4000)
    } else {
        //If fav status ==1 or if shop is marked as favourite we will delete favourite
        if (favStatus == 1) {
            var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
            var head = sessionInfo.basicAuthenticate;
            var myObj = {};
            myObj['typeId'] = typeId;
            myObj['userId'] = userId;
            myObj['userType'] = userType;
            myObj['shopId'] = shopId;
            var data = JSON.stringify(myObj);
            $.ajax({
                url: beUrl() + '/delfav',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                headers: {
                    "basicauthenticate": "" + head
                },
                data: data,
                success: function(response) {
                    var shopData = JSON.stringify(response);
                    var shopInfo = JSON.parse(shopData);
                    console.log(shopInfo)
                    window.location.reload();
                },
                error: function(response) {
                    console.log(response);
                    Materialize.toast('Something went wrong', 4000)
                }


            });
        }
        //if fav status is ==0 mark shop as favourite
        else if (favStatus == 0) {
            var sessionInfo = JSON.parse(localStorage.getItem("sessionInfo"));
            var head = sessionInfo.basicAuthenticate;
            var myObj = {};
            myObj['typeId'] = typeId;
            myObj['userId'] = userId;
            myObj['userType'] = userType;
            myObj['shopId'] = shopId;
            var data = JSON.stringify(myObj);
            $.ajax({
                url: beUrl() + '/addtofav',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                headers: {
                    "basicauthenticate": "" + head
                },
                data: data,
                success: function(response) {
                    var shopData = JSON.stringify(response);
                    var shopInfo = JSON.parse(shopData);
                    console.log(shopInfo)
                    window.location.reload();
                },
                error: function(response) {
                    var cap = JSON.stringify(response);
                    var cap2 = JSON.parse(cap);
                    var res = cap.Data;
                    console.log(res);
                    Materialize.toast('Something went wrong', 4000)
                }
            });
        }
    }
}
