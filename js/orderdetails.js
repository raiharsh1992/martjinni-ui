
$(document).ready(function(){
    var shopOrderListvar = [];
    $("#refresh_btn").hide();
     $('.modal').modal();
setInterval(function(){$("#refresh_btn").show(); console.log('Function executed') },300000);
    $('.collapsible').collapsible();
    if(localStorage.getItem('sessionInfo')){
      var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
      var orderId = getUrlVars()["orderId"];
      var myObj = {};
      myObj['orderId']=Number(orderId);
      myObj['typeId']=sessionInfo.typeId;
      myObj['userId']=sessionInfo.userId;
      myObj['userType']=sessionInfo.userType;
      var jsonObj = JSON.stringify(myObj);
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/orderdetails',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
          var jsonData = JSON.stringify(response);
          var dataUse = JSON.parse(jsonData);
          $('#orderId').append(dataUse.orderId);
          $('#orderStatus').append(dataUse.orderStatus);
          $('#createdOnDate').append(dataUse.creationDate);
          $('#paymentMethod').append(dataUse.paymentMethod);
          $('#shopName').append(dataUse.shopName);
          $('#addressName').append(dataUse.addName);
          var address = dataUse.addLine1+" "+dataUse.addLine2+" "+dataUse.city+" "+dataUse.state+" "+dataUse.country+" "+dataUse.pincode;
          $('#deliveryAddress').append(address);
          $('#totalAmount').append(dataUse.orderAmount);
          $('#totalItems').append(dataUse.totalShopCount);
          var items = dataUse.shopInfo;
          $.each(items, function(i,v){
            var shopName = v.shopName;
            var orderStatus = v.orderStatus;
            var shopNumber = v.shopNumber;
            var totalBilledAmount = v.shopAmount;
            var shopOrderId = v.shopOrderId;
            var sShopId = "s"+v.shopId;
              var handshake = "images/handshakecolor.png";
              var intransit = "images/intransitcolor.png";
              var delivered ="images/deliveredcolor.png";
              var pr1 = 0;
              var pr2 = 0;
              var pr3 = 0;
              var printy="";
              var canDis = "none;"
            console.log("rATING " +v.rating);
            if(orderStatus=="CANCELED")
              {
               printy = "<li class='z-depth-3'><div class='collapsible-header shopCard_name' style='font-weight:600; font-size:1em;'> "+shopName+"<div id='check'><p style='color:red;'> Order Canceled </p></div><p><span style='float:left;'>Total Amount : </span> <span style='float:left; font-weight:600'> &#8377; "+totalBilledAmount+"</span><span style='float:right; border:1px dotted #56f442; padding:3px 10px;'><a >Details</a></span></p></div><div class=collapsible-body id="+sShopId+"> </div></li>";
                  $('#shopInfo').append(printy);
                }
            else{ 
                if (orderStatus=="NEW")
                {
                canDis = "block";
                shopOrderListvar.push(shopOrderId);
                handshake = "images/handshake.png";
                intransit = "images/intransit.png";
                delivered ="images/delivered.png";
                pr1=0;
                pr2=0;
                pr3=0;
                }
                else if(orderStatus=="ACCEPTED"){
                handshake = "images/handshakecolor.png";
                intransit = "images/intransit.png";
                delivered ="images/delivered.png";
                pr1=100;
                pr2=0;
                pr3=0;
                }
                else if(orderStatus=="WORKING")
                {
                handshake = "images/handshakecolor.png";
                intransit = "images/intransitcolor.png";
                delivered ="images/delivered.png";
                pr1=100;
                pr2=100;
                pr3=0;
                }
                else
                {
                handshake = "images/handshakecolor.png";
                intransit = "images/intransitcolor.png";
                delivered ="images/deliveredcolor.png";
                pr1=100;
                pr2=100;
                pr3=100;
                }
               printy = "<li class='z-depth-3'><div class='collapsible-header shopCard_name' style='font-weight:600; font-size:1em;'>"+shopName+"<div id='check'><div class='row' style='position:relative; font-family:roboto; font-size:0.85em; padding: 1em;' ><div class='col s4' style='text-align:center; padding:0px; margin:0px;'><div class='row' style='vertical-align:middle; padding-bottom:0px;margin-bottom:0px; position:relative; width: 100%;'><div style='padding:0px; margin:0px; vertical-align:middle; z-index: 99; width: 100%; position: absolute;'><img src='images/createordercolor.png' style='width: 24px; z-index: 99;  height: 24px; float: left '/></div><div style='padding:0px; margin: 0px; vertical-align:middle; width: 120%; position: absolute;'><span class='progress'><span class='determinate' style='width:"+pr1+"%;  background-color: #ffa751;'></span></span></div><div style='padding:0px; vertical-align:middle;  width: 100%; position: absolute;'><img src='"+handshake+"'id='intransit' style='width: 24px; z-index: 99;  height: 24px; float: right'/></div></div><div style='color:green; margin-top:2em;'>Accepted</div></div><div class='col s4' style='text-align:center;  padding:0px; margin:0px;'><div class='row' style='vertical-align:middle; padding:0px; margin:0px; position:relative; width: 100%;'><div style='padding:0px; margin: 0px; vertical-align:middle; width: 100%; position: absolute;'><span class='progress'><span class='determinate' style='width:"+pr2+"%;  background-color: #ffa751;'></span></span></div><div style='padding:0px; vertical-align:middle;  width: 100%; position: absolute;'><img src='"+intransit+"'  style='width: 24px; z-index: 99;  height: 24px; float: right'/></div></div><div style='color:green; margin-top: 2em;'>In Transit</div></div><div class='col s4' style='text-align:center;  padding:0px; margin:0px;'><div class='row'style='vertical-align:middle;  padding:0px; margin:0px; position:relative; width: 100%;'><div style='padding:0px; vertical-align:middle; width:100%; margin: 0px; position: absolute '><span class='progress'><span class='determinate' style='width:"+pr3+"%; background-color: #ffa751;'></span></span></div><div  style='padding:0px; vertical-align:middle;  width:100%; position: absolute;'><img src='"+delivered+"'style='width: 24px; z-index: 99;  height: 24px; float: right'/></div></div><div style='color:green; padding:0px; margin-top: 2em;'>Delivered</div></div></div></div><p><span style='float:left;'>Total Amount : </span><span style='float:left; font-weight:600'> &#8377; "+totalBilledAmount+"</span><span style='float:right; border:1px dotted #56f442; padding:3px 10px;'><a >Details</a></span></p></div><div class=collapsible-body id="+sShopId+" onclick='cancBtn("+dataUse.orderId+", "+shopOrderId+")' ><a class='btn red ' id='"+sShopId+"cancel' style='display:"+canDis+"'>Cancel</a></div></li>";
               $('#shopInfo').append(printy);
                }
              if(orderStatus=="COMPLETED")
              {
               if(v.rating >= 1)
                {
                    var printx = "<p class='rating_class'> Your Rating : <i class=material-icons style='text-align:center; vertical-align:middle; ' right>stars</i>  " + v.rating +"</p>";
                    $('#review'+v.shopOrderId+'').append(printx);
                }
                else
                {
                    var printx = "<a class= ' modal-trigger' href = '#modal1' onclick='currentOrder("+v.shopOrderId+", "+dataUse.orderId+")' ><i class=material-icons right style='color:#6d4c41; font-size:3em;'>stars</i> </a>";
                    $('#review'+v.shopOrderId+'').append(printx);
                }
                }
            var shopItems = v.shopItems;
            $.each(shopItems, function(j,l){
              if(l.subsDay==null){
                var lPrint = "<div class='row z-depth-2' style='padding:0px; margin:0px;'> <div class='col s12' style='padding:0px;'> <p class='shopCard_name' style='text-align:left;'>"+l.itemName+"</p> <p class='shopCard_delChar' style='text-align:left; font-size:1em;'>Price : "+l.amount+"</p> </div> </div>";
              }
              else{
                var lPrint = "<div class='row z-depth-2' style='padding:0px; margin:0px;'> <div class='col s12' style='padding:0px;'> <p class='shopCard_name' style='text-align:left;'>"+l.itemName+"</p> <p class='shopCard_delChar' style='text-align:left; font-size:1em;'>Price : "+l.amount+"<span class='para_p' style='float:right;'><a href='/subsdetails.html?subsId="+l.orderItemId+"' class='para_an waves-effect waves-green' style='font-size:0.9em; font-weight:300;'>Track</a></span></p> </div> </div>";
              }
              var xPrint = "<tr><td>"+l.itemName+"</td><td>"+l.amount+"</td><td>"+l.quantity+"</td></tr>";
              $('#'+sShopId).append(lPrint);
            })
          })
        },
        error: function(response){
          Materialize.toast('Not authorized to view the page', 4000)
          
        }
      });
        
    }
    else{
      Materialize.toast('Not authorized to view the page', 4000)
      setTimeout(function(){
         window.location.replace(feUrl()+"x.html");
      }, 500);
    }
    
  });

  function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
    });
    return vars;
  }
    function currentOrder(orderStoreId, orderMasterId)
{
    localStorage.setItem("orderStoreId", orderStoreId);
    localStorage.setItem("orderMasterId", orderMasterId);
    
    return false
}
var orderId1;
var subOrderId1;
function cancBtn(orderId, subOrderId)
{    orderId1 = orderId;
     subOrderId1 = subOrderId;
    $('#canMod').modal('open');
    
 return false;
}
 function canPositive(orderId = orderId1, subOrderId = subOrderId1)
 {
  var subOrderArr = new Array();
  var subOrderIdStr = String(subOrderId)
  subOrderArr.push(subOrderIdStr);
  console.log("orderId, "+orderId+ "   subOrderId" + subOrderIdStr);
     var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
      var orderId = getUrlVars()["orderId"];
      var myObj = {};
      myObj['typeId']=sessionInfo.typeId;
      myObj['userId']=sessionInfo.userId;
      myObj['userType']=sessionInfo.userType;
      myObj['orderId']=orderId;
      myObj['orderList']=subOrderArr;
      var jsonObj = JSON.stringify(myObj);
      console.log("jsonObj= " + jsonObj);
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/cancelorder',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
             Materialize.toast('Order is cancelled', 1000)
          setTimeout(function(){
             window.location.replace(feUrl()+"orderdetails.html?orderId="+orderId);
          }, 1000);
},
          error: function(response)
          {
              Materialize.toast('Something went up wrong', 4000);
          }
      })
 }
function canNegative()
    {
     return false;
    }