$(document).ready(function(){
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
    var data = $("#search").serialize();

    $("#addressSearch").click();
});
$('.modal').modal()
$('#warnmodal1').modal({
  dismissible: false,
});
if(localStorage.getItem("delVal")){
  if(localStorage.getItem("delVal")){
    var url = feUrl()+'/shoplist';
    var delVal = localStorage.getItem("delVal");
    var delInfo = JSON.parse(delVal);
    var myObj = {};
    myObj['lattitude']=delInfo.lattitude;
    myObj['longitude']=delInfo.longitude;
    var data = JSON.stringify(myObj);
    var shopId = getUrlVars()["shopId"];
    $.ajax({
      url : beUrl()+'/shoplist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      data:data,
      success: function(response) {
        console.log('here');
        var shopData = JSON.stringify(response);
        var shopInfo = JSON.parse(shopData);
        var totalCount = shopInfo.totalCount;
        if(totalCount>0){
          var l = 0;
          var abc = shopInfo.data;
          localStorage.removeItem("availableShopList")
          $.each(abc, function(i,v){
            var storeInfo = JSON.stringify(v)
            localStorage.setItem(v.shopId, storeInfo);
            if(v.shopId==shopId){
              if(v.isOpen==1){
                l = 1;
              }
              else{
                l = 2;
              }
            }
          });
          if(l==1){
            window.location.replace(feUrl()+"/items.html?shopId="+shopId);
          }
          else if (l==2) {
            $('#closedShop1').modal('open');
          }
          else{
            $('#warnmodal1').modal('open');
          }
        }
        else{
          $('#warnmodal1').modal('open');
        }
      }
    });
  }
  }
$('head').append('<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Berkshire+Swash|Chela+One|Arima+Madurai|Josefin+Slab:700|Mogra|Limelight|Risque"rel="stylesheet">');
if(localStorage.getItem("sessionInfo"))
  { var printx = "<li><a href='logout.html'>Logout</a></li><li><a href='tel:+917289036012'>728-903-6012</a></li>";
   $('#nav').append(printx);}
else{
   var printx = "<li> <a href='login.html'>LogIn</a></li><li><a href='signup.html'>Register</a></li><li><a href='tel:+917289036012'>728-903-6012</a></li>";
$('#nav').append(printx);}


  $("#searchForm").submit(function(event){
    event.preventDefault();
    $("#addressSearch").click();
  })
  $("#addressSearch").click(function(){

    var userPassed = document.addresssearch.search.value;
    var dummy = userPassed;
    userPassed=userPassed.replace(/(^\s+|[^a-zA-Z0-9 ]+|\s+$)/g,"");   //this one
    userPassed=userPassed.replace(/\s+/g, "+");
    var url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+userPassed+'&key=AIzaSyAQF1IHXvQpG4nEP1wQA1n-9eHcbnk3w3w';
    $.ajax({
        url : url,
        type: 'GET',
		    dataType:'json',
		    processData: false,
		    complete: function(data) {
            var d =  JSON.stringify(data);
            var d1 = JSON.parse(d);
            var abc = d1.responseJSON.results;
            $.each(abc,function(i,v){
                var outLat = v.geometry.location.lat;
                var outLng = v.geometry.location.lng;
                formData = JSON.stringify({
                  "lattitude":outLat,
                  "longitude":outLng
                });

                var myObl = {};
                myObl['lattitude']=outLat;
                myObl['longitude']=outLng;
                var delData = JSON.stringify(myObl)
                localStorage.setItem('delVal',delData)
                if(localStorage.getItem('chosenAddId'))
                {
                  localStorage.removeItem('chosenAddId');
                }
                proceedWithAddress();
            });
		    },
		    error: function() {
             Materialize.toast('Please pass an address', 4000)
		    },

    });
  });
});

function proceedWithAddress() {
  if(localStorage.getItem('delVal')){
    var delVal = localStorage.getItem("delVal");
    var delInfo = JSON.parse(delVal);
    var myObj = {};
    myObj['lattitude']=delInfo.lattitude;
    myObj['longitude']=delInfo.longitude;
    var data = JSON.stringify(myObj);
    var shopId = getUrlVars()["shopId"];
    $.ajax({
      url : beUrl()+'/shoplist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      data:data,
      success: function(response) {
        console.log('here');
        var shopData = JSON.stringify(response);
        var shopInfo = JSON.parse(shopData);
        var totalCount = shopInfo.totalCount;
        if(totalCount>0){
          var l = 0;
          var abc = shopInfo.data;
          localStorage.removeItem("availableShopList")
          $.each(abc, function(i,v){
            var storeInfo = JSON.stringify(v)
            localStorage.setItem(v.shopId, storeInfo);
            if(v.shopId==shopId){
              if(v.isOpen==1){
                l = 1;
                console.log('here');
              }
              else{
                l = 2;
              }
            }
          });
          if(l==1){
            window.location.replace(feUrl()+"/items.html?shopId="+shopId);
          }
          else if (l==2) {
            $('#closedShop1').modal('open');
          }
          else{
            $('#warnmodal1').modal('open');
          }
        }
        else{
          $('#warnmodal1').modal('open');
        }
      }
    });
  }
  else {
    Materialize.toast('Please pass an address', 4000)
    $('#modal1').modal('open');
  }
}

function handleClick(addId) {
  localStorage.setItem('chosenAddId',addId);
  var aAddId = 'a'+addId;
  var delVal=sessionStorage.getItem(aAddId);
  var delValParse = JSON.parse(delVal);
  var addObj={};
  addObj['lattitude']=delValParse.lattitude;
  addObj['longitude']=delValParse.longitude;
  var delStoreVal = JSON.stringify(addObj)
  localStorage.setItem('delVal',delStoreVal);
}
function initialize() {
  var input = document.getElementById('search');
  new google.maps.places.Autocomplete(input);
}

function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    vars[key] = value;
  });
  return vars;
}

function goToShop() {
  window.location.replace(feUrl()+"/shops.html");
}


function clearAndProceed() {
  $('#warnmodal1').modal('close');
  if(localStorage.getItem("sessionInfo")){
    var sessionInfo = localStorage.getItem("sessionInfo");
    localStorage.clear();
    localStorage.setItem("sessionInfo", sessionInfo);
    var DBDeleteRequest = window.indexedDB.deleteDatabase("cartInfo");
    DBDeleteRequest.onerror = function(event) {
    };
    DBDeleteRequest.onsuccess = function(event) {
    };
    var DBDeleteRequest = window.indexedDB.deleteDatabase("cartShopInfo");
    DBDeleteRequest.onerror = function(event) {
    };
    DBDeleteRequest.onsuccess = function(event) {
    };
    var DBDeleteRequest = window.indexedDB.deleteDatabase("itemDesc");
    DBDeleteRequest.onerror = function(event) {
    };
    DBDeleteRequest.onsuccess = function(event) {
    };
  }
  else{
    localStorage.clear();
    var DBDeleteRequest = window.indexedDB.deleteDatabase("cartInfo");
    DBDeleteRequest.onerror = function(event) {
    };
    DBDeleteRequest.onsuccess = function(event) {
    };
    var DBDeleteRequest = window.indexedDB.deleteDatabase("cartShopInfo");
    DBDeleteRequest.onerror = function(event) {
    };
    DBDeleteRequest.onsuccess = function(event) {
    };
    var DBDeleteRequest = window.indexedDB.deleteDatabase("itemDesc");
    DBDeleteRequest.onerror = function(event) {
    };
    DBDeleteRequest.onsuccess = function(event) {
    };
  }
  $('.modal').modal()
  if(window.localStorage){
    if(localStorage.getItem("sessionInfo")){
      var sessionInfo = localStorage.getItem("sessionInfo");
      var sessionDummy = JSON.parse(sessionInfo);
      var myObj = {};
      myObj['userId']=sessionDummy.userId;
      var data = JSON.stringify(myObj);
      var head = sessionDummy.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/custaddresslist',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:data,
        success: function(response) {
          var address = JSON.stringify(response);
          var addressUse = JSON.parse(address);
          var address = addressUse.address;
          $.each(address, function(i,v){
            var addShow = v.addressLine1;
            addShow = addShow +" "+v.addressLine2;
            addShow = addShow + " "+ v.city;
            addShow = addShow + " "+v.state;
            addShow = addShow + " "+ v.country;
            addShow = addShow + "-"+ v.pincode;
            var addName = v.addName;
            var addId = v.addressId;
            var print = "<p><input name=group1 class=group1 type=radio id="+addId+" onclick=handleClick("+addId+") /><label for="+addId+" style='float:left'><h5 class='subhead'>"+addName+"</h5></label><p class='content'>"+addShow+"</p></p><hr>";
            $('#addressList').append(print);
            var addObj = {};
            addObj['lattitude']=v.addLat;
            addObj['longitude']=v.addLng;
            addObj['addressLine1']=v.addressLine1;
            addObj['addressLine2']=v.addressLine2;
            addObj['city']=v.city;
            addObj['state']=v.state;
            addObj['country']=v.country;
            addObj['pincode']=v.pincode;
            addObj['addName']=v.addName;
            var addDelVal = JSON.stringify(addObj);
            var aAddId = 'a'+addId
            sessionStorage.setItem(aAddId,addDelVal);
          })
          if(addressUse.addressCount>0){
            $('#modal1').modal('open');
          }
        }
      })
    }
  }


}
