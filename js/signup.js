$(document).ready(function(){
  $('.modal').modal({
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5,
  })
  $('#signUpOnly1').modal('open');
  $("#signUp1").submit(function(event){
event.preventDefault();
$("#signup1").click();
});
  $("#signup1").click(function(){
    var userNameId = document.getElementById("signupUsername")
    var userNameClass = userNameId.className;
    var userPassId = document.getElementById("signuppassword");
    var userPassClass = userPassId.className;
    if((userNameClass=="validate valid")&&(userPassClass=="validate valid")){
      var signUpObj = {}
      signUpObj['userNameLogin']=userNameId.value;
      signUpObj['password']=userPassId.value;
      var signUpJson = JSON.stringify(signUpObj);
      sessionStorage.setItem('signUpJson',signUpJson)
      if(sessionStorage.getItem('signUpJson')){
        Materialize.toast('Almost there', 4000);
        $('#signUpOnly1').modal('close');
        $('#signUpOnly2').modal('open');
      }
    }
    else {
      Materialize.toast('See the error message', 4000);
    }
  });
  $("#signUp2").submit(function(event){
event.preventDefault();
$("#signup2").click();
});
  $("#signup2").click(function(){
    event.preventDefault();
    var custNameId = document.getElementById('signupCustname');
    var custEmailId = document.getElementById('userEmail');
    var custPhoneId = document.getElementById('userPhone');
    var custNameClass = custNameId.className;
    var custEmailClass = custEmailId.className;
    var custPhoneClass = custPhoneId.className;
    if((custNameClass=="validate valid")&&(custEmailClass=="validate valid")&&(custPhoneClass=="validate valid"))
    {
      if((document.getElementById("check").checked) == true){
      var loginObject = sessionStorage.getItem('signUpJson');
      var loginPars = JSON.parse(loginObject);
      sessionStorage.removeItem('signUpJson');
      var signUpObject = {};
      signUpObject['userType']="CUST";
      signUpObject['userNameLogin']=loginPars.userNameLogin;
      signUpObject['password']=loginPars.password;
      signUpObject['phoneNumber']=custPhoneId.value;
      signUpObject['custName']=custNameId.value;
      signUpObject['emailId']=custEmailId.value;
      var jsonSignUpJson = JSON.stringify(signUpObject);
      sessionStorage.setItem('signUpJson',jsonSignUpJson);
      var otp = {};
      otp['phoneNumber']=custPhoneId.value;
      otp['userType']="CUST";
      otp['userNeed']="SIGNUP";
      var jsonOtp =JSON.stringify(otp);
      $.ajax({
        url : beUrl()+'/generateotp',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        data:jsonOtp,
        success: function(response) {
          Materialize.toast('Please see your phone for OTP', 4000);
          $('#signUpOnly2').modal('close');
          $('#signUpOnly3').modal('open');
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
        }
      });
    }
    else{
      Materialize.toast('Kindly accept our T&C', 4000);
    }
  }
    else {
      Materialize.toast('See the error message', 4000);
    }
  });
  $("#signUp2").submit(function(event){
event.preventDefault();
$("#signup2").click();
});
  $("#signup3").click(function(){
    event.preventDefault();
    $("#signup3").addClass('disabled');
    var otpId = document.getElementById('custOTP');
    var otpclass = otpId.className;
    if(otpclass=="validate valid"){
      var signUpObjectFinal = sessionStorage.getItem('signUpJson');
      var jsonParser = JSON.parse(signUpObjectFinal);
      var finalObject = {};
      finalObject['otp']=otpId.value;
      finalObject['userNameLogin']=jsonParser.userNameLogin;
      finalObject['password']=jsonParser.password;
      finalObject['phoneNumber']=jsonParser.phoneNumber;
      finalObject['custName']=jsonParser.custName;
      finalObject['emailId']=jsonParser.emailId;
      finalObject['userType']="CUST";
      var finalJsonObject = JSON.stringify(finalObject);
      $.ajax({
        url : beUrl()+'/createuser',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        data:finalJsonObject,
        success: function(response) {
          Materialize.toast('Please wait for magic', 4000);
          var sessionInfo = JSON.stringify(response);
          localStorage.setItem('sessionInfo',sessionInfo);;
          setTimeout(function(){
            if(localStorage.getItem('delVal')){
              window.location.replace(feUrl()+"/shops.html");
            }
            else{
              window.location.replace(feUrl()+"/index.html");
            }
          }, 1000);
        },
        error: function (response) {
          var responseJson = JSON.stringify(response);
          var responseJsonR = JSON.parse(responseJson);
          var statusCode = responseJsonR.responseJSON;
          var data = JSON.stringify(statusCode);
          var xData = JSON.parse(data);
          var message = xData.Data;
          Materialize.toast(message, 4000);
          $("#signup3").removeClass('disabled');
        }
      });
    }
    else {
      Materialize.toast('Please pass OTP', 4000);
    }
  })
});

function userAvailability() {
  var userData = document.getElementById('signupUsername').value;
  myObj = {};
  myObj['userName'] = userData;
  myObj['mode']="client";
  var jsonData = JSON.stringify(myObj);
  $.ajax({
    url : beUrl()+'/validateuser',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:jsonData,
    error: function(response) {
      $("#signupUsername").next("label").attr('data-error','The userName is already taken');
      $("#signupUsername").removeClass("valid");
      $("#signupUsername").addClass("invalid");
    },
    success: function (response) {
      $("#signupUsername").next("label").attr('data-success','The userName is available');
      $("#signupUsername").removeClass("invalid");
      $("#signupUsername").addClass("valid");
    }
  });
}
function resendotp()
{
  console.log('OTP resent');
  var phoneNumber=JSON.parse(sessionStorage.getItem('signUpJson')).phoneNumber;
  console.log(phoneNumber);
  var otp = {};
  otp['phoneNumber']=phoneNumber;
  otp['userType']="CUST";
  otp['userNeed']="SIGNUP";
  var jsonOtp =JSON.stringify(otp);
  $.ajax({
    url : beUrl()+'/generateotp',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:jsonOtp,
    success: function(response) {
      Materialize.toast('OTP has been resent', 4000);
      $('#signUpOnly3').modal('open');
    },
    error: function (response) {
      var responseJson = JSON.stringify(response);
      var responseJsonR = JSON.parse(responseJson);
      var statusCode = responseJsonR.responseJSON;
      var data = JSON.stringify(statusCode);
      var xData = JSON.parse(data);
      var message = xData.Data;
      Materialize.toast(message, 4000);
    }
  });
}
function passwordValidity() {
  var validatePass = document.getElementById('signuppassword');
  var length = validatePass.value.length;
  if(length<8){
    $("#signuppassword").next("label").attr('data-error','Too small password');
    $("#signuppassword").removeClass("valid");
    $("#signuppassword").addClass("invalid");
  }
  else if (length>24) {
    $("#signuppassword").next("label").attr('data-error','Too big password');
    $("#signuppassword").removeClass("valid");
    $("#signuppassword").addClass("invalid");
  }
  else {
    $("#signuppassword").next("label").attr('data-success','The password is acceptable');
    $("#signuppassword").removeClass("invalid");
    $("#signuppassword").addClass("valid");
  }
}
function phoneAvailablity() {
  var validatePhone = document.getElementById('userPhone');
  var length = validatePhone.value.length;
  if(length<10){
    $("#userPhone").next("label").attr('data-error','Too small phone number');
    $("#userPhone").removeClass("valid");
    $("#userPhone").addClass("invalid");
  }
  else if (length>10) {
    $("#userPhone").next("label").attr('data-error','Too big phone number');
    $("#userPhone").removeClass("valid");
    $("#userPhone").addClass("invalid");
  }
  else {
    var phoneNumber = validatePhone.value;
    var phoneObj = {};
    phoneObj['phoneNumber']=phoneNumber;
    phoneObj['userType']="CUST";
    var jsonPhone = JSON.stringify(phoneObj);
    $.ajax({
      url : beUrl()+'/validphone',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      data:jsonPhone,
      error: function(response) {
        $("#userPhone").next("label").attr('data-error','PhoneNumber already in use');
        $("#userPhone").removeClass("valid");
        $("#userPhone").addClass("invalid");
      },
      success: function (response) {
        $("#userPhone").next("label").attr('data-success','PhoneNumber is available');
        $("#userPhone").removeClass("invalid");
        $("#userPhone").addClass("valid");
      }
    });
  }
}
